package application;

import controleur.ChoixFeuilleDeMatchController;
import controleur.CompteRenduController;
import controleur.ScreensController;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import modele.Match;

public class Main  extends Application {
	private static Stage stage;

	public static void main(String[] args) {		
		loadFonts();
		launch(args);
	}
	
	private static void loadFonts() {
		Font.loadFont(Main.class.getResourceAsStream("/font/HelveticaNeue.ttf"), 20);
		Font.loadFont(Main.class.getResourceAsStream("/font/HelveticaNeueLight.ttf"), 20);
		Font.loadFont(Main.class.getResourceAsStream("/font/HelveticaNeueThin.ttf"), 20);
		Font.loadFont(Main.class.getResourceAsStream("/font/HelveticaNeueBold.ttf"), 20);
		Font.loadFont(Main.class.getResourceAsStream("/font/calendarfont.ttf"), 20);
		Font.loadFont(Main.class.getResourceAsStream("/font/fontello.ttf"), 20);
		Font.loadFont(Main.class.getResourceAsStream("/font/flaticon.ttf"), 20);
		Font.loadFont(Main.class.getResourceAsStream("/font/resume.ttf"), 20);
		Font.loadFont(Main.class.getResourceAsStream("/font/principal.ttf"), 20);
		Font.loadFont(Main.class.getResourceAsStream("/font/rank.ttf"), 20);
		Font.loadFont(Main.class.getResourceAsStream("/font/recapitulatif.ttf"), 20);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		stage = primaryStage;

		stage.getIcons().add(new Image(Main.class.getResourceAsStream("CompteRenduChampionnat.png")));
		stage.setTitle("Gestion de championnat de handball");
		stage.setScene(initChoixFeuilleDeMatch());
		stage.initStyle(StageStyle.TRANSPARENT);
		stage.show();
	}
	
	public Scene initChoixFeuilleDeMatch() {
		ChoixFeuilleDeMatchController controller = new ChoixFeuilleDeMatchController();
		
		String fichierCss1 = Main.class.getResource("application.css").toExternalForm();
		String fichierCss2 = Main.class.getResource("/vue/feuilleDeMatch.css").toExternalForm();
		
		ScreensController conteneurPrincipal = new ScreensController(425, 250);
		conteneurPrincipal.setId("fenetre");
		conteneurPrincipal.addScreen(controller.getVue().getPanePrincipal());
		conteneurPrincipal.showScreen();
		
		conteneurPrincipal.getS().getStylesheets().addAll(fichierCss1, fichierCss2);
		
		return conteneurPrincipal.getS();
	}
	
	public static void showInterfaceCompteRendu(Match m) {
		CompteRenduController controller = new CompteRenduController(m);
		
		String fichierCss1 = Main.class.getResource("application.css").toExternalForm();
		String fichierCss2 = Main.class.getResource("/vue/feuilleDeMatch.css").toExternalForm();
		
		ScreensController conteneurPrincipal = new ScreensController(1000, 725);
		conteneurPrincipal.setId("fenetre");
		conteneurPrincipal.addScreen(controller.getVue().getPanePrincipal());
		conteneurPrincipal.showScreen();
		
		conteneurPrincipal.getS().getStylesheets().addAll(fichierCss1, fichierCss2);
		
		stage.setScene(conteneurPrincipal.getS());
		stage.centerOnScreen();
		stage.sizeToScene();
		
	}

	public static Stage getStage() {
		return stage;
	}


}
