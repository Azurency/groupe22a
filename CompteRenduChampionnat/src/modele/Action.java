package modele;


public class Action {

	private static int idMax = 0;
	
	private int id , idJ, idM;
	private String timer,action,joueur, equipe;
	
	public Action(){
		id=-1;
		setIdJ(-1);
		idM = -1;
		timer = "";
		action = "";
		joueur= "";
		equipe = "";
	}
	
	public Action(int idM,int idJ,String equipe,String timer, String action, String joueur) {
		this.id = idMax++;
		this.idM=idM;
		this.idJ = idJ;
		this.timer = timer;
		this.action = action;
		this.joueur = joueur;
		this.equipe=equipe;
	}
	public Action(int id,int idM,int idJ,String equipe, String timer, String action, String joueur) {
		this.id = id;
		if(id>=idMax){
			idMax = id+1;
		}
		this.idJ = idJ;
		this.idM = idM;
		this.timer = timer;
		this.action = action;
		this.joueur = joueur;
		this.equipe = equipe;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTimer() {
		return timer;
	}
	public void setTimer(String timer) {
		this.timer = timer;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getJoueur() {
		return joueur;
	}
	public void setJoueur(String joueur) {
		this.joueur = joueur;
	}
	public String getEquipe() {
		return equipe;
	}
	public void setEquipe(String equipe) {
		this.equipe = equipe;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Action other = (Action) obj;
		if (id != other.id)
			return false;
		return true;
	}

	public int getIdJ() {
		return idJ;
	}

	public void setIdJ(int idJ) {
		this.idJ = idJ;
	}

	public int getidM() {
		return idM;
	}

	public void setidM(int idM) {
		this.idM = idM;
	}
	
}
