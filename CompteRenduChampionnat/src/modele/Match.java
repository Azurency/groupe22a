package modele;

import java.io.File;
import java.io.FileNotFoundException;
import java.math.MathContext;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;

import javax.swing.text.StyledEditorKit.BoldAction;

import modele.FeuilleDeMatch.JoueurFDM;

import org.ho.yaml.Yaml;

public class Match {


	private int id;

	private String nomEquipe1;
	private int idEquipe1;
	private int scoreEq1;
	private String nomEquipe2;
	private int idEquipe2;
	private int scoreEq2;
	private ArrayList<JoueurFDM> joueurs;
	private ArrayList<Action> actions;

	public Match(){
		id=-1;
		nomEquipe1="";
		setIdEquipe1(-1);
		scoreEq1=-1;
		nomEquipe2="";
		setIdEquipe2(-1);
		scoreEq2=-1;
		setJoueurs(new ArrayList<JoueurFDM>());
		actions=new ArrayList<Action>();
	}
	public Match(int id, String nomEquipe1, int idEquipe1, int scoreEq1,
			String nomEquipe2, int idEquipe2, int scoreEq2,
			ArrayList<JoueurFDM> joueurs, ArrayList<Action> actions) {
		super();
		this.id = id;
		this.nomEquipe1 = nomEquipe1;
		this.idEquipe1 = idEquipe1;
		this.scoreEq1 = scoreEq1;
		this.nomEquipe2 = nomEquipe2;
		this.idEquipe2 = idEquipe2;
		this.scoreEq2 = scoreEq2;
		this.joueurs = joueurs;
		this.actions = actions;
	}

	

	public static Match chargerFeuilleDeMatch(File file){
		Match m = null;
		try {
			FeuilleDeMatch fdm = Yaml.loadType(file,FeuilleDeMatch.class);
			m = new Match(fdm.getId(),fdm.getEquipe1().getNom(),fdm.getEquipe1().getId()
					,0,fdm.getEquipe2().getNom(),fdm.getEquipe2().getId(),0
					,fdm.getJoueurs(),new ArrayList<Action>());
		} catch (FileNotFoundException | NullPointerException e) {
			System.out.println("Fichier non valide");
		}
		return m;
	}
	
	public void enregistrerMatch(File file){
		RecapAction recap = new RecapAction(id, scoreEq1, scoreEq2, actions);
		try {
			Yaml.dump(recap, file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}



	public void ajouterBut(JoueurFDM j, String timer){
		String equipe = "";
		if(j.getIdE() == this.getIdEquipe1()){
			equipe = this.getNomEquipe1();
			scoreEq1++;
		}
		else{
			equipe = this.getNomEquipe2();
			scoreEq2++;
		}
		actions.add(new Action(this.getId(), j.getId(), equipe, timer, "but", j.getPrenom() + " "+ j.getNom()));
	}
	public void ajouterFaute(JoueurFDM j, String timer){
		String equipe = "";
		if(j.getIdE() == this.getIdEquipe1()){
			equipe = this.getNomEquipe1();
		}
		else{
			equipe = this.getNomEquipe2();
		}
		actions.add(new Action(this.getId(), j.getId(), equipe, timer, "faute", j.getPrenom() + " "+ j.getNom()));
	}
	public void ajouterTir(JoueurFDM j, String timer){
		String equipe = "";
		if(j.getIdE() == this.getIdEquipe1()){
			equipe = this.getNomEquipe1();
		}
		else{
			equipe = this.getNomEquipe2();
		}
		actions.add(new Action(this.getId(), j.getId(), equipe, timer, "tir", j.getPrenom() + " "+ j.getNom()));
	}
	public void ajouterEntree(JoueurFDM j, String timer){
		String equipe = "";
		if(j.getIdE() == this.getIdEquipe1()){
			equipe = this.getNomEquipe1();
		}
		else{
			equipe = this.getNomEquipe2();
		}
		actions.add(new Action(this.getId(), j.getId(), equipe, timer, "entrée", j.getPrenom() + " "+ j.getNom()));
	}
	public void ajouterSortie(JoueurFDM j, String timer){
		String equipe = "";
		if(j.getIdE() == this.getIdEquipe1()){
			equipe = this.getNomEquipe1();
		}
		else{
			equipe = this.getNomEquipe2();
		}
		actions.add(new Action(this.getId(), j.getId(), equipe, timer, "sortie", j.getPrenom() + " "+ j.getNom()));
	}
	public void ajouterPasse(JoueurFDM j, String timer){
		String equipe = "";
		if(j.getIdE() == this.getIdEquipe1()){
			equipe = this.getNomEquipe1();
		}
		else{
			equipe = this.getNomEquipe2();
		}
		actions.add(new Action(this.getId(), j.getId(), equipe, timer, "passe decisive", j.getPrenom() + " "+ j.getNom()));
	}
	public void ajouterErreur(JoueurFDM j, String timer){
		String equipe = "";
		if(j.getIdE() == this.getIdEquipe1()){
			equipe = this.getNomEquipe1();
		}
		else{
			equipe = this.getNomEquipe2();
		}
		actions.add(new Action(this.getId(), j.getId(), equipe, timer, "erreur", j.getPrenom() + " "+ j.getNom()));
	}
	public void ajouterAvertissement(JoueurFDM j, String timer){
		String equipe = "";
		if(j.getIdE() == this.getIdEquipe1()){
			equipe = this.getNomEquipe1();
		}
		else{
			equipe = this.getNomEquipe2();
		}
		actions.add(new Action(this.getId(), j.getId(), equipe, timer, "avertissement", j.getPrenom() + " "+ j.getNom()));
	}
	public void ajouterExclusion(JoueurFDM j, String timer){
		String equipe = "";
		if(j.getIdE() == this.getIdEquipe1()){
			equipe = this.getNomEquipe1();
		}
		else{
			equipe = this.getNomEquipe2();
		}
		actions.add(new Action(this.getId(), j.getId(), equipe, timer, "exclusion", j.getPrenom() + " "+ j.getNom()));
	}
	public void ajouterDisqualification(JoueurFDM j, String timer){
		String equipe = "";
		if(j.getIdE() == this.getIdEquipe1()){
			equipe = this.getNomEquipe1();
		}
		else{
			equipe = this.getNomEquipe2();
		}
		actions.add(new Action(this.getId(), j.getId(), equipe, timer, "discalification",j.getPrenom() + " "+ j.getNom()));
	}
	public void ajouterTempsMort(JoueurFDM j, String timer){
		String equipe = "";
		if(j.getIdE() == this.getIdEquipe1()){
			equipe = this.getNomEquipe1();
		}
		else{
			equipe = this.getNomEquipe2();
		}
		actions.add(new Action(this.getId(), j.getId(), equipe, timer, "temps mort", j.getPrenom() + " "+ j.getNom()));
	}
	
	
	

	//getters-setters
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getScoreEq1() {
		return scoreEq1;
	}
	public void setScoreEq1(int scoreEq1) {
		this.scoreEq1 = scoreEq1;
	}
	public int getScoreEq2() {
		return scoreEq2;
	}
	public void setScoreEq2(int scoreEq2) {
		this.scoreEq2 = scoreEq2;
	}
	public ArrayList<Action> getActions() {
		return actions;
	}
	public void setActions(ArrayList<Action> actions) {
		this.actions = actions;
	}

	public int getIdEquipe1() {
		return idEquipe1;
	}

	public void setIdEquipe1(int idEquipe1) {
		this.idEquipe1 = idEquipe1;
	}

	public int getIdEquipe2() {
		return idEquipe2;
	}

	public void setIdEquipe2(int idEquipe2) {
		this.idEquipe2 = idEquipe2;
	}
	
	public ArrayList<JoueurFDM> getJoueurs() {
		return joueurs;
	}
	
	public ArrayList<JoueurFDM> getEntraineurs() {
		ArrayList<JoueurFDM> res = new ArrayList<JoueurFDM>();
		for (JoueurFDM joueur : joueurs) {
			if (joueur.getRole().equals("entraineur")) {
				res.add(joueur);
			}
		}
		return res;
	}

	public void setJoueurs(ArrayList<JoueurFDM> joueurs) {
		this.joueurs = joueurs;
	}

	public String getNomEquipe1() {
		return nomEquipe1;
	}

	public void setNomEquipe1(String nomEquipe1) {
		this.nomEquipe1 = nomEquipe1;
	}

	public String getNomEquipe2() {
		return nomEquipe2;
	}

	public void setNomEquipe2(String nomEquipe2) {
		this.nomEquipe2 = nomEquipe2;
	}

	public String getTextEquipes(){
		return this.nomEquipe1 + " contre "+ this.nomEquipe2;
	}

}
