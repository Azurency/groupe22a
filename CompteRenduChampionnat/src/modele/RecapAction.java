package modele;

import java.util.ArrayList;



public class RecapAction{
	private int id, scoreEq1, scoreEq2;

	private ArrayList<Action> actions;
	
	
	public RecapAction(){
		id=0;
		scoreEq1=0;
		scoreEq2=0;
	}
	
	public RecapAction(int id, int scoreEq1, int scoreEq2,
			ArrayList<Action> actions) {
		this.id = id;
		this.scoreEq1 = scoreEq1;
		this.scoreEq2 = scoreEq2;
		this.actions = actions;
	}
	public RecapAction(int id,ArrayList<Action> actions){
		this.setId(id);
		this.setActions(actions);
	}

	

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public ArrayList<Action> getActions() {
		return actions;
	}

	public void setActions(ArrayList<Action> actions) {
		this.actions = actions;
	}

	public int getScoreEq1() {
		return scoreEq1;
	}

	public void setScoreEq1(int scoreEq1) {
		this.scoreEq1 = scoreEq1;
	}

	public int getScoreEq2() {
		return scoreEq2;
	}

	public void setScoreEq2(int scoreEq2) {
		this.scoreEq2 = scoreEq2;
	}
	
	
}