package modele;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;


public class FeuilleDeMatch{

	private int id;
	private ArrayList<JoueurFDM> joueurs;
	private EquipeFDM equipe1;
	private EquipeFDM equipe2;

	public FeuilleDeMatch(){
		id=-1;
		joueurs = null;
		equipe1 = null;
		equipe2 = null;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public ArrayList<JoueurFDM> getJoueurs() {
		return joueurs;
	}
	public void setJoueurs(ArrayList<JoueurFDM> joueurs) {
		this.joueurs = joueurs;
	}
	public EquipeFDM getEquipe1() {
		return equipe1;
	}
	public void setEquipe1(EquipeFDM equipe1) {
		this.equipe1 = equipe1;
	}
	public EquipeFDM getEquipe2() {
		return equipe2;
	}
	public void setEquipe2(EquipeFDM equipe2) {
		this.equipe2 = equipe2;
	}


	public static class JoueurFDM{
		private int id,idE;
		private int num;
		private String nom,prenom,age,role;


		public JoueurFDM() {
			this.setId(-1);
			this.setIdE(-1);
			this.setNum(-1);
			this.setNom("");
			this.setPrenom("");
			this.setAge("");
			this.setRole("");
		}


		public JoueurFDM(int id,int idE, int num, String nom, String prenom,
				String age, String role) {
			this.setId(id);
			this.setIdE(idE);
			this.setNum(num);
			this.setNom(nom);
			this.setPrenom(prenom);
			this.setAge(age);
			this.setRole(role);
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			JoueurFDM other = (JoueurFDM) obj;
			if (age == null) {
				if (other.age != null)
					return false;
			} else if (!age.equals(other.age))
				return false;
			if (id != other.id)
				return false;
			if (idE != other.idE)
				return false;
			if (nom == null) {
				if (other.nom != null)
					return false;
			} else if (!nom.equals(other.nom))
				return false;
			if (num != other.num)
				return false;
			if (prenom == null) {
				if (other.prenom != null)
					return false;
			} else if (!prenom.equals(other.prenom))
				return false;
			if (role == null) {
				if (other.role != null)
					return false;
			} else if (!role.equals(other.role))
				return false;
			return true;
		}


		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public int getNum() {
			return num;
		}
		public void setNum(int num) {
			this.num = num;
		}
		public String getNom() {
			return nom;
		}
		public void setNom(String nom) {
			this.nom = nom;
		}
		public String getPrenom() {
			return prenom;
		}
		public void setPrenom(String prenom) {
			this.prenom = prenom;
		}
		public String getAge() {
			return age;
		}
		public void setAge(String age) {
			this.age = age;
		}
		public String getRole() {
			return role;
		}
		public void setRole(String role) {
			this.role = role;
		}
		public int getIdE() {
			return idE;
		}
		public void setIdE(int idE) {
			this.idE = idE;
		}

	}
	public static class EquipeFDM{
		private int id;
		private String nom;
		public EquipeFDM(){
			id=-1;
			nom="";
		}
		public EquipeFDM(int id, String nom){
			this.id = id;
			this.nom = nom;
		}
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public String getNom() {
			return nom;
		}
		public void setNom(String nom) {
			this.nom = nom;
		}
	}
}
