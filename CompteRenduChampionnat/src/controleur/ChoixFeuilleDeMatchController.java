package controleur;

import java.io.File;

import modele.Match;
import application.Main;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;
import vue.ChoixFeuilleDeMatchView;

public class ChoixFeuilleDeMatchController {
	ChoixFeuilleDeMatchView vue;
	
	public ChoixFeuilleDeMatchController() {
		vue = new ChoixFeuilleDeMatchView();
		
		vue.getSelectionnerButton().setOnMouseClicked(new SelectionnerButtonActionEvent());
	}
	
	public ChoixFeuilleDeMatchView getVue() {
		return vue;
	}
	
	class SelectionnerButtonActionEvent implements EventHandler<MouseEvent> {

		@Override
		public void handle(MouseEvent event) {
			FileChooser fileChooser = new FileChooser();
			fileChooser.setTitle("Sélectionner une feuille de match");
			fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("YAML", "*.yaml", "*.yml"));
			fileChooser.setInitialDirectory(new File(System.getProperty("user.home"))); 
			File file = fileChooser.showOpenDialog(Main.getStage());
			if (file != null) {
				Match m = Match.chargerFeuilleDeMatch(file);
				if (m != null) {
					Main.showInterfaceCompteRendu(m);
				} else {
					vue.getPasFeuilleDeMatch().show(vue.getSelectionnerButton(), event.getScreenX(), event.getScreenY());
					vue.getPasFeuilleDeMatch().setAutoHide(true);
					vue.getPasFeuilleDeMatch().setConsumeAutoHidingEvents(false);
				}
			}
		}
		
	}
	
}
