package controleur;

import application.Main;
import javafx.animation.FadeTransition;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.util.Duration;

public class ScreensController extends StackPane {
	Scene s;
	double x, y; // Position de la fenetre
	private Button closeButton;
	private Node screen;
	private FadeTransition fadeout;
	private Timeline fadeCloseButtonTL;

	public ScreensController(int width, int height) {
		super();
		this.setOnMousePressed(new WindowEventHandler());
		this.setOnMouseDragged(new WindowEventHandler());

		// Close button
		closeButton = new Button("");
		closeButton.setId("closeButton");
		ScreensController.setAlignment(closeButton, Pos.TOP_RIGHT);
		ScreensController.setMargin(closeButton, new Insets(35, 40, 0, 0));
		closeButton.setOnMouseClicked(new CloseButtonEventHandler());
		closeButton.setOnMouseMoved(new CloseButtonEventHandler());

		this.setOnMouseMoved(new WindowEventHandler());

		s = new Scene(this, width, height, Color.TRANSPARENT);
	}

	public void addScreen(Node screen) {
		this.screen = screen;
	}

	public boolean showScreen() {       
		if (screen != null) {   //screen loaded
			setOpacity(0.0);
			getChildren().add(screen);
			getChildren().add(closeButton);
			Timeline fadeIn = new Timeline(
					new KeyFrame(Duration.ZERO, new KeyValue(this.opacityProperty(), 0.0)),
					new KeyFrame(new Duration(1500), new KeyValue(this.opacityProperty(), 1.0)));
			fadeIn.play();
			return true;
		}
		return false;
	}

	public Scene getS() {
		return s;
	}

	// EVENT
	class WindowEventHandler implements EventHandler<MouseEvent> {
		@Override
		public void handle(MouseEvent event) {
			if (event.getEventType().equals(MouseEvent.MOUSE_PRESSED)) {
				x = s.getWindow().getX() - event.getScreenX();
				y = s.getWindow().getY() - event.getScreenY();
			} 
			else if (event.getEventType().equals(MouseEvent.MOUSE_DRAGGED)) {
				s.getWindow().setX(event.getScreenX() + x);
				s.getWindow().setY(event.getScreenY() + y);
			}
			else if (event.getEventType().equals(MouseEvent.MOUSE_MOVED)) {

				if (fadeCloseButtonTL != null) {
					fadeout.stop();
					fadeCloseButtonTL.stop();
					closeButton.setOpacity(1);
				}

				fadeout = new FadeTransition(new Duration(300), closeButton);
				fadeout.setToValue(0.0);

				fadeCloseButtonTL = new Timeline(
						new KeyFrame(new Duration(1000), new EventHandler<ActionEvent>() {
							@Override
							public void handle(ActionEvent event) {
								fadeout.play();
							}
						})
						);
				fadeCloseButtonTL.play();

			}
		}
	}

	class CloseButtonEventHandler implements EventHandler<MouseEvent> {

		@Override
		public void handle(MouseEvent event) {
			if (event.getEventType().equals(MouseEvent.MOUSE_CLICKED)) {
				Main.getStage().setScene(null);
				Platform.setImplicitExit(true);
				Platform.exit();
			}
			else if (event.getEventType().equals(MouseEvent.MOUSE_MOVED)) {
				if (fadeCloseButtonTL != null) {
					fadeout.stop();
					fadeCloseButtonTL.stop();
					closeButton.setOpacity(1);
				}
			}
		}

	}
}
