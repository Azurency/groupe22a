package controleur;

import java.awt.event.ActionListener;
import java.io.File;

import application.Main;
import controleur.customListCell.ListActionCell;
import controleur.customListCell.ListJoueurCell;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;
import javafx.util.Callback;
import javafx.util.Duration;
import modele.Action;
import modele.Match;
import modele.FeuilleDeMatch.JoueurFDM;
import vue.CompteRenduView;

public class CompteRenduController {
	private CompteRenduView vue;
	private Match match;
	private ObservableList<String> actionsTexte;
	private ObservableList<Action> actions;
	private ObservableList<JoueurFDM> joueurs;
	private ObservableList<JoueurFDM> joueursNonExclus;
	private ObservableList<JoueurFDM> nouvelleListeJoueurs;
	private Clock timer;
	private static final KeyCodeCombination ADDBUT = new KeyCodeCombination(KeyCode.B, KeyCombination.SHORTCUT_DOWN);
	private static final KeyCodeCombination ADDFAUTE = new KeyCodeCombination(KeyCode.F, KeyCombination.SHORTCUT_DOWN);
	private static final KeyCodeCombination ADDTIR = new KeyCodeCombination(KeyCode.T, KeyCombination.SHORTCUT_DOWN);
	private static final KeyCodeCombination ADDENTREE = new KeyCodeCombination(KeyCode.E, KeyCombination.SHORTCUT_DOWN);
	private static final KeyCodeCombination ADDSORTIE = new KeyCodeCombination(KeyCode.S, KeyCombination.SHORTCUT_DOWN);
	private static final KeyCodeCombination ADDPASSE = new KeyCodeCombination(KeyCode.P, KeyCombination.SHORTCUT_DOWN);
	private static final KeyCodeCombination ADDERREUR = new KeyCodeCombination(KeyCode.R, KeyCombination.SHORTCUT_DOWN);
	private static final KeyCodeCombination ADDAVERT = new KeyCodeCombination(KeyCode.Q, KeyCombination.SHORTCUT_DOWN);
	private static final KeyCodeCombination ADDEXC = new KeyCodeCombination(KeyCode.X, KeyCombination.SHORTCUT_DOWN);
	private static final KeyCodeCombination ADDDISQUA = new KeyCodeCombination(KeyCode.D, KeyCombination.SHORTCUT_DOWN);
	private static final KeyCodeCombination ADDTEMPSMORT = new KeyCodeCombination(KeyCode.SEMICOLON, KeyCombination.SHORTCUT_DOWN);
	private static final KeyCodeCombination SWITCHTIMER = new KeyCodeCombination(KeyCode.ENTER, KeyCombination.SHORTCUT_DOWN);

	public CompteRenduController(Match m) {
		timer = new Clock();
		vue = new CompteRenduView();
		match = m;
		actionsTexte = FXCollections.observableArrayList("But", "Faute", "Tir", "Entrée", "Sortie", "Passe décisive", "Erreur",
				"Avertissement", "Exclusion", "Disqualification", "Temps mort");
		nouvelleListeJoueurs = FXCollections.observableArrayList();
		joueurs = FXCollections.observableArrayList();
		actions = FXCollections.observableArrayList();
		joueursNonExclus = FXCollections.observableArrayList();

		vue.getTitreMatch().setText(match.getNomEquipe1() + " - " + match.getNomEquipe2());
		vue.getEtatEquipe1().setText("ex-aequo");
		vue.getEtatEquipe2().setText("ex-aequo");
		vue.getScoreEquipe1().setText(match.getScoreEq1()+"");
		vue.getScoreEquipe2().setText(match.getScoreEq2()+"");

		vue.getActionsTextListView().setItems(actionsTexte);

		vue.getJoueursListView().setItems(joueurs);
		vue.getJoueursListView().setCellFactory(new Callback<ListView<JoueurFDM>, ListCell<JoueurFDM>>() {

			@Override
			public ListCell<JoueurFDM> call(ListView<JoueurFDM> param) {
				return new ListJoueurCell();
			}
		});

		vue.getActionsLV().setCellFactory(new Callback<ListView<Action>, ListCell<Action>>() {

			@Override
			public ListCell<Action> call(ListView<Action> param) {
				return new ListActionCell();
			}
		});

		vue.getActionTimer().setOnAction(new TimerButtonActionEventHandler());
		vue.getActionsTextListView().getSelectionModel().selectedItemProperty().addListener(new ActionsTextChangeListener());

		vue.getRechercheJoueurTextField().setOnMousePressed(new RechercheJoueurTextFieldEventHandler());
		vue.getRechercheJoueurTextField().focusedProperty().addListener(new RechercheJoueurTextFieldFocusChangeListener());
		vue.getRechercheJoueurTextField().textProperty().addListener(new RechercheJoueurTextFieldChangeListener());
		vue.getRechercheJoueurTextField().setOnKeyPressed(new RechercheJoueurTextFieldKeyPressedHandler());
		vue.getJoueursListView().setOnMouseClicked(new JoueurListViewDoubleClickEventHandler());

		vue.getPanePrincipal().setOnKeyPressed(new ShortcutsEventHandler());

	}

	public CompteRenduView getVue() {
		return vue;
	}

	/*
	 * Methodes
	 */

	public void refreshTimerString(int secondes, int minutes) {
		if (minutes == 60) {
			vue.getActionTimer().setText("arrêter le match");
		}
		vue.getTimer().setText(String.format("%02d", minutes) + ":"+ String.format("%02d", secondes));
	}

	public void resetSelectedAction() {
		vue.getActionsTextListView().getSelectionModel().clearSelection();
		joueurs = FXCollections.observableArrayList();
		vue.getJoueursListView().setItems(joueurs);
		vue.getJoueursListView().getSelectionModel().clearSelection();
		vue.getRechercheJoueurTextField().setText("rechercher ...");
		vue.getPanePrincipal().requestFocus();
	}
	
	public void refreshScore() {
		vue.getScoreEquipe1().setText(match.getScoreEq1()+"");
		vue.getScoreEquipe2().setText(match.getScoreEq2()+"");
		if (match.getScoreEq1() > match.getScoreEq2()) {
			vue.getEtatEquipe1().setText("gagnant");
			vue.getEtatEquipe2().setText("perdant");
		} else if (match.getScoreEq1() < match.getScoreEq2()) {
			vue.getEtatEquipe1().setText("perdant");
			vue.getEtatEquipe2().setText("gagnant");
		} else {
			vue.getEtatEquipe1().setText("ex-aequo");
			vue.getEtatEquipe2().setText("ex-aequo");
		}
	}

	/*
	 * EVENEMENTS
	 */

	class TimerButtonActionEventHandler implements EventHandler<ActionEvent> {

		@Override
		public void handle(ActionEvent event) {
			if (vue.getActionTimer().getText().equals("commencer") || vue.getActionTimer().getText().equals("relancer")) {
				timer.startStop();
				vue.getActionTimer().setText("stopper le timer");
			} else if (vue.getActionTimer().getText().equals("stopper le timer")) {
				timer.startStop();
				vue.getActionTimer().setText("relancer");
			} else if (vue.getActionTimer().getText().equals("arrêter le match")) {
				if (!timer.isStopped()) {
					timer.startStop();
				}
				FileChooser fileChooser = new FileChooser();
				fileChooser.setInitialFileName(match.getTextEquipes() + " - Enrichie.yaml");
				fileChooser.setTitle("Enregistrer la feuille de match enrichie");
				fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("YAML", "*.yaml", "*.yml"));
				fileChooser.setInitialDirectory(new File(System.getProperty("user.home"))); 
				File file = fileChooser.showSaveDialog(Main.getStage());
				if (file != null) {
					match.enregistrerMatch(file);
					vue.getActionTimer().setDisable(true);
				}
			}
		}

	}

	class RechercheJoueurTextFieldEventHandler implements EventHandler<MouseEvent> {

		@Override
		public void handle(MouseEvent event) {
			if (vue.getRechercheJoueurTextField().getText().equals("rechercher ...")) {
				vue.getRechercheJoueurTextField().clear();
			}
		}

	}

	class RechercheJoueurTextFieldFocusChangeListener implements ChangeListener<Boolean> {

		@Override
		public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
			if (!newValue) {
				if (vue.getRechercheJoueurTextField().getText().equals("")) {
					vue.getRechercheJoueurTextField().setText("rechercher ...");
				}
			}
		}

	}

	class RechercheJoueurTextFieldChangeListener implements ChangeListener<String> {

		@Override
		public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
			JoueurFDM joueurSelectionne = vue.getJoueursListView().getSelectionModel().getSelectedItem();

			if (oldValue != null && (newValue.length() < oldValue.length()) && !oldValue.equals("rechercher ...")) {
				vue.getJoueursListView().setItems(joueurs);
			}

			// on sépare la chaine pour une meilleur recherche ie pas linéaire, on peu avoir des bouts de mots
			String[] parties = newValue.toLowerCase().split(" ");

			nouvelleListeJoueurs = FXCollections.observableArrayList();
			for (JoueurFDM j : vue.getJoueursListView().getItems()) {
				boolean match = true;
				String rechercheSur = j.getPrenom() + j.getNom() + j.getRole();
				for (String partie : parties) {
					if (! rechercheSur.toLowerCase().contains(partie)) {
						match = false;
						break;
					}
				}
				if (match) {
					nouvelleListeJoueurs.add(j);
				}
			}
			if (!newValue.equals("rechercher ...")) {
				vue.getJoueursListView().setItems(nouvelleListeJoueurs);
				vue.getJoueursListView().getSelectionModel().select(0);
			}
		}

	}

	class RechercheJoueurTextFieldKeyPressedHandler implements EventHandler<KeyEvent> {

		@Override
		public void handle(KeyEvent event) {
			if (event.getCode() == KeyCode.DOWN) {
				Platform.runLater(new Runnable() {
				    @Override
				    public void run() {
				    	vue.getJoueursListView().requestFocus();
				    	Event.fireEvent(vue.getJoueursListView(), event);
				    	vue.getRechercheJoueurTextField().requestFocus();
				    	if (!vue.getRechercheJoueurTextField().getText().equals("rechercher ...")) {
					    	vue.getRechercheJoueurTextField().selectEnd();	
						}
				    }
				});
				event.consume();
			} else if (event.getCode() == KeyCode.UP) {
				Platform.runLater(new Runnable() {
				    @Override
				    public void run() {
				    	vue.getJoueursListView().requestFocus();
				    	Event.fireEvent(vue.getJoueursListView(), event);
				    	vue.getRechercheJoueurTextField().requestFocus();
				    	if (!vue.getRechercheJoueurTextField().getText().equals("rechercher ...")) {
					    	vue.getRechercheJoueurTextField().selectEnd();	
						}
				    }
				});
				event.consume();
			} else if (event.getCode() == KeyCode.ENTER && !timer.isStopped() && vue.getJoueursListView().getSelectionModel().getSelectedItem() != null) {
				switch (vue.getActionsTextListView().getSelectionModel().getSelectedItem()) {
				case "But":
					match.ajouterBut(vue.getJoueursListView().getSelectionModel().getSelectedItem(), vue.getTimer().getText());
					refreshScore();
					break;
				case "Faute":
					match.ajouterFaute(vue.getJoueursListView().getSelectionModel().getSelectedItem(), vue.getTimer().getText());
					break;
				case "Tir":
					match.ajouterTir(vue.getJoueursListView().getSelectionModel().getSelectedItem(), vue.getTimer().getText());
					break;
				case "Entrée":
					match.ajouterEntree(vue.getJoueursListView().getSelectionModel().getSelectedItem(), vue.getTimer().getText());
					break;
				case "Sortie":
					match.ajouterSortie(vue.getJoueursListView().getSelectionModel().getSelectedItem(), vue.getTimer().getText());
					break;	
				case "Passe décisive":
					match.ajouterPasse(vue.getJoueursListView().getSelectionModel().getSelectedItem(), vue.getTimer().getText());
					break;
				case "Erreur":
					match.ajouterErreur(vue.getJoueursListView().getSelectionModel().getSelectedItem(), vue.getTimer().getText());
					break;
				case "Avertissement":
					match.ajouterAvertissement(vue.getJoueursListView().getSelectionModel().getSelectedItem(), vue.getTimer().getText());
					break;
				case "Exclusion":
					match.ajouterExclusion(vue.getJoueursListView().getSelectionModel().getSelectedItem(), vue.getTimer().getText());
					break;
				case "Disqualification":
					match.ajouterDisqualification(vue.getJoueursListView().getSelectionModel().getSelectedItem(), vue.getTimer().getText());
					break;
				case "Temps mort":
					match.ajouterTempsMort(vue.getJoueursListView().getSelectionModel().getSelectedItem(), vue.getTimer().getText());
					if (!timer.isStopped()) {
						timer.startStop();
						vue.getActionTimer().setText("relancer");
					}
					break;
				default:
					break;
				}
				actions = FXCollections.observableArrayList(match.getActions());
				vue.getActionsLV().setItems(null);
				vue.getActionsLV().setItems(actions);
				vue.getActionsLV().scrollTo(actions.get(actions.size() - 1));
				resetSelectedAction();
				event.consume();
			} else if (event.getCode() == KeyCode.ESCAPE) {
				vue.getPanePrincipal().requestFocus();
			}
		}

	}

	class JoueurListViewDoubleClickEventHandler implements EventHandler<MouseEvent> {

		@Override
		public void handle(MouseEvent event) {
			if (event.getClickCount() == 2 && !timer.isStopped()) {
				switch (vue.getActionsTextListView().getSelectionModel().getSelectedItem()) {
				case "But":
					match.ajouterBut(vue.getJoueursListView().getSelectionModel().getSelectedItem(), vue.getTimer().getText());
					refreshScore();
					break;
				case "Faute":
					match.ajouterFaute(vue.getJoueursListView().getSelectionModel().getSelectedItem(), vue.getTimer().getText());
					break;
				case "Tir":
					match.ajouterTir(vue.getJoueursListView().getSelectionModel().getSelectedItem(), vue.getTimer().getText());
					break;
				case "Entrée":
					match.ajouterEntree(vue.getJoueursListView().getSelectionModel().getSelectedItem(), vue.getTimer().getText());
					break;
				case "Sortie":
					match.ajouterSortie(vue.getJoueursListView().getSelectionModel().getSelectedItem(), vue.getTimer().getText());
					break;	
				case "Passe décisive":
					match.ajouterPasse(vue.getJoueursListView().getSelectionModel().getSelectedItem(), vue.getTimer().getText());
					break;
				case "Erreur":
					match.ajouterErreur(vue.getJoueursListView().getSelectionModel().getSelectedItem(), vue.getTimer().getText());
					break;
				case "Avertissement":
					match.ajouterAvertissement(vue.getJoueursListView().getSelectionModel().getSelectedItem(), vue.getTimer().getText());
					break;
				case "Exclusion":
					match.ajouterExclusion(vue.getJoueursListView().getSelectionModel().getSelectedItem(), vue.getTimer().getText());
					break;
				case "Disqualification":
					match.ajouterDisqualification(vue.getJoueursListView().getSelectionModel().getSelectedItem(), vue.getTimer().getText());
					joueursNonExclus.remove(vue.getJoueursListView().getSelectionModel().getSelectedItem());
					break;
				case "Temps mort":
					match.ajouterTempsMort(vue.getJoueursListView().getSelectionModel().getSelectedItem(), vue.getTimer().getText());
					if (!timer.isStopped()) {
						timer.startStop();
						vue.getActionTimer().setText("relancer");
					}
					break;
				default:
					break;
				}
				actions = FXCollections.observableArrayList(match.getActions());
				vue.getActionsLV().setItems(null);
				vue.getActionsLV().setItems(actions);
				vue.getActionsLV().scrollTo(actions.get(actions.size() - 1));
				resetSelectedAction();
				event.consume();
			}
		}

	}

	class ActionsTextChangeListener implements ChangeListener<String> {

		@Override
		public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
			if (newValue != null) {
				if (joueursNonExclus.isEmpty()) {
					joueursNonExclus = FXCollections.observableArrayList(match.getJoueurs());
				}
				if (newValue.equals("Temps mort")) {
					joueurs = FXCollections.observableArrayList(match.getEntraineurs());
					vue.getJoueursListView().setItems(joueurs);
				} else {
					joueurs = joueursNonExclus;
					vue.getJoueursListView().setItems(joueurs);
				}
				Platform.runLater(new Runnable() {
					@Override
					public void run() {
						vue.getRechercheJoueurTextField().requestFocus();
					}
				});
			}
		}

	}

	class ShortcutsEventHandler implements EventHandler<KeyEvent> {

		@Override
		public void handle(KeyEvent event) {
			if (joueursNonExclus.isEmpty()) {
				joueursNonExclus = FXCollections.observableArrayList(match.getJoueurs());
			}

			if (ADDBUT.match(event)) {
				vue.getActionsTextListView().getSelectionModel().select("But");
				joueurs = joueursNonExclus;
				vue.getJoueursListView().setItems(joueurs);
			} else if (ADDFAUTE.match(event)) {
				vue.getActionsTextListView().getSelectionModel().select("Faute");
				joueurs = joueursNonExclus;
				vue.getJoueursListView().setItems(joueurs);
			} else if (ADDTIR.match(event)) {
				vue.getActionsTextListView().getSelectionModel().select("Tir");
				joueurs = joueursNonExclus;
				vue.getJoueursListView().setItems(joueurs);
			} else if (ADDENTREE.match(event)) {
				vue.getActionsTextListView().getSelectionModel().select("Entrée");
				joueurs = joueursNonExclus;
				vue.getJoueursListView().setItems(joueurs);
			} else if (ADDSORTIE.match(event)) {
				vue.getActionsTextListView().getSelectionModel().select("Sortie");
				joueurs = joueursNonExclus;
				vue.getJoueursListView().setItems(joueurs);
			} else if (ADDPASSE.match(event)) {
				vue.getActionsTextListView().getSelectionModel().select("Passe décisive");
				joueurs = joueursNonExclus;
				vue.getJoueursListView().setItems(joueurs);
			} else if (ADDERREUR.match(event)) {
				vue.getActionsTextListView().getSelectionModel().select("Erreur");
				joueurs = joueursNonExclus;
				vue.getJoueursListView().setItems(joueurs);
			} else if (ADDAVERT.match(event)) {
				vue.getActionsTextListView().getSelectionModel().select("Avertissement");
				joueurs = joueursNonExclus;
				vue.getJoueursListView().setItems(joueurs);
			} else if (ADDEXC.match(event)) {
				vue.getActionsTextListView().getSelectionModel().select("Exclusion");
				joueurs = joueursNonExclus;
				vue.getJoueursListView().setItems(joueurs);
			} else if (ADDDISQUA.match(event)) {
				vue.getActionsTextListView().getSelectionModel().select("Disqualification");
				joueurs = joueursNonExclus;
				vue.getJoueursListView().setItems(joueurs);
			} else if (ADDTEMPSMORT.match(event)) {
				vue.getActionsTextListView().getSelectionModel().select("Temps mort");
				joueurs = FXCollections.observableArrayList(match.getEntraineurs());
				vue.getJoueursListView().setItems(joueurs);
			} else if (SWITCHTIMER.match(event)) {
				if ((timer.getMinutes() < 60)) {
					timer.startStop();
					if (!timer.isStopped()) {
						vue.getActionTimer().setText("stopper le timer");
					} else {
						vue.getActionTimer().setText("relancer");
					}
				}
			}
		}

	}

	public class Clock {
		private int elapsedMillis = 0;
		private int lastClockTime = 0;
		private Timeline time = new Timeline();

		private int seconds;
		private int mins ;

		public Clock() {
			configureTimeline();
		}

		private void configureTimeline() {
			time.setCycleCount(Timeline.INDEFINITE);
			KeyFrame keyFrame = new KeyFrame(Duration.millis(47), new EventHandler<ActionEvent>() {
				public void handle(ActionEvent event) {
					calculate();
				}
			});
			time.getKeyFrames().add(keyFrame);
		}

		public void calculate() {
			if (lastClockTime == 0) {
				lastClockTime = (int) System.currentTimeMillis();
			}

			int now = (int) System.currentTimeMillis();
			//int delta = now - lastClockTime;
			int delta = 3000; //pour voir le passage à 60 minutes
			
			elapsedMillis += delta;

			seconds = (elapsedMillis / 1000) % 60;
			mins = (elapsedMillis / 60000);

			refreshTimerString(seconds, mins);

			lastClockTime = now;
		}

		public void startStop() {
			if (time.getStatus() != Animation.Status.STOPPED) {
				time.stop();
				lastClockTime = 0;
			} else {
				time.play();
			}
		}

		public void stopReset() {
			if (time.getStatus() != Animation.Status.STOPPED) {
				time.stop();
				lastClockTime = 0;
			} else {
				lastClockTime = 0;
				elapsedMillis = 0;
			}
		}

		public boolean isStopped() {
			return time.getStatus() == Animation.Status.STOPPED;
		}

		public int getMinutes() {
			return mins;
		}

		public int getSecondes() {
			return seconds;
		}

	}
}
