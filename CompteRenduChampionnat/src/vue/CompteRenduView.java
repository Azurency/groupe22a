package vue;

import modele.Action;
import modele.FeuilleDeMatch.JoueurFDM;
import controleur.ScreensController;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;

public class CompteRenduView {
	private BorderPane panePrincipal;
	private ListView<String> actionsTexteListView;
	private GridPane panneauMilieu;
	private TextField rechercheJoueurTextField;
	private ListView<JoueurFDM> joueursListView;
	private Text titreMatch;
	private Text etatEquipe1;
	private Text etatEquipe2;
	private Text scoreEquipe1;
	private Text scoreEquipe2;
	private ListView<Action> actionsLV;
	private Text timer;
	private Button actionTimer;

	public CompteRenduView() {
		initPanePrincipal();
		initWindowTitle();
		initPanneauGauche();
		initContent();
	}

	private void initPanePrincipal() {
		panePrincipal = new BorderPane();
		panePrincipal.getStyleClass().add("contentPane");
		ScreensController.setMargin(panePrincipal, new Insets(25, 25, 25, 25));
	}

	private void initWindowTitle() {
		Text titre = new Text("compte rendu de match");
		titre.getStyleClass().add("titreFenetre");
		StackPane titrePane = new StackPane(titre);
		titrePane.setId("titrePane");
		BorderPane.setMargin(titrePane, new Insets(0, 15, 10, 15));
		StackPane.setAlignment(titre, Pos.CENTER);
		StackPane.setMargin(titre, new Insets(7, 0, 7, 0));

		panePrincipal.setTop(titrePane);
	}

	private void initPanneauGauche() {
		// Panneau Gauche
		GridPane panneauGauche = new GridPane();
		//panneauGauche.setGridLinesVisible(true);
		panneauGauche.setId("panneauGauche");
		BorderPane.setMargin(panneauGauche, new Insets(0, 10, 0, 30));
		
		TitledPane equipesPane = new TitledPane();
		equipesPane.setId("actionsPane");
		equipesPane.setPrefHeight(TitledPane.USE_COMPUTED_SIZE);
		equipesPane.setCollapsible(false);
		equipesPane.setExpanded(true);

		// formatage du titre du TitledPane
		Text titledPaneTitleSigne = new Text("  ");
		titledPaneTitleSigne.getStyleClass().add("signeEquipeTitledPane");
		Text titledPaneTitleTexte = new Text("Actions");
		titledPaneTitleTexte.getStyleClass().add("titreCompteRendu");
		TextFlow titledPaneTitle = new TextFlow(titledPaneTitleSigne, titledPaneTitleTexte);
		equipesPane.setGraphic(titledPaneTitle);

		AnchorPane equipesAnchorPane = new AnchorPane();
		equipesAnchorPane.setId("equipesAnchorPane");
		equipesAnchorPane.setPadding(new Insets(0));
		equipesAnchorPane.setPrefWidth(AnchorPane.USE_COMPUTED_SIZE);
		equipesAnchorPane.setPrefHeight(AnchorPane.USE_COMPUTED_SIZE);
		actionsTexteListView = new ListView<String>();
		actionsTexteListView.setPrefWidth(140);
		actionsTexteListView.setPrefHeight(565);
		AnchorPane.setLeftAnchor(actionsTexteListView, 0.0);
		AnchorPane.setRightAnchor(actionsTexteListView, 0.0);
		AnchorPane.setTopAnchor(actionsTexteListView, 0.0);
		AnchorPane.setBottomAnchor(actionsTexteListView, 0.0);
		equipesAnchorPane.getChildren().add(actionsTexteListView);

		equipesPane.setContent(equipesAnchorPane);
		equipesPane.setPrefHeight(TitledPane.USE_COMPUTED_SIZE);
		panneauGauche.add(equipesPane, 0, 0);

		panePrincipal.setLeft(panneauGauche);
	}
	
	private void initContent() {
		panneauMilieu = new GridPane();
		panneauMilieu.getStyleClass().add("panneauMilieu");
		
		GridPane barreJoueurs = new GridPane();
		barreJoueurs.setId("barreMilieu");
		barreJoueurs.setMinWidth(225);
		GridPane.setMargin(barreJoueurs, new Insets(0, 10, 0, 0));

		HBox rechercheJoueurBox = new HBox();
		rechercheJoueurBox.getStyleClass().add("recherche-box");
		rechercheJoueurTextField = new TextField("rechercher ...");
		rechercheJoueurTextField.getStyleClass().add("champ-recherche");
		HBox.setHgrow(rechercheJoueurTextField, Priority.ALWAYS);
		Text iconJoueurTextField = new Text("");
		iconJoueurTextField.getStyleClass().add("signe-recherche");
		HBox.setMargin(iconJoueurTextField, new Insets(5, 5, 0, 0));
		rechercheJoueurBox.getChildren().addAll(iconJoueurTextField, rechercheJoueurTextField);
		GridPane.setMargin(rechercheJoueurBox, new Insets(0, 0, 10, 0));
		barreJoueurs.add(rechercheJoueurBox, 0, 0);
		
		Text instruction = new Text("double cliquer la personne :");
		GridPane.setMargin(instruction, new Insets(0, 0, 10, 5));
		instruction.getStyleClass().add("instruction");
		barreJoueurs.add(instruction, 0, 1);

		joueursListView = new ListView<JoueurFDM>();
		joueursListView.setPrefWidth(225);
		joueursListView.setPrefHeight(540);
		Text placeholder = new Text("aucune action sélectionée");
		placeholder.getStyleClass().add("placeholder");
		joueursListView.setPlaceholder(placeholder);
		GridPane.setMargin(joueursListView, new Insets(0, 0, 10, 0));
		barreJoueurs.add(joueursListView, 0, 2);

		panneauMilieu.add(barreJoueurs, 0, 0);
		
		GridPane infoMatch = new GridPane();
		infoMatch.setPrefWidth(500);
		GridPane.setMargin(infoMatch, new Insets(0, 0, 10, 0));
		infoMatch.getStyleClass().add("infoMatch");
		
		GridPane recapMatch = new GridPane();
		recapMatch.getStyleClass().add("recapMatch");
		recapMatch.setPrefWidth(500);
		GridPane.setMargin(recapMatch, new Insets(0, 0, 10, 0));
		ColumnConstraints column1 = new ColumnConstraints();
		column1.setPercentWidth(50);
		ColumnConstraints column2 = new ColumnConstraints();
		column2.setPercentWidth(50);
		recapMatch.getColumnConstraints().addAll(column1, column2);
		timer = new Text("00:00");
		timer.getStyleClass().add("timer");
		GridPane.setColumnSpan(timer, GridPane.REMAINING);
		GridPane.setHalignment(timer, HPos.CENTER);
		GridPane.setMargin(timer, new Insets(0, 0, 5, 0));
		recapMatch.add(timer, 0, 0);
		actionTimer = new Button("commencer");
		actionTimer.getStyleClass().add("timerButton");
		GridPane.setColumnSpan(actionTimer, GridPane.REMAINING);
		GridPane.setHalignment(actionTimer, HPos.CENTER);
		GridPane.setMargin(actionTimer, new Insets(0, 0, 5, 0));
		recapMatch.add(actionTimer, 0, 1);
		titreMatch = new Text();
		titreMatch.getStyleClass().add("titreMatch");
		GridPane.setColumnSpan(titreMatch, GridPane.REMAINING);
		GridPane.setHalignment(titreMatch,HPos.CENTER);
		recapMatch.add(titreMatch, 0, 2);
		etatEquipe1 = new Text();
		etatEquipe1.getStyleClass().add("etatEquipe");
		etatEquipe2 = new Text();
		etatEquipe2.getStyleClass().add("etatEquipe");
		GridPane.setHalignment(etatEquipe1, HPos.CENTER);
		GridPane.setHalignment(etatEquipe2, HPos.CENTER);
		recapMatch.add(etatEquipe1, 0, 3);
		recapMatch.add(etatEquipe2, 1, 3);
		scoreEquipe1 = new Text();
		scoreEquipe1.getStyleClass().add("scoreMatch");
		scoreEquipe2 = new Text();
		scoreEquipe2.getStyleClass().add("scoreMatch");
		GridPane.setHalignment(scoreEquipe1, HPos.CENTER);
		GridPane.setHalignment(scoreEquipe2, HPos.CENTER);
		recapMatch.add(scoreEquipe1, 0, 4);
		recapMatch.add(scoreEquipe2, 1, 4);
		infoMatch.add(recapMatch, 0, 0);
		
		actionsLV = new ListView<Action>();
		actionsLV.getStyleClass().addAll("customListView", "actionLV");
		infoMatch.add(actionsLV, 0, 1);
		
		panneauMilieu.add(infoMatch, 2, 0);
		
		panePrincipal.setCenter(panneauMilieu);
		
	}

	public Text getTitreMatch() {
		return titreMatch;
	}

	public Text getEtatEquipe1() {
		return etatEquipe1;
	}

	public Text getEtatEquipe2() {
		return etatEquipe2;
	}

	public Text getScoreEquipe1() {
		return scoreEquipe1;
	}

	public Text getScoreEquipe2() {
		return scoreEquipe2;
	}

	public ListView<Action> getActionsLV() {
		return actionsLV;
	}

	public BorderPane getPanePrincipal() {
		return panePrincipal;
	}

	public ListView<String> getActionsTextListView() {
		return actionsTexteListView;
	}

	public TextField getRechercheJoueurTextField() {
		return rechercheJoueurTextField;
	}

	public ListView<JoueurFDM> getJoueursListView() {
		return joueursListView;
	}

	public Text getTimer() {
		return timer;
	}

	public Button getActionTimer() {
		return actionTimer;
	}

}
