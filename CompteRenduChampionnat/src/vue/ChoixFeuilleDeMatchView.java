package vue;

import controleur.ScreensController;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.TextFlow;

public class ChoixFeuilleDeMatchView {
	private GridPane panePrincipal;
	private Button selectionnerButton;
	private Tooltip pasFeuilleDeMatch;
	
	public ChoixFeuilleDeMatchView() {
		panePrincipal = new GridPane();
		panePrincipal.getStyleClass().addAll("contentPane", "panePrincipalPadding");
		ScreensController.setMargin(panePrincipal, new Insets(25, 25, 25, 25));
		
		Text titre = new Text("compte rendu de match");
		titre.getStyleClass().add("titreCompteRendu");
		GridPane.setHalignment(titre, HPos.CENTER);
		GridPane.setMargin(titre, new Insets(0, 0, 15, 0));
		panePrincipal.add(titre, 0, 0);
		
		Text explications = new Text("Veuillez sélectionner le match à commenter pour commencer la saisie du compte rendu.");
		explications.getStyleClass().add("explications");
		TextFlow explicationFlow = new TextFlow(explications);
		explicationFlow.setTextAlignment(TextAlignment.JUSTIFY);
		GridPane.setHalignment(explicationFlow, HPos.CENTER);
		GridPane.setMargin(explicationFlow, new Insets(0, 0, 25, 0));
		panePrincipal.add(explicationFlow, 0, 1);
		
		selectionnerButton = new Button();
		selectionnerButton.setText("sélectionner une feuille de match");
		selectionnerButton.getStyleClass().add("selectionnerButton");
		GridPane.setHalignment(selectionnerButton, HPos.CENTER);
		panePrincipal.add(selectionnerButton, 0, 2);
		
		pasFeuilleDeMatch = new Tooltip("Ce fichier n'est pas une feuille de match");
	}

	public GridPane getPanePrincipal() {
		return panePrincipal;
	}

	public Button getSelectionnerButton() {
		return selectionnerButton;
	}

	public Tooltip getPasFeuilleDeMatch() {
		return pasFeuilleDeMatch;
	}

}
