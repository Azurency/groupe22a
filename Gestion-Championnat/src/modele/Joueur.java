package modele;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Data;
import javafx.scene.chart.XYChart.Series;

public class Joueur implements Serializable {

	private static final long serialVersionUID = -1579106764477695230L; // utilisé par serializable

	private static int idMax;

	private int     		 	id;
	private int      		 	numLic;
	private int 			 	num;
	private String   		 	nom;
	private String   		 	prenom;
	private boolean  		 	professionnel;
	private LocalDate 	     	dateNais;
	private String    		 	role;
	private Equipe   		 	equipe;
	private ArrayList<String>	ancienneEquipes;
	private boolean			 	equipeDejaChangee;

	private static HashMap<Integer, Joueur> mapJoueurs = new HashMap<Integer, Joueur>();

	/**
	 * @deprecated
	 */
	public Joueur(){
		id = -1;
		numLic = -1;
		nom = "NoName";
		num = -1;
		prenom = "NoName";
		professionnel = false;
		dateNais = null;
		role = "non Definie";		
		equipe = null;
		ancienneEquipes = new ArrayList<String>();
		ancienneEquipes.add("aucun");
		equipeDejaChangee = false;
	}

	public Joueur(JoueurCharge j){
		this.id = idMax++;
		this.numLic = j.getNumLic();
		this.num = j.getNum();
		this.nom = j.getNom();
		this.prenom = j.getPrenom();
		this.professionnel = j.isProfessionnel();
		this.dateNais = LocalDate.parse(j.getDateNais());

		this.role = j.getRole();
		this.equipe = null;
		this.ancienneEquipes = j.getAncienneEquipes();
		equipeDejaChangee = false;
		Joueur.mapJoueurs.put(this.id, this);
	}

	public Joueur(int id, int numLic, int num, String nom, String prenom,
			boolean professionnel, LocalDate dateNais, String role,
			Equipe equipe, ArrayList<String> ancienneEquipes) {
		if(id>=Joueur.idMax){
			idMax = id +1;			
		}
		this.id = id;
		this.numLic = numLic;
		this.num = num;
		this.nom = nom;
		this.prenom = prenom;
		this.professionnel = professionnel;
		this.dateNais = dateNais;
		this.role = role;
		this.equipe = equipe;
		this.ancienneEquipes = ancienneEquipes;
		Joueur.mapJoueurs.put(this.id, this);
		equipeDejaChangee = false;

	}

	public Joueur(int id, int numLic,int num, String nom, String prenom,
			boolean professionnel, LocalDate dateNais, String role, Equipe equipe) {
		if(id>=Joueur.idMax){
			idMax = id +1;			
		}
		this.id = id;
		this.numLic = numLic;
		this.num = num;
		this.nom = nom;
		this.prenom = prenom;
		this.professionnel = professionnel;
		this.dateNais = dateNais;
		this.role = role;
		this.equipe = equipe;
		ancienneEquipes = new ArrayList<String>();
		ancienneEquipes.add("aucun");
		Joueur.mapJoueurs.put(this.id, this);
		equipeDejaChangee = false;
	}
	public Joueur(int numLic, String nom, String prenom,
			boolean professionnel, LocalDate dateNais, String role, Equipe equipe) {
		this.id = idMax++;
		this.numLic = numLic;
		this.nom = nom;
		this.num = -1;
		this.prenom = prenom;
		this.professionnel = professionnel;
		this.dateNais = dateNais;
		this.role = role;
		this.equipe = equipe;
		ancienneEquipes = new ArrayList<String>();
		ancienneEquipes.add("aucun");
		Joueur.mapJoueurs.put(this.id, this);
		equipeDejaChangee = false;
	}

	public Joueur(int numLic, String nom, String prenom,
			boolean professionnel, LocalDate dateNais, String role, Equipe equipe, ArrayList<String> ancienneEquipes) {
		this.id = idMax++;
		this.numLic = numLic;
		this.nom = nom;
		this.num = -1;
		this.prenom = prenom;
		this.professionnel = professionnel;
		this.dateNais = dateNais;
		this.role = role;
		this.equipe = equipe;
		this.ancienneEquipes = ancienneEquipes;
		Joueur.mapJoueurs.put(this.id, this);
		equipeDejaChangee = false;
	}

	public static ArrayList<String> makeAncienneEquipeFromString(String s) {
		return new ArrayList<String>(Arrays.asList(s.split(", ")));
	}

	public void removeLastAncienneEquipe(){
		if (ancienneEquipes.size() != 1) {
			ancienneEquipes.remove(ancienneEquipes.size() - 1);
		}
		else {
			ancienneEquipes.remove(ancienneEquipes.size() - 1);
			ancienneEquipes.add("aucun");
		}
	}

	public String getLastAncienneEquipe(){
		return ancienneEquipes.get(ancienneEquipes.size() - 1);
	}

	public void addAncienneEquipe(String equipe){
		if (!equipeDejaChangee && !equipe.equals("Nouvelle equipe") && !equipe.equals("Sans équipe")) {
			if(ancienneEquipes.get(0).equals("aucun")){
				ancienneEquipes.remove(0);
				ancienneEquipes.add(equipe);
				equipeDejaChangee = true;
			}
			else{
				ancienneEquipes.add(equipe);
				equipeDejaChangee = true;
			}
		}
		else if (this.equipe.getNom().equals(getLastAncienneEquipe())) {
			removeLastAncienneEquipe();
			equipeDejaChangee = false;
		}
	}

	public void changerEquipe(Equipe e){
		String nomEquipe = equipe.getNom();
		this.equipe.getJoueurs().remove(this);
		this.equipe = e;
		e.addJoueur(this);
		this.addAncienneEquipe(nomEquipe);
	}



	// faire la requete pour obtenir le nbr de but total du joueur
	public int getNbrButsTotal() {
		if(!ConnectionBD.getStatement().isConnecte()){
			ConnectionBD.getStatement().seCo();			
		}
		try {
			Statement s = ConnectionBD.getStatement().getConnection().createStatement();
			ResultSet rs = s.executeQuery("select sum(nbButs) from Participer where idJ = "+this.id);
			if (rs.next()) {
				int res = rs.getInt(1);
				s.close();
				rs.close();
				return res;
			}
			s.close();
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return -1;
	}


	public Series<String, Number> getButParMatch(){
		Series<String, Number> serie = new XYChart.Series<>();
		serie.setName("buts");
		try {
			Statement s = ConnectionBD.getStatement().getConnection().createStatement();
			ResultSet rs = s.executeQuery("SELECT to_char(match.dateM,'DD/MM/YYYY'), p.nbButs "
					+ "FROM Match match, Participer p "
					+ "WHERE p.idJ="+this.id+" AND match.idM=p.idM order by match.dateM");

			while(rs.next()){
				String date = rs.getString(1);
				int nbBut = rs.getInt("nbButs");
				serie.getData().add(new Data<String, Number>(date, nbBut));
			}
			s.close();
			rs.close();
			return serie;
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;

	}
	public Series<String, Number> getPasseParMatch(){
		Series<String, Number> serie = new XYChart.Series<>();
		serie.setName("passes");
		try {
			Statement s = ConnectionBD.getStatement().getConnection().createStatement();
			ResultSet rs = s.executeQuery("select to_char(match.dateM,'DD/MM/YYYY') "
					+", p.nbPassesDecisives from Match match, Participer p"+
					" where p.idJ ="+this.id+" and match.idM = p.idM order by match.dateM");

			while(rs.next()){
				String date = rs.getString(1);
				int nbPasse = rs.getInt("nbPassesDecisives");
				serie.getData().add(new Data<String, Number>(date, nbPasse));
			}
			s.close();
			rs.close();

			return serie;
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;

	}
	public Series<String, Number> getFauteParMatch(){
		Series<String, Number> serie = new XYChart.Series<>();
		serie.setName("fautes");
		try {
			Statement s = ConnectionBD.getStatement().getConnection().createStatement();
			ResultSet rs = s.executeQuery("select to_char(match.dateM,'DD/MM/YYYY') "
					+", p.nbFautes from Match match, Participer p"+
					" where p.idJ ="+this.id+" and match.idM = p.idM order by match.dateM");

			while(rs.next()){
				String date = rs.getString(1);
				int nbPasse = rs.getInt("nbFautes");
				serie.getData().add(new Data<String, Number>(date, nbPasse));
			}
			s.close();
			rs.close();
			return serie;
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;

	}


	public int getNbrPassesDecisivesTotales(){
		if(!ConnectionBD.getStatement().isConnecte()){
			ConnectionBD.getStatement().seCo();			
		}
		try {
			Statement s = ConnectionBD.getStatement().getConnection().createStatement();

			ResultSet rs = s.executeQuery("select sum(nbPassesDecisives) from Participer where idJ ="+this.id);
			if (rs.next()) {
				int res =rs.getInt(1); 
				s.close();
				rs.close();
				return res;
			}
			s.close();
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return -1;
	}
	public int getNbrFautesTotales() {
		try {
			Statement s = ConnectionBD.getStatement().getConnection().createStatement();
			ResultSet rs = s.executeQuery("select sum(nbFautes) from Participer where idJ ="+this.id);
			if (rs.next()) {
				int res = rs.getInt(1);
				s.close();
				rs.close();
				return res;
			}
			s.close();
			rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return -1;
	}

	public double getMoyenneButsParMatch() {
		try {
			Statement s = ConnectionBD.getStatement().getConnection().createStatement();
			ResultSet rs = s.executeQuery("select avg(nbButs) from Participer where idJ ="+this.id);
			if (rs.next()) {
				double res = rs.getDouble(1);
				s.close();
				rs.close();
				return res;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return -1.0;
	}




	public void supprJoueurBD(){
		try {
			Statement s = ConnectionBD.getStatement().getConnection().createStatement();
			s.execute("delete from joueur where idJ = "+this.getId());
			s.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}



	//Nececite que les equipes soit deja crée !!!
	public static ArrayList<Joueur> getAllJoueurBD() throws SQLException{

		ArrayList<Joueur> res = new ArrayList<Joueur>();

		if(!ConnectionBD.getStatement().isConnecte()){
			ConnectionBD.getStatement().seCo();			
		}
		Statement s = ConnectionBD.getStatement().getConnection().createStatement();

		ResultSet r = s.executeQuery("select * from Joueur");
		while(r.next()){
			int idJ = r.getInt("idJ");
			if(idJ> Joueur.idMax){
				Joueur.idMax = idJ+1;
			}
			int idE = r.getInt("idE");
			int num = r.getInt("num");
			int numLicJ = r.getInt("numLicJ");
			String nomJ = r.getString("nomJ");
			String prenomJ = r.getString("prenomJ");
			char profJ = r.getString("profJ").charAt(0);
			boolean prof = false;
			if(profJ=='O'){
				prof = true;
			}
			else if(profJ=='N'){
				prof=false;
			}
			// todo ajout d'une exception

			String date = r.getString("datenaisJ");
			String[] dates = date.split("-");
			LocalDate datenaiss = LocalDate.of(Integer.parseInt(dates[0]),
					Integer.parseInt(dates[1]),
					Integer.parseInt(dates[2].split(" ")[0]));

			//r.getString("datenaisJ")
			char roleC = r.getString("roleJ").charAt(0);
			String role = "non Definie";
			switch(roleC){
			case 'J':
				role = "joueur";
				break;
			case 'E':
				role = "entraineur";
				break;
			case 'A':
				role = "arbitre";
				break;
			}
			String ancienneEquipes = r.getString("anciennesEquipe");
			if(ancienneEquipes ==null){
				ancienneEquipes = "aucun";
			}
			Joueur j = new Joueur(idJ,numLicJ,num,nomJ,prenomJ,prof,datenaiss
					,role,Equipe.getMapEquipes().get(idE),makeAncienneEquipeFromString(ancienneEquipes));
			Equipe.getMapEquipes().get(idE).addJoueur(j);
			res.add(j);

		}



		return res;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		result = prime * result + ((prenom == null) ? 0 : prenom.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Joueur other = (Joueur) obj;
		if (id != other.id)
			return false;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		if (prenom == null) {
			if (other.prenom != null)
				return false;
		} else if (!prenom.equals(other.prenom))
			return false;
		if (dateNais == null) {
			if (other.dateNais != null)
				return false;
		} else if (!dateNais.equals(other.dateNais))
			return false;
		return true;
	}

	public boolean equalsWithoutId(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Joueur other = (Joueur) obj;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		if (num != other.num)
			return false;
		if (numLic != other.numLic)
			return false;
		if (prenom == null) {
			if (other.prenom != null)
				return false;
		} else if (!prenom.equals(other.prenom))
			return false;
		if (professionnel != other.professionnel)
			return false;
		if (role == null) {
			if (other.role != null)
				return false;
		} else if (!role.equals(other.role))
			return false;
		return true;
	}



	//Getters-Setters
	public static int getIdMax() {
		return idMax;
	}
	public int getId() {
		return id;
	}
	public int getNumLic() {
		return numLic;
	}
	public void setNumLic(int numLic) {
		this.numLic = numLic;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public boolean isProfessionnel() {
		return professionnel;
	}
	public void setProfessionnel(boolean professionnel) {
		this.professionnel = professionnel;
	}
	public LocalDate getDateNais() {
		return dateNais;
	}
	public void setDateNais(LocalDate dateNais) {
		this.dateNais = dateNais;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public Equipe getEquipe() {
		return equipe;
	}
	public void setEquipe(Equipe equipe) {
		this.equipe = equipe;
	}
	public void setAncienneEquipes(ArrayList<String> ancienneEquipes) {
		this.ancienneEquipes = ancienneEquipes;
	}
	public String getAncienneEquipesString() {
		return String.join(", ",ancienneEquipes);
	}
	public ArrayList<String> getAncienneEquipes() {
		return ancienneEquipes;
	}
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public boolean isEquipeDejaChangee() {
		return equipeDejaChangee;
	}
	public void setEquipeDejaChangee(boolean equipeDejaChangee) {
		this.equipeDejaChangee = equipeDejaChangee;
	}
	public static HashMap<Integer, Joueur> getMapJoueurs() {
		return mapJoueurs;
	}

	/**
	 * Joueur chargeable en YAML pour importation
	 * @author Sebastien Gedeon
	 *
	 */
	public static class JoueurCharge{
		private int      		 	numLic;
		private int 			 	num;
		private String   		 	nom;
		private String   		 	prenom;
		private boolean  		 	professionnel;

		private String	 	     	dateNais;

		private String    		 	role;
		private ArrayList<String>	ancienneEquipes;

		public JoueurCharge(){
			this.numLic=0;
			this.num=0;
			this.nom="";
			this.prenom = "";
			this.professionnel= true;
			this.dateNais = "";
			this.role="";
			this.ancienneEquipes = new ArrayList<String>();
		}

		public JoueurCharge(Joueur j){
			this.numLic = j.getNumLic();
			this.num = j.getNum();
			this.nom = j.getNom();
			this.prenom = j.getPrenom();
			this.professionnel = j.isProfessionnel();

			this.dateNais = j.getDateNais().toString();

			this.role = j.getRole();
			this.ancienneEquipes = j.getAncienneEquipes();
		}


		public int getNumLic() {
			return numLic;
		}
		public void setNumLic(int numLic) {
			this.numLic = numLic;
		}
		public int getNum() {
			return num;
		}
		public void setNum(int num) {
			this.num = num;
		}
		public String getNom() {
			return nom;
		}
		public void setNom(String nom) {
			this.nom = nom;
		}
		public String getPrenom() {
			return prenom;
		}
		public void setPrenom(String prenom) {
			this.prenom = prenom;
		}
		public boolean isProfessionnel() {
			return professionnel;
		}
		public void setProfessionnel(boolean professionnel) {
			this.professionnel = professionnel;
		}
		public String getDateNais() {
			return dateNais;
		}
		public void setDateNais(String dateNais) {
			this.dateNais = dateNais;
		}
		public String getRole() {
			return role;
		}
		public void setRole(String role) {
			this.role = role;
		}
		public ArrayList<String> getAncienneEquipes() {
			return ancienneEquipes;
		}
		public void setAncienneEquipes(ArrayList<String> ancienneEquipes) {
			this.ancienneEquipes = ancienneEquipes;
		}



	}


}
