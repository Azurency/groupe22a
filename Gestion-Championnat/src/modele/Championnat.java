package modele;

import java.lang.reflect.Array;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * <b>Le Championnat represente la totalité du championnat</b>
 * 
 * @author Sebastien Gedeon
 *
 */
public class Championnat {

	private String nom;
	private Journee[] journees;
	private Equipe[] equipes;

	/**
	 * @deprecated Utilise le constructeur utilisant des parametre
	 */
	public Championnat(){
		setNom("NoName");
		setJournees(null);
		setEquipes(null);
	}
	/**
	 * Instancie Championnat et repartie les matchs.
	 *  
	 * @param nom
	 * 		Le nom du Championnat
	 * @param journees
	 * 		La liste des Journees du Championnat
	 * @param equipes
	 * 		La liste des Equipes du Championnat
	 * @param premiereCreation
	 * 		Boolean true si c'est la premiere creation sinon false
	 */
	public Championnat(String nom, Journee[] journees, Equipe[] equipes, boolean premiereCreation) {
		this.nom = nom;
		this.journees = journees;
		this.equipes = equipes;
		if(premiereCreation){
			this.inserJourneeBD();
			this.repartirMatch();
		}
		else{
			this.chargerMatchFromBD();
		}
	}

	/**
	 * Charge tout les matchs depuis la Base de Donnée et les rentre dans leurs journees.
	 */
	private void chargerMatchFromBD(){
		try {			
			Statement s = ConnectionBD.getStatement().getConnection().createStatement();

			ResultSet rs = s.executeQuery("select * from Match");
			while(rs.next()){
				int idM = rs.getInt("idM");
				int idE1 = rs.getInt("idE1");
				int idE2 = rs.getInt("idE2");

				String date = rs.getString("dateM");
				String[] dates = date.split("-");
				LocalDate dateM = LocalDate.of(Integer.parseInt(dates[0]),
						Integer.parseInt(dates[1]),
						Integer.parseInt(dates[2].split(" ")[0]));

				int scoreLocaux = rs.getInt("scoreLocaux");
				int scoreVisiteur = rs.getInt("scoreVisiteur");
				int numJour = rs.getInt("numJour");
				char charge = rs.getString("charge").charAt(0);
				boolean isCharge = false;
				if(charge == 'O'){
					isCharge = true;
				}

				Match m = new Match(idM, Equipe.getMapEquipes().get(idE1),scoreLocaux
						, Equipe.getMapEquipes().get(idE2), scoreVisiteur, dateM
						, journees[numJour], isCharge);

				ArrayList<Action> actions = new ArrayList<>();
				if(isCharge){
					Statement s2 = ConnectionBD.getStatement().getConnection().createStatement();
					ResultSet rs2 = s2.executeQuery("select * from Action where idM = "+idM);
					while(rs2.next()){
						int idA = rs2.getInt("idA");
						int idJ = rs2.getInt("idJ");
						int idM2 = rs2.getInt("idM");
						String timer = rs2.getString("timer");
						String action = rs2.getString("action");
						String joueur = rs2.getString("joueur");
						String equipe = rs2.getString("equipe");

						actions.add(new Action(idA, idM, idJ, equipe, timer, action, joueur));
					}
				}
				m.setActions(actions);

				this.journees[numJour].addMatch(m);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	/**
	 * Methode static qui recupere le Championnat en cours depuis la Base de donnee.
	 * 
	 * @return le championnat en cours
	 */
	public static Championnat chargerChampionnat(){
		Journee[] jours = Journee.getJourneeFromBD();
		ArrayList<Equipe> equipesA = Equipe.getEquipeFromBD();
		equipesA.remove(0);
		Equipe[] equipes = equipesA.toArray(new Equipe[equipesA.size()]);

		LocalDate debut = jours[0].getDate();
		LocalDate fin = jours[jours.length-1].getDate();
		String nom = "Championnat du "+debut.getDayOfMonth()+"/"+
				debut.getMonth()+"/"+debut.getYear()+" - "+
				fin.getDayOfMonth()+"/"+fin.getMonth()+"/"+fin.getYear();

		return new Championnat(nom, jours, equipes, false);
	}



/**
 * Insere les journees dans la base de donnee.
 */
	private void inserJourneeBD() {
		try{
			Statement s = ConnectionBD.getStatement().getConnection().createStatement();
			for(Journee j : journees){
				s.execute("insert into journee values("+j.getNum()+",to_date('"+j.getDate().format(DateTimeFormatter.ofPattern("dd-MM-YYYY"))+"','DD-MM-YYYY'))");

			}
		}
		catch(SQLException e){
			e.printStackTrace();
		}

	}
	/**
	 * Methode static appeler lors de la validation d'un championnat
	 * 
	 * @param equipeSuppr
	 * 		La liste des équipes suprimée
	 * @param allEquipe
	 * 		La liste de toutes les équipes
	 * @param equipeModif
	 * 		La liste des équipes modifiée
	 * @param equipeNouvelle
	 * 		La liste des nouvelles équipes 
	 * @param joueurModif
	 * 		La liste des joueurs modifié
	 * @param joueurNouveau
	 * 		la liste des nouveaux joueurs
	 * @param journees
	 * 		La liste des journees
	 * 
	 * @return le Championnat créé.
	 */
	public static Championnat validerChampionnat(List<Equipe> equipeSuppr,List<Equipe> allEquipe
			,List<Equipe> equipeModif,
			List<Equipe> equipeNouvelle, List<Joueur> joueurModif,
			List<Joueur> joueurNouveau ,List<LocalDate> journees  ){

		try {
			Statement s = ConnectionBD.getStatement().getConnection().createStatement();
			//s.execute("insert into equipe values(0,'Sans équipe', 'aucune',0)");


			for(Equipe e : equipeNouvelle){
				Equipe.getMapEquipes().put(e.getId(), e);
				s.execute("insert into Equipe values ("+e.getId()+",'"+e.getNom()+"','"+e.getVille()+"',0)");
			}
			for(Equipe e : equipeModif){
				s.execute("update equipe set "
						+"nomE = '"+ e.getNom()+"',"
						+"VilleE = '"+ e.getVille()+"',"
						+"points = 0"
						+"where idE ="+e.getId());
			}
			s.execute("update equipe set points = 0"); // mettre tout les equipes à 0 points

			for(Joueur j : joueurNouveau){
				String prof = "'N'";
				if(j.isProfessionnel()){
					prof = "'O'";
				}
				String date = "to_date('"+j.getDateNais().format(DateTimeFormatter.ofPattern("dd-MM-YYYY"))+"','DD-MM-YYYY')";
				s.execute("insert into joueur values("
						+j.getId()+","
						+j.getEquipe().getId()+","
						+j.getNum()+","
						+j.getNumLic()+","
						+"'"+j.getNom()+"',"
						+"'"+j.getPrenom()+"',"
						+prof+","
						+date+","
						+"'"+j.getRole().toUpperCase().charAt(0)+"',"
						+"'"+j.getAncienneEquipesString()+"'"
						+")");
			}

			for(Joueur j : joueurModif){
				String prof = "'N'";
				if(j.isProfessionnel()){
					prof = "'O'";
				}
				String date = "to_date('"+j.getDateNais().format(DateTimeFormatter.ofPattern("dd-MM-YYYY"))+"','DD-MM-YYYY')";
				s.execute("update joueur set "
						+"idE ="+j.getEquipe().getId()+","
						+"num ="+j.getNum()+","
						+"numLicJ ="+j.getNumLic()+","
						+"nomJ = "+"'"+j.getNom()+"',"
						+"prenomJ ="+"'"+j.getPrenom()+"',"
						+"profJ ="+prof+","
						+"datenaisJ ="+date+","
						+"roleJ ="+"'"+j.getRole().toUpperCase().charAt(0)+"',"
						+"anciennesEquipe ="+"'"+j.getAncienneEquipesString()+"'"
						+"where idJ="+j.getId());
			}
			for(Equipe e : equipeSuppr){
				s.execute("delete from equipe where idE = "+e.getId());
			}





		} catch (SQLException e) {
			System.out.println("Probleme enregistrement dans la Base de donnée du nouveau championnat");
			e.printStackTrace();
		} 
		LocalDate debut = journees.get(0);
		LocalDate fin = journees.get(journees.size()-1);
		String nom = "Championnat du "+debut.getDayOfMonth()+"/"+debut.getMonth()+"/"+debut.getYear()+" - "+
				fin.getDayOfMonth()+"/"+fin.getMonth()+"/"+fin.getYear();

		Equipe[] equipes =  allEquipe.toArray(new Equipe[allEquipe.size()]);
		Arrays.sort(equipes);

		return new Championnat(nom,  Journee.journeeListFromLocalDates(journees), equipes, true);
	}

	/**
	 * Repartie les matchs
	 */
	private void repartirMatch() {
		//journees[5].getMatchs().add(new Match(equipe1, equipe2, jour));
		int nbEquipe = equipes.length;
		int nbReel = nbEquipe;
		if(nbEquipe%2 != 0){
			nbEquipe++;
		}

		//Phase aller
		for(int i=1;i<nbReel;i++){
			for(int j=i+1;j<nbReel+1;j++){
				if(j==nbEquipe){
					if(2*i>nbEquipe){
						journees[((2*i)-nbEquipe)-1].getMatchs().add(
								new Match(equipes[i-1],equipes[j-1]
										,journees[((2*i)-nbEquipe)-1]));
					}
					else{
						journees[(2*i)-2].getMatchs().add(
								new Match(equipes[i-1],equipes[j-1]
										,journees[(2*i)-2]));
					}
				}
				else if(i+j-1<nbEquipe){
					journees[i+j-2].getMatchs().add(
							new Match(equipes[i-1],equipes[j-1]
									,journees[i+j-2]));
				}
				else{
					journees[(i+j-nbEquipe)-1].getMatchs().add(
							new Match(equipes[i-1],equipes[j-1]
									,journees[(i+j-nbEquipe)-1]));
				}

			}
		}
		//on met les matchs inverses jusqua la fin des dates
		int jour = nbEquipe-1;
		while(jour<journees.length){
			for(Match m : journees[jour-(nbEquipe-1)].getMatchs()){
				journees[jour].getMatchs().add(new Match(m.getEquipe2()
						,m.getEquipe1(),journees[jour]));
			}

			jour++;
		}


		//On insere dans la BDD
		try {
			Statement s = ConnectionBD.getStatement().getConnection().createStatement();
			for(Journee j : this.journees){
				for(Match m : j.getMatchs()){
					int idMatch = m.getId();
					int equipe1 = m.getEquipe1().getId();
					int equipe2 = m.getEquipe2().getId();
					String date = "to_date('"
							+m.getJour().getDate().format(DateTimeFormatter.ofPattern("dd-MM-YYYY"))+"','DD-MM-YYYY')";
					int numJour = j.getNum();

					s.execute("insert into match values("+idMatch+","+
							equipe1+","+equipe2+","+date+",0,0,"+numJour+",'N')");

				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
	
	public ArrayList<Equipe> getPodiumEquipe() {
		ArrayList<Equipe> aux = new ArrayList<Equipe>(Arrays.asList(equipes));
		Collections.sort(aux, new Equipe.ComparateurParPoints());
		ArrayList<Equipe> res = new ArrayList<Equipe>();
		res.add(aux.get(0));
		res.add(aux.get(1));
		res.add(aux.get(2));
		return res;
	}

	public ArrayList<Match> getProchainsMatchs() {
		LocalDate now = LocalDate.now();
		ArrayList<Match> aux = null;
		for (Journee j : journees) {
			if (aux != null) {
				break;
			}
			if (j.getDate().compareTo(now) >= 0) {
				aux = j.getMatchs();
			}
		}
		ArrayList<Match> res = new ArrayList<Match>();
		int cpt = 0;
		if (aux != null) {
			for (Match m : aux) {
				if (!m.estCharge() && cpt < 3) {
					if (cpt == 2 && aux.size() <= 2) {
						break;
					}
					res.add(aux.get(cpt++));
				}
			}
		}
		return res;
	}

	public ArrayList<Equipe> getClassement() {
		ArrayList<Equipe> res = new ArrayList<Equipe>(Arrays.asList(equipes));
		Collections.sort(res, new Equipe.ComparateurParPoints());
		return res;
	}

	public ArrayList<Joueur> getAllJoueurs() {
		ArrayList<Joueur> res = new ArrayList<Joueur>();
		for (Equipe equipe : equipes) {
			for (Joueur joueur : equipe.getJoueurs()) {
				res.add(joueur);
			}
		}
		return res;
	}

	public ArrayList<Match> getRecapitulatifMatchs() {
		ArrayList<Match> res = new ArrayList<Match>();
		for (Journee j : journees) {
			if (res.size() >= 4) {
				break;
			}
			if (j.getDate().compareTo(LocalDate.now()) < 0) {
				for (Match m : j.getMatchs()) {
					if (res.size() >= 4) {
						break;
					}
					if (!m.estCharge()) {
						res.add(m);
					}
				}
			} else {
				ArrayList<Match> dejaDansProchain = getProchainsMatchs();
				for (Match m : j.getMatchs()) {
					if (res.size() >= 4) {
						break;
					}
					if (!dejaDansProchain.contains(m)) {
						res.add(m);
					}
				}
			}
		}
		return res;
	}

	public boolean isFinish() {
		for (Journee j : journees) {
			for (Match m : j.getMatchs()) {
				if (!m.isCharge()) {
					return false;
				}
			}
		}
		return true;
	}

	public ArrayList<Match> getAllMatchs() {
		ArrayList<Match> res = new ArrayList<Match>();
		for (Journee j : journees) {
			for (Match match : j.getMatchs()) {
				res.add(match);
			}
		}
		return res;
	}

	public void finaliseLeChampionnat(){
		try {
			Statement s = ConnectionBD.getStatement().getConnection().createStatement();
			s.execute("DROP TABLE Visiter");
			s.execute("DROP TABLE Recevoir");
			s.execute("DROP TABLE Action");
			s.execute("DROP TABLE Participer");
			s.execute("DROP TABLE Match");
			s.execute("DROP TABLE Journee");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}


	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public Journee[] getJournees() {
		return journees;
	}
	public void setJournees(Journee[] journees) {
		this.journees = journees;
	}
	public Equipe[] getEquipes() {
		return equipes;
	}
	public void setEquipes(Equipe[] equipes) {
		this.equipes = equipes;
	}





}
