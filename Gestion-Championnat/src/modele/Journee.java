package modele;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Journee {
	
	private static int numMax = 0;

	private int num;
	private LocalDate date;
	private ArrayList<Match> matchs;
	
	/**
	 * @deprecated
	 */
	public Journee(){
		num=-1;
		date = null;
		setMatchs(null);
	}
	public Journee(int num,LocalDate date,ArrayList<Match> matchs) {
		this.num = num;
		if(num>=numMax){
			numMax=num+1;
		}
		this.date = date;
		this.setMatchs(matchs);
	}
	public Journee(LocalDate date){
		this.num = numMax++;
		this.date=date;
		matchs = new ArrayList<Match>();
	}

	public static Journee[] journeeListFromLocalDates(List<LocalDate> dates){
		Journee[] res = new Journee[dates.size()];
		for (int i = 0; i < dates.size(); i++) {
			res[i] = new Journee(dates.get(i));
		}
		return res;
	}
	
	//BDD
	public static Journee[] getJourneeFromBD(){
		ArrayList<Journee> res = new ArrayList<Journee>();
		try {
			Statement s = ConnectionBD.getStatement().getConnection().createStatement();
			ResultSet rs = s.executeQuery("select * from journee");
			
			while(rs.next()){
				int numJour = rs.getInt("numJour");
				String date = rs.getString("dateJour");
				String[] dates = date.split("-");
				LocalDate dateJ = LocalDate.of(Integer.parseInt(dates[0]),
						Integer.parseInt(dates[1]),
						Integer.parseInt(dates[2].split(" ")[0]));
				res.add(new Journee(numJour, dateJ, new ArrayList<Match>()));
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		Journee[] journes = res.toArray(new Journee[res.size()]);
		
		return journes;
		
	}
	
	
	
	
	
	
	
	//getters-setters
	public int getNum() {
		return num;
	}
	public void setNum(int num) {
		this.num = num;
	}
	public LocalDate getDate() {
		return date;
	}
	public void setDate(LocalDate date) {
		this.date = date;
	}
	public ArrayList<Match> getMatchs() {
		return matchs;
	}
	public void setMatchs(ArrayList<Match> matchs) {
		this.matchs = matchs;
	}
	public void addMatch(Match m){
		this.matchs.add(m);
	}
	
}
