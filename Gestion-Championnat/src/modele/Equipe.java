package modele;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import org.ho.yaml.Yaml;

import modele.Joueur.JoueurCharge;

public class Equipe implements Serializable, Comparable<Equipe>{

	private static final long serialVersionUID = -2038053879188584963L; // utilisé par serializable

	private static int idMax;

	private int id;
	private String nom;
	private String ville;
	private int points;
	private ArrayList<Joueur> joueurs;

	private static HashMap<Integer, Equipe> mapEquipes = new HashMap<Integer, Equipe>();

	/**
	 * @deprecated
	 */
	public Equipe(){
		id=-1;
		nom="NoName";
		ville="NoName";
		points=-1;
		joueurs=null;
	}


	public Equipe(int id, String nom, String ville, int points,ArrayList<Joueur> joueurs) {
		this.id = id;
		this.nom = nom;
		this.ville = ville;
		this.points = points;
		this.joueurs = joueurs;
	}

	public Equipe(String nom, String ville) {
		this.id = idMax++;
		this.nom = nom;
		this.ville = ville;
		this.points = 0;
		this.joueurs = new ArrayList<Joueur>();
	}

	public Equipe(int id,String nom, String ville) {
		this.id = id;
		this.nom = nom;
		this.ville = ville;
		this.points = 0;
		this.joueurs = new ArrayList<Joueur>();
	}
	public Equipe(int id,String nom, String ville,int points) {
		this.id = id;
		this.nom = nom;
		this.ville = ville;
		this.points = points;
		this.joueurs = new ArrayList<Joueur>();
	}

	public Equipe(EquipeCharge e){
		this.id=idMax++;
		this.nom = e.getNom();
		this.ville=e.getVille();
		this.points=e.getPoints();
		this.joueurs = new ArrayList<Joueur>();
		for(JoueurCharge j : e.getJoueurs()){
			Joueur j2 = new Joueur(j);
			j2.setEquipe(this);
			this.joueurs.add(j2);
		}
		Equipe.getMapEquipes().put(this.id, this);
	}

	public static Equipe chargerEquipeYAML(File f) throws FileNotFoundException{
		Equipe eq = null;
		EquipeCharge ec = (EquipeCharge) Yaml.load(f);
		eq = new Equipe(ec);
		return eq;
	}

	public void saveEquipeYAML(File f){
		EquipeCharge ec = new EquipeCharge(this);
		try {
			Yaml.dump(ec, f);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}


	public void addJoueur(Joueur j){
		if(j.getEquipe().getJoueurs() != null){
			j.getEquipe().getJoueurs().remove(j);
		}
		j.setEquipe(this);
		this.joueurs.add(j);		
	}

	public void removeJoueur(Joueur j) {
		if(j.getEquipe().getJoueurs() != null){
			j.getEquipe().getJoueurs().remove(j);
		}
		Joueur.getMapJoueurs().remove(j.getId());
		j.setEquipe(null);
	}





	//Requete SQL

	public static ArrayList<Equipe> getEquipeFromBD(){

		ArrayList<Equipe> equipe = new ArrayList<Equipe>();

		equipe = Equipe.getEquipesFromBDSansJoueur();
		for(Equipe e : equipe){
			mapEquipes.put(e.getId(), e);
		}
		try {
			ArrayList<Joueur> joueurs = Joueur.getAllJoueurBD();
		} catch (SQLException e1) {
			System.out.println("Erreur chargement joueur BD");
			e1.printStackTrace();
		}

		return equipe;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Equipe other = (Equipe) obj;
		if (id != other.id)
			return false;
		if (joueurs == null) {
			if (other.joueurs != null)
				return false;
		} else if (!joueurs.equals(other.joueurs))
			return false;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		if (ville == null) {
			if (other.ville != null)
				return false;
		} else if (!ville.equals(other.ville))
			return false;
		return true;
	}
	
	public boolean equalsWithouId(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Equipe other = (Equipe) obj;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		if (ville == null) {
			if (other.ville != null)
				return false;
		} else if (!ville.equals(other.ville))
			return false;
		if (joueurs == null) {
			if (other.joueurs != null)
				return false;
		} else {
			if (joueurs.size() != other.joueurs.size()) {
				return false;
			}
			for (int i = 0; i < joueurs.size(); i++) {
				if (!joueurs.get(i).equalsWithoutId(other.joueurs.get(i))) {
					return false;
				}
			}
		}
		return true;
	}


	//renvoie tout les equipes de la bd
	private static ArrayList<Equipe> getEquipesFromBDSansJoueur() {
		ArrayList<Equipe> res = new ArrayList<Equipe>();
		try{
			Statement s = ConnectionBD.getStatement().getConnection().createStatement();

			ResultSet r = s.executeQuery("select * from Equipe order by idE");
			while(r.next()){
				int idE = r.getInt("idE");
				if(idE>=Equipe.idMax){
					Equipe.idMax=idE+1;
				}
				String nomE = r.getString("nomE");
				String villeE = r.getString("VilleE");
				int points = r.getInt("points");
				res.add(new Equipe(idE,nomE,villeE,points));

			}
		}
		catch(SQLException  e){
			e.printStackTrace();
		}
		return res;
	}
	public static Equipe getEquipeFromBDByID(int id){

		try {
			Statement s = ConnectionBD.getStatement().getConnection().createStatement();

			ResultSet rs = s.executeQuery("select * from equipe where idE = "+id);
			if (rs.next()) {
				int idE = rs.getInt("idE");
				if(idE>Equipe.idMax){
					Equipe.idMax=idE+1;
				}
				String nomE = rs.getString("nomE");
				String villeE = rs.getString("VilleE");
				int points = rs.getInt("points");
				return new Equipe(idE,nomE,villeE,points);
			}


		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}


	//Fin Requete SQL
	public static int getIdMax() {
		return idMax;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getVille() {
		return ville;
	}
	public void setVille(String ville) {
		this.ville = ville;
	}
	public int getPoints() {
		return points;
	}
	public void setPoints(int points) {
		this.points = points;
	}
	public ArrayList<Joueur> getJoueurs() {
		return joueurs;
	}
	public void setJoueurs(ArrayList<Joueur> joueurs) {
		this.joueurs = joueurs;
	}

	public int getNombreJoueurs() {
		return joueurs.size();
	}

	public static HashMap<Integer, Equipe> getMapEquipes() {
		return mapEquipes;
	}


	@Override
	public int compareTo(Equipe o) {
		return Integer.compare(id, o.getId());

	}

	public static class ComparateurParPoints implements Comparator<Equipe> {
		@Override
		public int compare(Equipe o1, Equipe o2) {
			return o1.getPoints() > o2.getPoints() ? -1 : o1.getPoints() < o2.getPoints() ? +1 : 0;
		}

	}


/**
 * <b>Equipe chargeable en YAML</b>
 * 
 * @author Sebastien Gedeon
 *
 */
	public static class EquipeCharge{
		private String nom;
		private String ville;
		private int points;
		private ArrayList<JoueurCharge> joueurs;

		public EquipeCharge() {
			nom="";
			ville="";
			points=0;
			joueurs=new ArrayList<JoueurCharge>();
		}

		public EquipeCharge(Equipe e){
			this.nom = e.getNom();
			this.ville = e.getVille();
			this.points = e.getPoints();

			joueurs = new ArrayList<JoueurCharge>();
			for(Joueur j : e.getJoueurs()){
				this.joueurs.add(new JoueurCharge(j));
			}
		}

		public String getNom() {
			return nom;
		}
		public void setNom(String nom) {
			this.nom = nom;
		}
		public String getVille() {
			return ville;
		}
		public void setVille(String ville) {
			this.ville = ville;
		}
		public int getPoints() {
			return points;
		}
		public void setPoints(int points) {
			this.points = points;
		}
		public ArrayList<JoueurCharge> getJoueurs() {
			return joueurs;
		}
		public void setJoueurs(ArrayList<JoueurCharge> joueurs) {
			this.joueurs = joueurs;
		}





	}


}
