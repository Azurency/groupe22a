package modele;

import java.sql.*;

/**
 * <b>Singleton represantant la connection avec la Base de donnée</b>
 * 
 * @author Sebastien Gedeon
 *
 */
public class ConnectionBD {

	private static ConnectionBD instance;

	private Connection connection;
	private boolean connecte = false;

	public ConnectionBD(){
		this.seCo();
	}

	public void seCo(){
		try{
			Class.forName("oracle.jdbc.driver.OracleDriver");
			//connection=DriverManager.getConnection("jdbc:oracle:thin:@oracle:1521:oracle"
			//		,"gedeon","cadepub6"); // oui c'est mon mot de passe mouahahahahahahahahhahahahahahahahahahahahahahahahahahahahahahahahahhahah
			//connection=DriverManager.getConnection("jdbc:oracle:thin::1521:oracle"
			//,"SYSTEM","cadepub6");
			connection=DriverManager.getConnection("jdbc:oracle:thin:@//127.0.0.1:1521/PDB1"     //Et oui il faut changer ca a chaque fois
					,"pmuser","oracle");
			connecte = true;


		}
		catch(SQLException e){
			System.out.println("Erreur connexion: "+e.getErrorCode());
		}
		catch(ClassNotFoundException e){
			System.out.println("Erreur de driver: "+e.getMessage());
		}

	}

	public static ConnectionBD getStatement(){
		if (instance!=null){
			if(! instance.isConnecte()){
				instance.seCo();
			}
			return instance;
		}
		else{
			instance = new ConnectionBD();
			return instance;
		}
	}

	public void seCo(String nomBD,String login,String mdp){
		try{
			Class.forName("oracle.jdbc.driver.OracleDriver");
			connection= DriverManager.getConnection(nomBD,login,mdp);
			connecte = true;
		}
		catch(SQLException e){
			System.out.println("Erreur connexion: "+e.getErrorCode());
		}
		catch(ClassNotFoundException e){
			System.out.println("Erreur de driver: "+e.getMessage());
		}

	}

	public void seDeco() throws SQLException{
		try{
			if(connection != null){
				setConnecte(false);
				connection.close();
			}
		}
		catch(SQLException e){
			System.out.println("Erreur deconnexion: "+e.getErrorCode());
		}
	}

	public Connection getConnection() {
		return connection;
	}

	public void setConnection(Connection c) {
		this.connection = c;
	}

	public boolean isConnecte() {
		return connecte;
	}

	public void setConnecte(boolean connecte) {
		this.connecte = connecte;
	}




}
