package modele;

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;


public class FeuilleDeMatch{

	private int id;
	private ArrayList<JoueurFDM> joueurs;
	private EquipeFDM equipe1;
	private EquipeFDM equipe2;

	public FeuilleDeMatch(){
		id=-1;
		joueurs = null;
		equipe1 = null;
		equipe2 = null;
	}
	
	public FeuilleDeMatch(int id,Equipe equipe1,Equipe equipe2){
		this.setId(id);
		joueurs = new ArrayList<JoueurFDM>();
		for(Joueur j1 : equipe1.getJoueurs()){
			//System.out.println(j1.getId());
			joueurs.add(new JoueurFDM(j1));
		}
		for(Joueur j2 : equipe2.getJoueurs()){
			joueurs.add(new JoueurFDM(j2));
		}
		this.equipe1 = new EquipeFDM(equipe1);
		this.equipe2 = new EquipeFDM(equipe2);
		
		
	}
	
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public ArrayList<JoueurFDM> getJoueurs() {
		return joueurs;
	}
	public void setJoueurs(ArrayList<JoueurFDM> joueurs) {
		this.joueurs = joueurs;
	}
	public EquipeFDM getEquipe1() {
		return equipe1;
	}
	public void setEquipe1(EquipeFDM equipe1) {
		this.equipe1 = equipe1;
	}
	public EquipeFDM getEquipe2() {
		return equipe2;
	}
	public void setEquipe2(EquipeFDM equipe2) {
		this.equipe2 = equipe2;
	}

/**
 * <b>Joueur chargeable en YAML pour la Feuille de match</b>
 * 
 * @author Sebastien Gedeon
 *
 */
	public static class JoueurFDM{
		private int id,idE;
		private int num;
		private String nom,prenom,age,role;
		
		
		public JoueurFDM() {
			this.setId(-1);
			this.setIdE(-1);
			this.setNum(-1);
			this.setNom("");
			this.setPrenom("");
			this.setAge("");
			this.setRole("");
		}
		public JoueurFDM(Joueur j){
			this.setId(j.getId());
			this.setIdE(j.getEquipe().getId());
			this.setNum(j.getNum());
			this.setNom(j.getNom());
			this.setPrenom(j.getPrenom());
			Period age = Period.between(j.getDateNais(), LocalDate.now());
			this.setAge(age.getYears()+" ans");
			this.setRole(j.getRole());
		}


		public JoueurFDM(int id,int idE, int num, String nom, String prenom,
				String age, String role) {
			this.setId(id);
			this.setIdE(idE);
			this.setNum(num);
			this.setNom(nom);
			this.setPrenom(prenom);
			this.setAge(age);
			this.setRole(role);
		}

		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public int getNum() {
			return num;
		}
		public void setNum(int num) {
			this.num = num;
		}
		public String getNom() {
			return nom;
		}
		public void setNom(String nom) {
			this.nom = nom;
		}
		public String getPrenom() {
			return prenom;
		}
		public void setPrenom(String prenom) {
			this.prenom = prenom;
		}
		public String getAge() {
			return age;
		}
		public void setAge(String age) {
			this.age = age;
		}
		public String getRole() {
			return role;
		}
		public void setRole(String role) {
			this.role = role;
		}
		public int getIdE() {
			return idE;
		}
		public void setIdE(int idE) {
			this.idE = idE;
		}
		
	}
	/**
	 * <b>Equipe chargeable en YAML pour la feuille de match</b>
	 * @author Sebastien Gedeon
	 *
	 */
	public static class EquipeFDM{
		private int id;
		private String nom;
		public EquipeFDM(){
			id=-1;
			nom="";
		}
		public EquipeFDM(int id, String nom){
			this.id = id;
			this.nom = nom;
		}
		public EquipeFDM(Equipe e){
			this.id = e.getId();
			this.nom = e.getNom();
		}
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public String getNom() {
			return nom;
		}
		public void setNom(String nom) {
			this.nom = nom;
		}
	}
}
