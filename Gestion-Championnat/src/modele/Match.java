package modele;

import java.io.File;
import java.io.FileNotFoundException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import javax.swing.text.StyledEditorKit.BoldAction;

import org.ho.yaml.Yaml;

public class Match {

	private static int idMax;

	private int id;
	private Equipe equipe1;
	private int scoreEq1;
	private Equipe equipe2;
	private int scoreEq2;
	private LocalDate date;
	private Journee jour;
	private boolean charge; // boolean pour savoir si le Match à été chargé
	private ArrayList<Action> actions;

	/**
	 * @deprecated
	 */
	public Match(){
		id=-1;
		equipe1=null;
		scoreEq1=-1;
		equipe2=null;
		scoreEq2=-1;
		jour=null;
		charge = false;
		actions=new ArrayList<Action>();
	}

	public Match(int id, Equipe equipe1, int scoreEq1, Equipe equipe2,
			int scoreEq2, Journee jour) {
		this.id = id;
		this.equipe1 = equipe1;
		this.scoreEq1 = scoreEq1;
		this.equipe2 = equipe2;
		this.scoreEq2 = scoreEq2;
		this.jour = jour;
		charge = false;
		actions = new ArrayList<Action>();
	}

	public Match(int id, Equipe equipe1, int scoreEq1, Equipe equipe2,
			int scoreEq2, LocalDate date, Journee jour, boolean charge) {
		this.id = id;
		this.equipe1 = equipe1;
		this.scoreEq1 = scoreEq1;
		this.equipe2 = equipe2;
		this.scoreEq2 = scoreEq2;
		this.date = date;
		this.jour = jour;
		this.charge = charge;
		actions = new ArrayList<Action>();
	}

	public Match(Equipe equipe1, Equipe equipe2, Journee jour) {
		this.id = idMax++;
		this.equipe1 = equipe1;
		this.scoreEq1 = 0;
		this.equipe2 = equipe2;
		this.scoreEq2 = 0;
		this.jour = jour;
		charge = false;
		actions = new ArrayList<Action>();
	}
	public Match(int id, Equipe equipe1, int scoreEq1, Equipe equipe2,
			int scoreEq2, LocalDate date, Journee jour, boolean charge,
			ArrayList<Action> actions) {
		this.id = id;
		this.equipe1 = equipe1;
		this.scoreEq1 = scoreEq1;
		this.equipe2 = equipe2;
		this.scoreEq2 = scoreEq2;
		this.date = date;
		this.jour = jour;
		this.charge = charge;
		this.actions = actions;
	}
	public Match(int id, Equipe equipe1, Equipe equipe2) {
		super();
		this.id = id;
		this.equipe1 = equipe1;
		this.equipe2 = equipe2;
		actions = new ArrayList<Action>();
	}

	public String getTextEquipes(){
		return this.equipe1.getNom() + " contre "+ this.equipe2.getNom();
	}



	//charger le YAML
	public void charger(File file){
		Statement s = null;
		try {
			s = ConnectionBD.getStatement().getConnection().createStatement();
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		try {
			RecapAction recap = Yaml.loadType(file,RecapAction.class);
			if(recap.getId() == this.getId()){
				this.scoreEq1 = recap.getScoreEq1();
				this.scoreEq2 = recap.getScoreEq2();


				if(scoreEq1<scoreEq2){
					this.equipe2.setPoints(equipe2.getPoints()+2);
				}
				else if (scoreEq2<scoreEq1){
					this.equipe1.setPoints(equipe1.getPoints()+2);
				}
				else{
					this.equipe1.setPoints(equipe1.getPoints()+1);
					this.equipe2.setPoints(equipe2.getPoints()+1);
				}


				this.charge = true;
				int nbBut1 =0;
				int nbFaute1 =0;
				int nbBut2 =0;
				int nbFaute2 =0;

				HashMap<Integer,Integer> mapBut = new HashMap<Integer,Integer>();
				HashMap<Integer,Integer> mapPasse = new HashMap<Integer,Integer>();
				HashMap<Integer,Integer> mapFaute = new HashMap<Integer,Integer>();


				for(Action a : recap.getActions()){
					int idA = a.getId();
					int idJ = a.getIdJ();
					int idM = a.getIdM();
					String timer = a.getTimer();
					String action = a.getAction();
					String joueur = a.getJoueur();
					String equipe = a.getEquipe();

					Action a1 = new Action(idM, idJ, equipe, timer, action, joueur);
					this.actions.add(a1);

					if(Joueur.getMapJoueurs().get(idJ).getEquipe().getId() 
							== this.equipe1.getId()){
						if(action.equals("but")){
							nbBut1++;
						}
						else if(action.equals("faute")){
							nbFaute1++;
						}
					}
					else{
						if(action.equals("but")){
							nbBut2++;
						}
						else if(action.equals("faute")){
							nbFaute2++;
						}
					}
					//rempli les MAP
					if(action.equals("but")){

						if(mapBut.get(idJ)!=null){
							mapBut.put(idJ, mapBut.get(idJ)+1);
						}
						else{
							mapBut.put(idJ, 1);
						}
					}
					else if (action.equals("faute")){

						if(mapFaute.get(idJ)!=null){
							mapFaute.put(idJ, mapFaute.get(idJ)+1);
						}
						else{
							mapFaute.put(idJ, 1);
						}
					}
					else if (action.equals("passe decisive")){
						if(mapPasse.get(idJ)!=null){
							mapPasse.put(idJ, mapPasse.get(idJ)+1);
						}
						else{
							mapPasse.put(idJ, 1);
						}
					}
					try { //j'ai eu ici Nbr max de curseur dépacé
						s.execute("insert into action values("+a1.getId()+","+idJ+","+
								idM+",'"+timer+"','"+action+"','"+
								joueur+"','"+equipe+"')");
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}
				try {
					s.execute("insert into recevoir values("+this.getId()+","
							+this.getEquipe1().getId()+","+nbBut1+","+nbFaute1
							+")");
					s.execute("insert into visiter values("+this.getId()+","
							+this.getEquipe2().getId()+","+nbBut2+","+nbFaute2
							+")");
					s.execute("update match set charge = 'O' , scoreLocaux = "+scoreEq1+" , scoreVisiteur = "+scoreEq2+
							" where idM = "+this.getId());

					for(Joueur j : this.equipe1.getJoueurs()){
						int idJJ = j.getId();
						int idMM = this.getId();
						s.execute("insert into Participer values("+idJJ+","+idMM+",0,0,0)");
					}
					for(Joueur j : this.equipe2.getJoueurs()){
						int idJJ = j.getId();
						int idMM = this.getId();
						s.execute("insert into Participer values("+idJJ+","+idMM+",0,0,0)");
					}
					for(Entry<Integer, Integer> entry : mapBut.entrySet()){
						int idJPart = entry.getKey();
						int idMPart = this.getId();
						int nbBut = entry.getValue();
						s.execute("update Participer set nbButs = "+nbBut+" where idJ = "
								+idJPart+" and idM = "+idMPart);
					}
					for(Entry<Integer, Integer> entry : mapFaute.entrySet()){
						int idJPart = entry.getKey();
						int idMPart = this.getId();
						int nbFaute = entry.getValue();
						s.execute("update Participer set nbFautes = "+nbFaute+" where idJ = "
								+idJPart+" and idM = "+idMPart);
					}
					for(Entry<Integer, Integer> entry : mapPasse.entrySet()){
						int idJPart = entry.getKey();
						int idMPart = this.getId();
						int nbPasse = entry.getValue();
						s.execute("update Participer set nbPassesDecisives = "+nbPasse+" where idJ = "
								+idJPart+" and idM = "+idMPart);
					}
					s.execute("update equipe set points = "+equipe1.getPoints()+ " where idE = "+equipe1.getId());
					s.execute("update equipe set points = "+equipe2.getPoints()+ " where idE = "+equipe2.getId());
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		} catch (FileNotFoundException | NullPointerException e) {
			e.printStackTrace();
		}
	}

	//creer le YAML
	public void genererFeuilleMatch(File file){

		FeuilleDeMatch fdm = new FeuilleDeMatch(id, equipe1, equipe2);
		try {
			Yaml.dump(fdm, file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public int[] getSommesFautePasse() {
		int res[] = {0, 0};
		try {
			Statement s = ConnectionBD.getStatement().getConnection().createStatement();
			ResultSet rs = s.executeQuery("SELECT sum(nbFautes), sum(nbPassesDecisives) FROM Participer natural join Joueur"
					+ " WHERE idE="+equipe1.getId()+" OR idE="+equipe2.getId());
			if (rs.next()) {
				res[0] = rs.getInt(1);
				res[1] = rs.getInt(2);
			}
			return res;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return res;
	}

	//getters-setters
	public static int getIdMax() {
		return idMax;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Equipe getEquipe1() {
		return equipe1;
	}
	public void setEquipe1(Equipe equipe1) {
		this.equipe1 = equipe1;
	}
	public int getScoreEq1() {
		return scoreEq1;
	}
	public void setScoreEq1(int scoreEq1) {
		this.scoreEq1 = scoreEq1;
	}
	public Equipe getEquipe2() {
		return equipe2;
	}
	public void setEquipe2(Equipe equipe2) {
		this.equipe2 = equipe2;
	}
	public int getScoreEq2() {
		return scoreEq2;
	}
	public void setScoreEq2(int scoreEq2) {
		this.scoreEq2 = scoreEq2;
	}
	public Journee getJour() {
		return jour;
	}
	public void setJour(Journee jour) {
		this.jour = jour;
	}
	public boolean estCharge() {
		return charge;
	}
	public boolean isCharge() {
		return charge;
	}
	public void setCharge(boolean charge) {
		this.charge = charge;
	}
	public LocalDate getDate() {
		return date;
	}
	public void setDate(LocalDate date) {
		this.date = date;
	}
	public ArrayList<Action> getActions() {
		return actions;
	}
	public void setActions(ArrayList<Action> actions) {
		this.actions = actions;
	}



}
