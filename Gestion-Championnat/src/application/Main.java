package application;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import controlleur.fenetreCreation.ResumeScreenController;
import modele.ConnectionBD;
import modele.Equipe;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * <b>Main est la classe principale de l'application de gestion du championnat de handball.</b>
 * <p>
 * On test si un championnat est en cours et on lance la fenêtre correspondante
 * </p>
 * @see Main#main(String[])
 * @see Main#start(Stage)
 * 
 * @author Antoine Lassier
 *
 */

public class Main extends Application {
	/**
	 * L'état du championnat, true si il est en cours, false sinon.
	 */
	private static boolean championnatEnCours = false;

	/**
	 * La Scene qu'il faut charger dans la méthode Start. Cette scène dépend de l'état du championnat.
	 * @see Main#start(Stage)
	 * @see Main#championnatEnCours
	 */
	private static Scene sceneACharger = null;

	/**
	 * Le Stage qui est affiché. Défini Static pour permettre de l'appeler facilement depuis les autres classes.
	 * @see Main#getStage()
	 */
	private static Stage stage;


	/**
	 * La fonction main test si un championnat et en cours et charge les polices (pour le css)
	 * <ul>
	 * <li>Si un championnat est en cours, la variable championnatEnCours à true</li>
	 * <li>Si il n'y a pas de championnat en cours, on regarde si il existe des équipes et des joueurs, si non c'est que que l'application
	 * est lancée pour la premiere fois, si oui, on ne créer que les tables manquantes.</li>
	 * </ul>
	 * @param args
	 * 
	 * @see Main#championnatEnCours
	 * @see Main#loadFonts()
	 */
	public static void main(String[] args) {

		// oui j'ai commenté ton test pck fuck de lancer la machine virtuelle nianianianiania
		try {
			Statement s = ConnectionBD.getStatement().getConnection().createStatement();

			ResultSet rs = s.executeQuery("select * from journee");
			if(rs.next()){
				championnatEnCours=true;
			}
		} catch (SQLException e) {
			try {
				Statement s = ConnectionBD.getStatement().getConnection().createStatement();

				try{
					ResultSet rs = s.executeQuery("select * from joueur");
				}catch(SQLException e2){
					s.execute("CREATE TABLE Equipe("+
							"idE number(8) NOT NULL PRIMARY KEY,"+
							"nomE varchar2(30) NOT NULL,"+
							"VilleE varchar2(30) NOT NULL,"+
							"points number(10)"+
							")");

					s.execute("CREATE TABLE Joueur("+
							"idJ number(9) NOT NULL PRIMARY KEY,"+
							"idE number(8),"+
							"constraint fk_idEJ FOREIGN KEY (idE) REFERENCES Equipe(idE),"+
							"num number(2),"+
							"numLicJ number(8),"+
							"nomJ varchar2(30) NOT NULL,"+
							"prenomJ varchar2(30) NOT NULL,"+
							"profJ char, "+
							"constraint estProfessionel check (profJ='O' or profJ='N'),"+
							"datenaisJ date NOT NULL,"+
							"roleJ char,"+
							"anciennesEquipe varchar2(1000),"+
							"constraint roleJoueurOk check (roleJ='E' or roleJ='A' or roleJ='J')"+
							")");
				}



				s.execute("CREATE TABLE Journee("+
						" numJour number(3) NOT NULL PRIMARY KEY,"
						+" dateJour date NOT NULL )");

				s.execute("CREATE TABLE Match("+
						"idM number(8) NOT NULL PRIMARY KEY,"+
						"idE1 number(9) NOT NULL,"+
						"constraint fk_idE1M FOREIGN KEY (idE1) REFERENCES Equipe(idE),"+
						"idE2 number(9) NOT NULL,"+
						"constraint fk_idE2M FOREIGN KEY (idE2) REFERENCES Equipe(idE),"+
						"constraint ck_idEDiffM check (idE1 <> idE2),"+
						"dateM date NOT NULL,"+
						"scoreLocaux number(3),"+
						"scoreVisiteur number(3),"+
						"numJour number(3) NOT NULL,"+
						"constraint fk_jourM FOREIGN KEY (numJour) REFERENCES Journee(numJour),"+
						"charge char,"+
						"constraint chargeOK check (charge='O' or charge='N')"+
						")");

				s.execute("CREATE TABLE Participer("+
						"idJ number(9),"+
						"idM number(8),"+
						"constraint pk_idMJP PRIMARY KEY (idJ,idM),"+
						"constraint fk_idJP FOREIGN KEY (idJ) REFERENCES Joueur(idJ),"+
						"constraint fk_idMP FOREIGN KEY (idM) REFERENCES Match(idM),"+
						"nbButs number(3),"+
						"nbPassesDecisives number(3),"+
						"nbFautes number(3)"+
						")");

				s.execute("CREATE TABLE Action("+
						"idA number(4) PRIMARY KEY,"+
						"idJ number(9),"+
						"idM number(8) NOT NULL,"+
						"constraint fk_idJA FOREIGN KEY (idJ) REFERENCES Joueur(idJ),"+
						"constraint fk_idMA FOREIGN KEY (idM) REFERENCES Match(idM),"+
						"timer varchar2(10) NOT NULL,"+
						"action varchar2(30) NOT NULL,"+
						"joueur varchar2 (30) NOT NULL,"+
						"equipe varchar2(30) NOT NULL"+
						")");

				s.execute("CREATE TABLE Recevoir("+
						"idM number(8) NOT NULL PRIMARY KEY,"+
						"idE number(8),"+
						"constraint fk_idER FOREIGN KEY (idE) REFERENCES Equipe(idE),"+
						"constraint fk_idMR FOREIGN KEY (idM) REFERENCES Match(idM),"+
						"nbButs number(3),"+
						"nbFautes number(3)"+
						")");

				s.execute("CREATE TABLE Visiter("+
						"idM number(8) NOT NULL PRIMARY KEY,"+
						"idE number(8),"+
						"constraint fk_idEV FOREIGN KEY (idE) REFERENCES Equipe(idE),"+
						"constraint fk_idMV FOREIGN KEY (idM) REFERENCES Match(idM),"+
						"nbButs number(3),"+
						"nbFautes number(3)"+
						")");
			} catch (SQLException e2) {
				// TODO Auto-generated catch block
				e2.printStackTrace();
			}
			
		championnatEnCours=false;
	}

	loadFonts();
	launch(args);
}

/**
 * loadFonts charge les polices pour les utiliser plus tard dans le CSS
 */
public static void loadFonts() {
	Font.loadFont(Main.class.getResourceAsStream("/font/HelveticaNeue.ttf"), 20);
	Font.loadFont(Main.class.getResourceAsStream("/font/HelveticaNeueLight.ttf"), 20);
	Font.loadFont(Main.class.getResourceAsStream("/font/HelveticaNeueThin.ttf"), 20);
	Font.loadFont(Main.class.getResourceAsStream("/font/HelveticaNeueBold.ttf"), 20);
	Font.loadFont(Main.class.getResourceAsStream("/font/calendarfont.ttf"), 20);
	Font.loadFont(Main.class.getResourceAsStream("/font/fontello.ttf"), 20);
	Font.loadFont(Main.class.getResourceAsStream("/font/flaticon.ttf"), 20);
	Font.loadFont(Main.class.getResourceAsStream("/font/resume.ttf"), 20);
	Font.loadFont(Main.class.getResourceAsStream("/font/principal.ttf"), 20);
	Font.loadFont(Main.class.getResourceAsStream("/font/rank.ttf"), 20);
	Font.loadFont(Main.class.getResourceAsStream("/font/recapitulatif.ttf"), 20);
}

/**
 * Start initialise le Stage et choisi quelle Scene afficher en fonction de l'état du championnat.
 * @param primaryStage
 * 
 * @see Main#stage
 * @see Main#championnatEnCours
 * @see Main#sceneACharger
 */
@Override
public void start(Stage primaryStage) throws Exception {
	stage = primaryStage;

	if (championnatEnCours) {
		sceneACharger = FenetrePrincipale.initFenetrePrincipale();
	} else {
		Statement s = ConnectionBD.getStatement().getConnection().createStatement();
		if(Equipe.getEquipeFromBDByID(0)==null){
			s.execute("insert into equipe values(0,'Sans équipe', 'aucune',0)");
		}
		sceneACharger = FenetreCreation.initFenetreCreation();
	}

	stage.getIcons().add(new Image(Main.class.getResourceAsStream("GestiondeChampionnatHandball.png")));
	stage.setTitle("Gestion de championnat de handball");
	stage.setScene(sceneACharger);
	stage.initStyle(StageStyle.TRANSPARENT);
	stage.show();
}

/**
 * Change de Scene pour afficher la fenetrePrinciaple (appelé quand on valide la création d'un championnat)
 * 
 * @see ResumeScreenController
 */
public static void showFenetrePrincipale() {
	stage.setScene(FenetrePrincipale.initFenetrePrincipale());
	stage.sizeToScene();
}

/**
 * Renvoie le stage affiché actuellement.
 * 
 * @return Le stage affiché.
 */
public static Stage getStage() {
	return stage;
}
}
