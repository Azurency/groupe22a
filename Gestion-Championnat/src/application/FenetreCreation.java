package application;

import controlleur.ScreensController;
import controlleur.fenetreCreation.DateScreenController;
import controlleur.fenetreCreation.EquipeScreenController;
import controlleur.fenetreCreation.ResumeScreenController;
import javafx.scene.Scene;

/**
 * <b>FenetreCreation contient la méthode statique qui permet d'initialiser l'interface de création d'un championnat
 * de handball</b>
 * 
 * @author Antoine Lassier
 *
 */
public abstract class FenetreCreation {
	/**
	 * L'instance de la 1ere fenêtre de création, la sélection des dates.
	 * @see DateScreenController
	 */
	public static DateScreenController dateScreen = null;
	
	/**
	 * Permet d'identifier l'écran de sélection des dates.
	 */
	public static final String SCREEN1ID = "SelectionDate";
	
	/**
	 * L'instance de la 2ème fenêtre de création, la sélection des équipes.
	 * @see EquipeScreenController
	 */
	public static EquipeScreenController equipeScreen = null;
	
	/**
	 * Permet d'identifier l'écran de sélection des équipes.
	 */
	public static final String SCREEN2ID = "SelectionEquipe";
	
	/**
	 * L'instance de la 3ème fenetre de création, le résumé.
	 */
	public static ResumeScreenController resumeScreen = null;
	
	/**
	 * Permet d'identifier l'écran de résumé.
	 */
	public static final String SCREEN3ID = "Resumé";
	
	/**
	 * Retourne la Scene correspondant à l'écrant de création du championnat. Elle instancie de le controlleur et charge les fichiers
	 * css.
	 * @return
	 * 		la Scene de l'écran de création du championnat
	 */
	public static Scene initFenetreCreation() {
		dateScreen = new DateScreenController();
		equipeScreen = new EquipeScreenController();
		resumeScreen = new ResumeScreenController();
		
		// Création du controlleur
		
		ScreensController conteneurPrincipal = new ScreensController(980,650);
		conteneurPrincipal.setId("fenetre");
		conteneurPrincipal.loadScreen(SCREEN1ID, dateScreen, dateScreen.getVue().getPanePrincipal());
		conteneurPrincipal.loadScreen(SCREEN2ID, equipeScreen, equipeScreen.getVue().getPanePrincipal());
		conteneurPrincipal.loadScreen(SCREEN3ID, resumeScreen, resumeScreen.getVue().getPanePrincipal());
		
		conteneurPrincipal.setScreen(SCREEN1ID);
		
		String fichierCss1 = FenetreCreation.class.getResource("application.css").toExternalForm();
		String fichierCss2 = FenetreCreation.class.getResource("/vue/fenetreCreation/FenetreCreation.css").toExternalForm();
		
		conteneurPrincipal.getS().getStylesheets().addAll(fichierCss1, fichierCss2);
		
		return conteneurPrincipal.getS();
	}
	
}
