package application;

import controlleur.ScreensController;
import controlleur.fenetrePrincipale.FenetrePrincipaleScreenController;
import javafx.scene.Scene;

/**
 * <b>FenetrePrincipale contient la méthode statique qui permet d'initialiser l'interface principale d'un championnat
 * de handball</b>
 * @author Antoine Lassier
 *
 */
public abstract class FenetrePrincipale {
	
	/**
	 * Permet d'identifier l'écran principal.
	 */
	public static final String SCREEN1ID = "principal";
	
	public static Scene initFenetrePrincipale() {
		FenetrePrincipaleScreenController fenetre = new FenetrePrincipaleScreenController();
		
		ScreensController conteneurPrincipal = new ScreensController(1100,700);
		conteneurPrincipal.setId("fenetre");
		
		conteneurPrincipal.loadScreen(SCREEN1ID, fenetre, fenetre.getVue().getPanePrincipal());
		conteneurPrincipal.setScreen(SCREEN1ID);
		
		String fichierCss1 = FenetreCreation.class.getResource("application.css").toExternalForm();
		String fichierCss2 = FenetreCreation.class.getResource("/vue/fenetrePrincipale/FenetrePrincipale.css").toExternalForm();
		
		conteneurPrincipal.getS().getStylesheets().addAll(fichierCss1, fichierCss2); // changer de place
		
		return conteneurPrincipal.getS();
	}
	
}
