package vue.fenetreCreation;

import java.time.LocalDate;

import modele.Equipe;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.TextFlow;
import controlleur.ScreensController;

public class ResumeScreenView {

	private BorderPane panePrincipal;
	private Text titre;
	private GridPane grilleContenu;
	private TextFlow etape;
	private Text etapeSigne;
	private Text etapeTexte;
	private Text texte;
	private Text texte2;
	private Text texte3;
	private TextFlow flow;
	private GridPane grilleBas;
	private Button valider;
	private Button ecranDate;
	private Button ecranEquipe;
	private GridPane grilleDateRetenues;
	private Label lDateListView;
	private ListView<LocalDate> dateListView;
	private GridPane grilleEquipeSelectionnee;
	private Label lEquipeListView;
	private ListView<Equipe> equipeListView;
	private Text texte4;

	public ResumeScreenView() {
		// Création du paneaux principal
		panePrincipal = new BorderPane();
		panePrincipal.setId("ResumePane");
		ScreensController.setMargin(panePrincipal, new Insets(25, 25, 25, 25));

		// Titre de la fenetre
		titre = new Text("création du championnat");
		titre.setId("titre");
		BorderPane.setAlignment(titre, Pos.CENTER);
		BorderPane.setMargin(titre, new Insets(7, 0, 7, 0));

		panePrincipal.setTop(titre);

		// Contenu
		grilleContenu = new GridPane();
		grilleContenu.setId("grilleContenu");
		BorderPane.setMargin(grilleContenu, new Insets(0, 15, 0, 15));
		grilleContenu.setPadding(new Insets(10, 60, 15, 60));

		panePrincipal.setCenter(grilleContenu);

		// Textes de l'étape
		etape = new TextFlow();
		GridPane.setMargin(etape, new Insets(0, 0, 10, 0));

		etapeSigne = new Text("   ");
		etapeSigne.getStyleClass().add("signeResume");

		etapeTexte = new Text(" Résumé :");
		etapeTexte.getStyleClass().add("etape");

		etape.getChildren().addAll(etapeSigne, etapeTexte);
		GridPane.setColumnSpan(etape, GridPane.REMAINING);
		etape.setTextAlignment(TextAlignment.CENTER);
		grilleContenu.add(etape, 0, 0);

		texte = new Text("Pour valider la création du championnat, veuillez vérifier les informations que vous avez renseignées précedemment."
				+ " Une fois le Championnat créé ");
		texte2 = new Text("il sera impossible");
		texte3 = new Text(" d'en modifier les informations. Vous pouvez revenir en arrière en cliquant sur les boutons ci dessous "
				+ "et ainsi modifier le championnat.");
		texte2.getStyleClass().add("explicationsTexteGras");
		flow = new TextFlow(texte, texte2, texte3);
		flow.getStyleClass().add("flowPresentationDates");
		flow.setMinHeight(80);
		GridPane.setColumnSpan(flow, GridPane.REMAINING);
		grilleContenu.add(flow, 0, 1);

		// Texte Bas
		texte4 = new Text("Avec cette configuration, aucune équipe n'est desavantagée. Vous pouvez valider le championnat.");
		texte4.getStyleClass().add("flowPresentationDates");
		GridPane.setColumnSpan(texte4, GridPane.REMAINING);
		grilleContenu.add(texte4, 0, 3);
		
		// Grille bas
		grilleBas = new GridPane();
		grilleBas.setPadding(new Insets(10, 75, 10, 75));

		// Boutons du bas
		valider = new Button("valider");
		valider.setId("bottomButton");
		GridPane.setHgrow(valider, Priority.ALWAYS);
		GridPane.setHalignment(valider, HPos.RIGHT);

		ecranDate = new Button("séléction des dates");
		ecranDate.setId("bottomButton");
		ecranDate.setPrefWidth(170);
		GridPane.setMargin(ecranDate, new Insets(0, 10, 0, 0));

		ecranEquipe = new Button("séléction des équipes");
		ecranEquipe.setId("bottomButton");
		ecranEquipe.setPrefWidth(170);
		GridPane.setMargin(ecranEquipe, new Insets(0, 10, 0, 10));

		grilleBas.add(ecranDate, 0, 0);
		grilleBas.add(ecranEquipe, 1, 0);
		grilleBas.add(valider, 2, 0);

		panePrincipal.setBottom(grilleBas);

		// Equipes séléctionnées
		grilleEquipeSelectionnee = new GridPane();
		grilleEquipeSelectionnee.setId("grilleDateRetenues");
		grilleEquipeSelectionnee.setPadding(new Insets(10, 20, 10, 20));
		GridPane.setMargin(grilleEquipeSelectionnee, new Insets(20, 0, 30, 10));

		lEquipeListView = new Label("Equipes séléctionnées :");
		lEquipeListView.setId("lDateListView");
		GridPane.setMargin(lEquipeListView, new Insets(0, 0, 10, 10));

		equipeListView = new ListView<Equipe>();
		equipeListView.setId("dateListView");
		equipeListView.setPrefWidth(350);

		grilleEquipeSelectionnee.add(lEquipeListView, 0, 0);
		grilleEquipeSelectionnee.add(equipeListView, 0, 1);

		grilleContenu.add(grilleEquipeSelectionnee, 0, 2);

		// Dates retenues
		grilleDateRetenues = new GridPane();
		grilleDateRetenues.setId("grilleDateRetenues");
		grilleDateRetenues.setPadding(new Insets(10, 20, 10, 20));
		GridPane.setMargin(grilleDateRetenues, new Insets(20, 0, 30, 10));

		lDateListView = new Label("Dates retenues :");
		lDateListView.setId("lDateListView");
		GridPane.setMargin(lDateListView, new Insets(0, 0, 10, 10));

		dateListView = new ListView<LocalDate>();
		dateListView.setId("dateListView");
		dateListView.setPrefWidth(350);

		grilleDateRetenues.add(lDateListView, 0, 0);
		grilleDateRetenues.add(dateListView, 0, 1);

		grilleContenu.add(grilleDateRetenues, 1, 2);
	}

	public Text getTexte4() {
		return texte4;
	}

	public BorderPane getPanePrincipal() {
		return panePrincipal;
	}

	public Button getEcranDate() {
		return ecranDate;
	}

	public Button getEcranEquipe() {
		return ecranEquipe;
	}

	public ListView<LocalDate> getDateListView() {
		return dateListView;
	}

	public ListView<Equipe> getEquipeListView() {
		return equipeListView;
	}

	public Button getValider() {
		return valider;
	}

}
