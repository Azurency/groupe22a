package vue.fenetreCreation;

import java.time.LocalDate;

import controlleur.ScreensController;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.TextFlow;
import jfxtras.scene.control.CalendarPicker;

public class DateScreenView {
	private BorderPane panePrincipal;
	private GridPane grilleContenu;
	private GridPane grilleDateRetenues;
	
	private Text titre;
	private Text texte;
	private CalendarPicker calendrier;
	private ListView<LocalDate> dateListView;
	private Label lDateListView;
	private Button continuer;
	private Text etapeTexte;
	private GridPane grilleBas;
	private Label lNombreEquipes;
	private TextFlow etape;
	private Text etapeSigne;
	private Text texte2;
	private Text texte3;
	private Tooltip nbrJoursTooltip;
	
	public DateScreenView() {
		
		// Création du paneaux principal
		panePrincipal = new BorderPane();
		panePrincipal.setId("DatePane");
		ScreensController.setMargin(panePrincipal, new Insets(25, 25, 25, 25));
		
		// Titre de la fenetre
		titre = new Text("création du championnat");
		titre.setId("titre");
		BorderPane.setAlignment(titre, Pos.CENTER);
		BorderPane.setMargin(titre, new Insets(7, 0, 7, 0));
		panePrincipal.setTop(titre);
		
		// Contenu
		grilleContenu = new GridPane();
		grilleContenu.setId("grilleContenu");
		BorderPane.setMargin(grilleContenu, new Insets(0, 15, 0, 15));
		grilleContenu.setPadding(new Insets(10, 60, 15, 60));
		
		// Textes de l'étape
		etape = new TextFlow();
		GridPane.setMargin(etape, new Insets(0, 0, 10, 0));
		
		etapeSigne = new Text("   ");
		etapeSigne.setId("signeCalendrier");
		
		etapeTexte = new Text(" Sélection des dates :");
		etapeTexte.getStyleClass().add("etape");
		
		etape.getChildren().addAll(etapeSigne, etapeTexte);
		GridPane.setColumnSpan(etape, GridPane.REMAINING);
		etape.setTextAlignment(TextAlignment.CENTER);
		
		texte = new Text("Pour commencer la création d’un championnat, vous devez séléctionner les journées de match,");
		texte2 = new Text(" au minimum 6 jours");
		texte3 = new Text(". Vous pouvez selectionner autant de jours que vous voulez. Le nombre maximum d’équipes qui peuvent être "
							+ "inscrites dans le championnat dépend du nombre de jours sélectionnés.");
		texte2.getStyleClass().add("explicationsTexteGras");
		TextFlow flow = new TextFlow(texte, texte2, texte3);
		flow.getStyleClass().add("flowPresentationDates");
		flow.setMinHeight(80);
		GridPane.setColumnSpan(flow, GridPane.REMAINING);
		
		// Calendrier
		calendrier = new CalendarPicker();
		calendrier.setMode(CalendarPicker.Mode.MULTIPLE);
		calendrier.setPrefWidth(400);
		calendrier.setMaxHeight(0);
		GridPane.setMargin(calendrier, new Insets(0, 10, 0, 10));
		
		// Dates retenues
		grilleDateRetenues = new GridPane();
		grilleDateRetenues.setId("grilleDateRetenues");
		grilleDateRetenues.setPadding(new Insets(10, 20, 10, 20));
		GridPane.setMargin(grilleDateRetenues, new Insets(20, 0, 30, 10));
		
		lDateListView = new Label("Dates retenues :");
		lDateListView.setId("lDateListView");
		GridPane.setMargin(lDateListView, new Insets(0, 0, 10, 10));
		
		dateListView = new ListView<LocalDate>();
		dateListView.setId("dateListView");
		dateListView.setPrefWidth(350);
		
		// Ajout dans la grilleDateRetenues
		grilleDateRetenues.add(lDateListView, 0, 0);
		grilleDateRetenues.add(dateListView, 0, 1);
		
		// Ajout dans la grilleContenu
		grilleContenu.add(etape, 0, 0);
		grilleContenu.add(flow, 0, 1);
		grilleContenu.add(calendrier, 0, 2);
		grilleContenu.add(grilleDateRetenues, 1, 2);
		
		panePrincipal.setCenter(grilleContenu);
		
		// Grille bas
		grilleBas = new GridPane();
		grilleBas.setPadding(new Insets(10, 75, 10, 75));
		
		// Boutons du bas
		continuer = new Button("continuer");
		continuer.setId("bottomButton");
		GridPane.setHgrow(continuer, Priority.SOMETIMES);
		GridPane.setHalignment(continuer, HPos.RIGHT);
		
		// Texte du bas
		lNombreEquipes = new Label("Vous pouvez inscrire aux maximum 4 équipes");
		lNombreEquipes.setId("lNombreEquipes");
		GridPane.setHgrow(lNombreEquipes, Priority.SOMETIMES);
		
		// Ajout dans la grilleBas
		grilleBas.add(lNombreEquipes, 0, 0);
		grilleBas.add(continuer, 1, 0);
		
		panePrincipal.setBottom(grilleBas);
		
		// Tooltip
		nbrJoursTooltip = new Tooltip("Vous devez sélectionner au moins 6 journées de match");
		
	}
	
	public BorderPane getPanePrincipal() {
		return panePrincipal;
	}
	
	public Button getContinuer() {
		return continuer;
	}
	
	public ListView<LocalDate> getDateListView() {
		return dateListView;
	}
	
	public CalendarPicker getCalendrier() {
		return calendrier;
	}

	public Label getlNombreEquipes() {
		return lNombreEquipes;
	}

	public Tooltip getNbrJoursTooltip() {
		return nbrJoursTooltip;
	}
}
