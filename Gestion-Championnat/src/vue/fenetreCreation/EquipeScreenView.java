package vue.fenetreCreation;

import modele.Equipe;
import modele.Joueur;
import controlleur.ScreensController;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.geometry.VPos;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.TextFlow;

public class EquipeScreenView {
	private BorderPane panePrincipal;
	private StackPane titrePane;
	private Text titre;
	private GridPane panneauGauche;
	private StackPane panneauMilieu;
	private ScrollPane accordeon;
	private TitledPane equipesPane;
	private AnchorPane equipesAnchorPane;
	private ListView<Equipe> equipesListView;
	private Button ajouterEquipe;
	private GridPane grilleBas;
	private Button continuer;
	private Button precedent;
	private GridPane explicationsGrid;
	private TextFlow explications;
	private TextFlow etape;
	private Text etapeSigne;
	private Text etapeTexte;
	private Text explicationsTexte;
	private Text explicationTexteGras;
	private TextFlow titledPaneTitle;
	private Text titledPaneTitleSigne;
	private Text titledPaneTitleTexte;
	private HBox titledPaneTitleCell;
	private Text titledPaneTitleNombre;
	private GridPane joueursEquipeGrid;
	private HBox rechercheBox;
	private TextField rechercheTextField;
	private Text iconTextField;
	private ListView<Joueur> joueursListView;
	private Button ajouterJoueur;
	private GridPane joueurGrid;
	private GridPane barreMilieu;
	private Text prenomJoueur;
	private Text nomJoueur;
	private TextFlow nomCompletJoueur;
	private Text roleJoueurValeur;
	private Text dateNaissanceJoueurValeur;
	private Text professionelJoueurValeur;
	private Text numLicenseJoueurValeur;
	private Text ancienClubJoueurValeur;
	private TextFlow infosJoueur;
	private GridPane pasJoueurGrid;
	private ComboBox<String> roleJoueurComboBox;
	private ScrollPane nomConteneur;
	private Text nomEquipeValeur;
	private Text villeEquipeValeur;
	private TextFlow infoEquipe;
	private Text titreEquipe;
	private Button supprimerJoueur;
	private Button supprimerEquipe;
	private Tooltip pasAssezEquipeTooltip;
	private Tooltip tropEquipeTooltip;
	private Tooltip pasBonFormatEquipeTooltip;
	private Tooltip equipeDejaChargerTooltip;
	private Tooltip pasAssezJoueurTooltip;
	private Tooltip pasEntraineurTooltip;
	

	public EquipeScreenView() {
		panePrincipal = new BorderPane();
		panePrincipal.setId("EquipePane");
		ScreensController.setMargin(panePrincipal, new Insets(25, 25, 25, 25));
		
		// Titre de la fenetre
		titre = new Text("création du championnat");
		titre.setId("titre");
		titrePane = new StackPane(titre);
		titrePane.setId("titrePane");
		BorderPane.setMargin(titrePane, new Insets(0, 15, 10, 15));
		StackPane.setAlignment(titre, Pos.CENTER);
		StackPane.setMargin(titre, new Insets(7, 0, 7, 0));
		
		panePrincipal.setTop(titrePane);
		
		// Panneau Gauche
		panneauGauche = new GridPane();
		//panneauGauche.setGridLinesVisible(true);
		panneauGauche.setPrefWidth(205);
		panneauGauche.setId("panneauGauche");
		BorderPane.setMargin(panneauGauche, new Insets(0, 10, 0, 30));
		
		ajouterEquipe = new Button("+ ajouter une equipe");
		GridPane.setMargin(ajouterEquipe, new Insets(10, 0, 0, 0));
		ajouterEquipe.getStyleClass().add("addButton");
		panneauGauche.add(ajouterEquipe, 0, 1);
		
		accordeon = new ScrollPane();
		GridPane.setHgrow(accordeon, Priority.ALWAYS);
		GridPane.setVgrow(accordeon, Priority.ALWAYS);
		accordeon.setPrefWidth(ScrollPane.USE_COMPUTED_SIZE);
		accordeon.setPrefHeight(ScrollPane.USE_COMPUTED_SIZE);
		accordeon.setId("accordeon");
		equipesPane = new TitledPane();
		equipesPane.setId("equipesPane");
		equipesPane.setPrefHeight(TitledPane.USE_COMPUTED_SIZE);
		equipesPane.setCollapsible(false);
		equipesPane.setExpanded(true);
		
		// formatage du titre du TitledPane
		titledPaneTitleCell = new HBox();
		titledPaneTitleNombre = new Text("56");
		titledPaneTitleNombre.getStyleClass().add("titledPaneTitleNombre");
		HBox.setMargin(titledPaneTitleNombre, new Insets(3, 0, 0, 65));
		titledPaneTitleSigne = new Text("  ");
		titledPaneTitleSigne.getStyleClass().add("signeEquipeTitledPane");
		titledPaneTitleTexte = new Text("Equipes");
		titledPaneTitleTexte.getStyleClass().add("equipeTitledPane");
		titledPaneTitle = new TextFlow(titledPaneTitleSigne, titledPaneTitleTexte);
		titledPaneTitleCell.getChildren().addAll(titledPaneTitle, titledPaneTitleNombre);
		equipesPane.setGraphic(titledPaneTitleCell);
		
		// Ajout des style du bouton sélectionné par défaut
		titledPaneTitleCell.getStyleClass().add("titleCellSelected");
		titledPaneTitleNombre.getStyleClass().add("titledPaneTitleNombreSelected");
		titledPaneTitleSigne.getStyleClass().add("titledPaneTitleSigneSelected");
		titledPaneTitleTexte.getStyleClass().add("titledPaneTitleTexteSelected");
		
		equipesAnchorPane = new AnchorPane();
		equipesAnchorPane.setId("equipesAnchorPane");
		equipesAnchorPane.setPadding(new Insets(0));
		equipesAnchorPane.setPrefWidth(AnchorPane.USE_COMPUTED_SIZE);
		equipesAnchorPane.setPrefHeight(AnchorPane.USE_COMPUTED_SIZE);
		equipesListView = new ListView<Equipe>();
		equipesListView.setId("equipeListView");
		equipesListView.setPrefWidth(190);
		Label placeholder = new Label("Aucune équipe");
		placeholder.setId("placeholder");
		equipesListView.setPlaceholder(placeholder);
		AnchorPane.setLeftAnchor(equipesListView, 0.0);
		AnchorPane.setRightAnchor(equipesListView, 0.0);
		AnchorPane.setTopAnchor(equipesListView, 0.0);
		AnchorPane.setBottomAnchor(equipesListView, 0.0);
		equipesAnchorPane.getChildren().add(equipesListView);
		
		equipesPane.setContent(equipesAnchorPane);
		equipesPane.setPrefHeight(TitledPane.USE_COMPUTED_SIZE);
		accordeon.setContent(equipesPane);
		panneauGauche.add(accordeon, 0, 0);
		
		panePrincipal.setLeft(panneauGauche);
		
		// Panneau Millieu
		panneauMilieu = new StackPane();
		
		// Explications
		explicationsGrid = new GridPane();
		explicationsGrid.getStyleClass().add("explicationsGrid");
		StackPane.setMargin(explicationsGrid, new Insets(0, 20, 0, 0));
		
		etapeSigne = new Text("   ");
		etapeSigne.getStyleClass().add("etapeEquipeSigne");
		etapeTexte = new Text("Sélection des équipes :");
		etapeTexte.getStyleClass().add("etape");
		etape = new TextFlow(etapeSigne,etapeTexte);
		etape.setTextAlignment(TextAlignment.CENTER);
		GridPane.setMargin(etape, new Insets(0, 0, 40, 0));
		explicationsGrid.add(etape, 0, 0);
		
		explicationsTexte = new Text("Vous devez maintenant choisir les équipes participant au championnat. "
				+ "Les equipes des championnats précédents (si existants) ont été chargées.\n\n"
				+ "Vous pouvez rajouter des équipes en cliquant sur le bouton ajouter une équipe. "
				+ "Vous pouvez aussi déplacer les joueurs entre les équipes par glisser-déposer. "
				+ "Les joueurs qui ne sont dans aucune équipe sont rassemblés sous l’onglet «sans équipe».\n\n"
				+ "Par rapport au nombre de dates que vous avez sélectionnées précédemment, vous devez inscrire ");
		explicationTexteGras = new Text("entre 4 et 48 équipes.");
		explicationTexteGras.getStyleClass().add("explicationsTexteGras");
		explications = new TextFlow(explicationsTexte, explicationTexteGras);
		explications.getStyleClass().add("explicationsTexte");
		explicationsGrid.add(explications, 0, 1);
		
		// Les joueurs d'une équipe sélectionnée
		joueursEquipeGrid = new GridPane();
		
		barreMilieu = new GridPane();
		barreMilieu.setId("barreMilieu");
		GridPane.setMargin(barreMilieu, new Insets(0, 20, 0, 0));
		barreMilieu.setMinWidth(225);
		joueursEquipeGrid.add(barreMilieu, 0, 0);
		
		rechercheBox = new HBox();
		rechercheBox.getStyleClass().add("recherche-box");
		rechercheTextField = new TextField("rechercher ...");
		rechercheTextField.getStyleClass().add("champ-recherche");
		HBox.setHgrow(rechercheTextField, Priority.ALWAYS);
		iconTextField = new Text("");
		iconTextField.getStyleClass().add("signe-recherche");
		HBox.setMargin(iconTextField, new Insets(5, 5, 0, 0));
		rechercheBox.getChildren().addAll(iconTextField, rechercheTextField);
		GridPane.setMargin(rechercheBox, new Insets(0, 0, 10, 0));
		barreMilieu.add(rechercheBox, 0, 0);
		
		joueursListView = new ListView<Joueur>();
		joueursListView.setPrefWidth(225);
		joueursListView.setPrefHeight(450);
		GridPane.setMargin(joueursListView, new Insets(0, 0, 10, 0));
		Label placeholder2 = new Label("Aucun joueur");
		placeholder2.getStyleClass().add("placeholder2");
		joueursListView.setPlaceholder(placeholder2);
		barreMilieu.add(joueursListView, 0, 1);
		
		ajouterJoueur = new Button("+ ajouter un joueur");
		ajouterJoueur.getStyleClass().add("addButton");
		barreMilieu.add(ajouterJoueur, 0, 2);
		
		// Le joueur selectionné
		
		nomConteneur = new ScrollPane();
		nomConteneur.getStyleClass().add("nomCompletScroll");
		joueurGrid = new GridPane();
		prenomJoueur = new Text("\n");
		prenomJoueur.getStyleClass().add("prenomJoueurEquipe");
		nomJoueur = new Text();
		nomJoueur.getStyleClass().add("nomJoueurEquipe");
		nomCompletJoueur = new TextFlow(prenomJoueur, nomJoueur);
		nomCompletJoueur.getStyleClass().add("nomCompletJoueurEquipe");
		nomCompletJoueur.setPrefWidth(400);
		roleJoueurComboBox = new ComboBox<String>();
		roleJoueurComboBox.setPrefWidth(100);
		dateNaissanceJoueurValeur = new Text("\n");
		dateNaissanceJoueurValeur.getStyleClass().add("valeurEquipe");
		professionelJoueurValeur = new Text("\n");
		professionelJoueurValeur.getStyleClass().add("valeurEquipe");
		numLicenseJoueurValeur = new Text("\n");
		numLicenseJoueurValeur.getStyleClass().add("valeurEquipe");
		ancienClubJoueurValeur = new Text("\n");
		ancienClubJoueurValeur.getStyleClass().add("valeurEquipeUnmutable");
		nomConteneur.setContent(nomCompletJoueur);
		nomConteneur.setPrefHeight(180);
		nomConteneur.setPrefWidth(450);
		joueurGrid.add(nomConteneur, 0, 0);
		infosJoueur = new TextFlow(new Text("rôle : "), roleJoueurComboBox,
				  new Text("\ndate de naissance : "), dateNaissanceJoueurValeur,
				  new Text("\nprofessionnel : "), professionelJoueurValeur,
				  new Text("\nnuméro de licence : "), numLicenseJoueurValeur,
				  new Text("\nancien club : "), ancienClubJoueurValeur);
		infosJoueur.getStyleClass().add("infosJoueurEquipe");
		joueurGrid.add(infosJoueur, 0, 1);
		supprimerJoueur = new Button("supprimer le joueur");
		supprimerJoueur.getStyleClass().add("addButton");
		joueurGrid.add(supprimerJoueur, 0, 2);
		GridPane.setHalignment(supprimerJoueur, HPos.RIGHT);
		GridPane.setVgrow(supprimerJoueur, Priority.ALWAYS);
		GridPane.setValignment(supprimerJoueur, VPos.BOTTOM);
		GridPane.setMargin(supprimerJoueur, new Insets(0, 0, 10, 0));
		
		
		panePrincipal.setCenter(panneauMilieu);
		
		// Grille bas
		grilleBas = new GridPane();
		grilleBas.setId("grilleBas");
		BorderPane.setMargin(grilleBas, new Insets(15, 15, 0, 15));
		grilleBas.setPadding(new Insets(10, 35, 10, 35));
				
		// Boutons du bas
		continuer = new Button("continuer");
		continuer.setId("bottomButton");
		GridPane.setHgrow(continuer, Priority.SOMETIMES);
		GridPane.setHalignment(continuer, HPos.RIGHT);
		
		precedent = new Button("précédent");
		precedent.setId("bottomButton");
		GridPane.setHgrow(precedent, Priority.SOMETIMES);
				
		// Ajout dans la grilleBas
		grilleBas.add(precedent, 0, 0);
		grilleBas.add(continuer, 1, 0);
				
		panePrincipal.setBottom(grilleBas);
		
		showExplications();
		//showPasJoueurGrid();
		//showJoueurGrid();
		
		// Pas joueur selectionné
		nomEquipeValeur = new Text("\n");
		villeEquipeValeur = new Text("\n");
		nomEquipeValeur.getStyleClass().add("valeurEquipe");
		villeEquipeValeur.getStyleClass().add("valeurEquipe");
		titreEquipe = new Text("Informations sur l'equipe :");
		titreEquipe.setWrappingWidth(380);
		titreEquipe.getStyleClass().add("titreEquipe");
		infoEquipe = new TextFlow(titreEquipe, new Text("\nnom : "), nomEquipeValeur, new Text("\nville : "), villeEquipeValeur);
		infoEquipe.setPrefWidth(395);
		infoEquipe.getStyleClass().add("infosEquipe");
		supprimerEquipe = new Button("supprimer l'équipe");
		supprimerEquipe.getStyleClass().add("addButton");
		GridPane.setHalignment(supprimerEquipe, HPos.RIGHT);
		GridPane.setVgrow(supprimerEquipe, Priority.ALWAYS);
		GridPane.setValignment(supprimerEquipe, VPos.BOTTOM);
		GridPane.setMargin(supprimerEquipe, new Insets(0, 0, 10, 0));
		
		// TOOLTIP
		pasAssezEquipeTooltip = new Tooltip("Vous devez inscrire aux moins 4 équipes");
		tropEquipeTooltip = new Tooltip("Vous devez inscrire moins de 16 équipes");
		pasBonFormatEquipeTooltip = new Tooltip("Ceci n'est pas un fichier d'équipe");
		equipeDejaChargerTooltip = new Tooltip("Cette équipe a déjà été chargée");
		pasAssezJoueurTooltip = new Tooltip("Vous devez inscrire aux moins 7 joueurs par équipe");
		pasEntraineurTooltip = new Tooltip("Vous devez inscrire aux moins un entraineur par équipe");
	}

	public void showJoueurGrid() {
		joueursEquipeGrid.getChildren().remove(pasJoueurGrid);
		joueursEquipeGrid.getChildren().remove(joueurGrid);
		joueurGrid.getStyleClass().add("joueurGrid");
		GridPane.setHgrow(joueurGrid, Priority.ALWAYS);
		GridPane.setMargin(joueurGrid, new Insets(0, 13, 0, 0));
		joueursEquipeGrid.add(joueurGrid, 1, 0);
		showInfosJoueurs();
	}
	
	public void showPasJoueurGrid(boolean avecSupprimer) {
		joueursEquipeGrid.getChildren().remove(pasJoueurGrid);
		joueursEquipeGrid.getChildren().remove(joueurGrid);
		pasJoueurGrid = new GridPane();
		pasJoueurGrid.getStyleClass().add("pasJoueurGrid");
		Text pasJoueur = new Text("Pas de joueur sélectionné");
		pasJoueur.setWrappingWidth(380);
		pasJoueur.getStyleClass().add("pasJoueur");
		GridPane.setMargin(pasJoueur, new Insets(0, 0, 10, 0));
		pasJoueurGrid.add(pasJoueur, 0, 0);
		pasJoueurGrid.add(infoEquipe, 0, 2);
		if (avecSupprimer) {
			pasJoueurGrid.add(supprimerEquipe, 0, 3);
		}
		joueursEquipeGrid.add(pasJoueurGrid, 1, 0);
		showInfosJoueurs();
	}
	
	public void showExplications() {
		if (!panneauMilieu.getChildren().isEmpty()) {
			panneauMilieu.getChildren().remove(0);
		}
		panneauMilieu.getChildren().add(0, explicationsGrid);
	}
	
	public void showInfosJoueurs() {
		if (!panneauMilieu.getChildren().isEmpty()) {
			panneauMilieu.getChildren().remove(0);
		}
		panneauMilieu.getChildren().add(0, joueursEquipeGrid);
	}

	public TextFlow getInfosJoueur() {
		return infosJoueur;
	}

	public Text getPrenomJoueur() {
		return prenomJoueur;
	}

	public Text getNomJoueur() {
		return nomJoueur;
	}

	public TextFlow getNomCompletJoueur() {
		return nomCompletJoueur;
	}

	public GridPane getJoueurGrid() {
		return joueurGrid;
	}

	public Text getRoleJoueurValeur() {
		return roleJoueurValeur;
	}

	public ComboBox<String> getRoleJoueurComboBox() {
		return roleJoueurComboBox;
	}

	public GridPane getPanneauGauche() {
		return panneauGauche;
	}

	public Text getDateNaissanceJoueurValeur() {
		return dateNaissanceJoueurValeur;
	}

	public Text getProfessionelJoueurValeur() {
		return professionelJoueurValeur;
	}

	public Text getNumLicenseJoueurValeur() {
		return numLicenseJoueurValeur;
	}

	public Text getAncienClubJoueurValeur() {
		return ancienClubJoueurValeur;
	}

	public Text getTitledPaneTitleNombre() {
		return titledPaneTitleNombre;
	}

	public Text getTitledPaneTitleSigne() {
		return titledPaneTitleSigne;
	}

	public Text getTitledPaneTitleTexte() {
		return titledPaneTitleTexte;
	}

	public ListView<Equipe> getEquipesListView() {
		return equipesListView;
	}
	
	public ListView<Joueur> getJoueursListView() {
		return joueursListView;
	}
	
	public HBox getTitledPaneTitleCell() {
		return titledPaneTitleCell;
	}
	
	public TitledPane getEquipesPane() {
		return equipesPane;
	}

	public ScrollPane getAccordeon() {
		return accordeon;
	}
	
	public AnchorPane getEquipesAnchorPane() {
		return equipesAnchorPane;
	}
	
	public BorderPane getPanePrincipal() {
		return panePrincipal;
	}
	
	public Button getPrecedent() {
		return precedent;
	}

	public Button getContinuer() {
		return continuer;
	}

	public TextField getRechercheTextField() {
		return rechercheTextField;
	}

	public Button getAjouterEquipe() {
		return ajouterEquipe;
	}

	public Button getAjouterJoueur() {
		return ajouterJoueur;
	}

	public Text getNomEquipeValeur() {
		return nomEquipeValeur;
	}

	public Text getVilleEquipeValeur() {
		return villeEquipeValeur;
	}

	public TextFlow getInfoEquipe() {
		return infoEquipe;
	}

	public Button getSupprimerJoueur() {
		return supprimerJoueur;
	}

	public Button getSupprimerEquipe() {
		return supprimerEquipe;
	}

	public Text getExplicationTexteGras() {
		return explicationTexteGras;
	}

	public Tooltip getPasAssezEquipeTooltip() {
		return pasAssezEquipeTooltip;
	}

	public Tooltip getTropEquipeTooltip() {
		return tropEquipeTooltip;
	}

	public Tooltip getPasBonFormatEquipeTooltip() {
		return pasBonFormatEquipeTooltip;
	}

	public Tooltip getEquipeDejaChargerTooltip() {
		return equipeDejaChargerTooltip;
	}

	public Tooltip getPasAssezJoueurTooltip() {
		return pasAssezJoueurTooltip;
	}

	public Tooltip getPasEntraineurTooltip() {
		return pasEntraineurTooltip;
	}
}
