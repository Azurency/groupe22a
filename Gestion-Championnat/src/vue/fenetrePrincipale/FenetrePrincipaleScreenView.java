package vue.fenetrePrincipale;

import modele.Action;
import modele.Equipe;
import modele.Joueur;
import modele.Journee;
import modele.Match;
import controlleur.ScreensController;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.text.TextFlow;

public class FenetrePrincipaleScreenView {
	private BorderPane panePrincipal;
	private Text titre;
	private StackPane titrePane;
	private GridPane panneauGauche;
	private Button accueilButton;
	private Button classementButton;
	private Button matchsButton;
	private Button journeesButton;
	private Button statistiquesButton;
	private GridPane panneauMilieuAccueil;
	private ListView<Equipe> podiumListView;
	private ListView<Match> prochainsMatchsListView;
	private TableView<Match> recapitulatifTableView;
	private TableColumn<Match, Integer> matchColumn;
	private TableColumn<Match, Journee> dateColumn;
	private TableColumn<Match, Boolean> actionColumn;
	private GridPane panneauMilieuClassement;
	private TableView<Equipe> classementTableView;
	private TableColumn<Equipe, String> equipeClassementColumn;
	private TableColumn<Equipe, String> villeClassementColumn;
	private TableColumn<Equipe, Integer> pointsClassementColumn;
	private GridPane panneauMilieuMatchs;
	private TableView<Match> matchsTableView;
	private TableColumn<Match, Integer> matchForMatchsColumn;
	private TableColumn<Match, Journee> dateForMatchsColumn;
	private TableColumn<Match, Boolean> actionForMatchsColumn;
	private GridPane panneauMilieuJournees;
	private TextField rechercheDateTextField;
	private ListView<Journee> journeesListView;
	private TextField rechercheMatchTextField;
	private ListView<Match> matchListView;
	private Text titreMatch;
	private Text etatEquipe1;
	private Text etatEquipe2;
	private Text scoreEquipe1;
	private Text scoreEquipe2;
	private Text nombreDeFautes;
	private Text nombreDePasses;
	private GridPane pasInfoMatch;
	private GridPane infoMatch;
	private ListView<Action> actionsLV;
	private GridPane panneauMilieuStatistiques;
	private GridPane barreJoueurs;
	private HBox rechercheStatistiquesBox;
	private TextField rechercheStatistiquesTextField;
	private Text iconStatistiqueTextField;
	private ListView<Joueur> joueursStatistiquesListView;
	private Text nomStat;
	private LineChart<String, Number> detailLineChart;
	private Text nombreButTotal;
	private Text nombrePassesTotales;
	private Text nombreFautesTotales;
	private Text nombreMoyenButsParMatch;
	private GridPane pasStatistiqueGrid;
	private GridPane statistiqueGrid;
	private ScrollPane statScroll;
	private GridPane pasChargerMatch;
	private GridPane recapMatch;
	private Text titreProchainMatch;
	private GridPane prochainsMatchsGrid;
	private Button supprimerChampionnat;

	public FenetrePrincipaleScreenView() {
		initPanePrincipal();
		initWindowTitle();
		initPanneauGauche();
		initContent();
	}

	private void initPanePrincipal() {
		panePrincipal = new BorderPane();
		panePrincipal.getStyleClass().add("contentPane");
		ScreensController.setMargin(panePrincipal, new Insets(25, 25, 25, 25));
	}

	private void initWindowTitle() {
		titre = new Text("gestion du championnat");
		titre.getStyleClass().add("titre");
		titrePane = new StackPane(titre);
		titrePane.setId("titrePane");
		BorderPane.setMargin(titrePane, new Insets(0, 15, 10, 15));
		StackPane.setAlignment(titre, Pos.CENTER);
		StackPane.setMargin(titre, new Insets(7, 0, 7, 0));

		panePrincipal.setTop(titrePane);
	}

	private void initPanneauGauche() {
		panneauGauche = new GridPane();
		panneauGauche.setId("panneauGauche");
		BorderPane.setMargin(panneauGauche, new Insets(0, 10, 0, 30));

		accueilButton = initButton("", "Accueil", "signePrincipalGauche");
		classementButton = initButton("", "Classement", "signeRankGauche");
		matchsButton = initButton("", "Matchs", "signeFlaticonGauche");
		journeesButton = initButton("", "Journées", "signeCalendarGauche");
		statistiquesButton = initButton("", "Statistiques", "signePrincipalGauche");

		panneauGauche.add(accueilButton, 0, 0);
		panneauGauche.add(classementButton, 0, 1);
		panneauGauche.add(matchsButton, 0, 2);
		panneauGauche.add(journeesButton, 0, 3);
		panneauGauche.add(statistiquesButton, 0, 4);

		accueilButton.getStyleClass().add("panneauGaucheButtonpressed");

		panePrincipal.setLeft(panneauGauche);
	}

	private void initContent() {
		initAccueilContent();
		initClassementContent();
		initMatchsContent();
		initJourneesContent();
		initStatistiquesContent();
		showAccueilContent();
	}

	public void showAccueilContent() {
		panePrincipal.setCenter(panneauMilieuAccueil);
	}

	private void initAccueilContent() {
		panneauMilieuAccueil = new GridPane();
		panneauMilieuAccueil.getStyleClass().add("panneauMilieu");
		// Texte explicatif
		Text text1 = new Text("Bienvenu sur l’application de gestion du championnat de Handball. Cet écran vous donne ");
		Text text2 = new Text("un aperçu global");
		text2.getStyleClass().add("explicationsTexteGras");
		Text text3 = new Text(" de l’avancement du championnat (podium, matchs à venir, compte rendu à charger).");
		TextFlow explicationFlow = new TextFlow(text1, text2, text3);
		explicationFlow.getStyleClass().add("explicationsTexte");
		explicationFlow.setTextAlignment(TextAlignment.JUSTIFY);
		GridPane.setMargin(explicationFlow, new Insets(0, 0, 20, 0));
		panneauMilieuAccueil.add(explicationFlow, 0, 0);

		// Podium + prochains matchs
		GridPane grillePodium = new GridPane();
		//grillePodium.setGridLinesVisible(true);
		grillePodium.getStyleClass().add("podiumGrid");

		Text signePodium = new Text("  ");
		signePodium.getStyleClass().add("signeRankGaucheBig");
		Text textePodium = new Text("Podium");
		textePodium.getStyleClass().add("etape");
		TextFlow titrePodium = new TextFlow(signePodium, textePodium);
		grillePodium.add(titrePodium, 0, 0);

		Text sousTitrePodium = new Text("Le classement complet est visible dans «classement»");
		sousTitrePodium.getStyleClass().add("sous-titre");
		grillePodium.add(sousTitrePodium, 0, 1);

		podiumListView = new ListView<Equipe>();
		podiumListView.getStyleClass().addAll("customListView", "podiumLV");
		podiumListView.setPrefWidth(400);
		podiumListView.setMaxHeight(260);
		GridPane.setMargin(podiumListView, new Insets(0, 30, 0, 0));
		grillePodium.add(podiumListView, 0, 2);

		prochainsMatchsGrid = new GridPane();
		//prochainsMatchsGrid.setGridLinesVisible(true);
		prochainsMatchsGrid.getStyleClass().add("prochainsMatchsGrid");

		titreProchainMatch = new Text("Prochain matchs :");
		titreProchainMatch.getStyleClass().add("titreSmall");
		prochainsMatchsGrid.add(titreProchainMatch, 0, 0);

		prochainsMatchsListView = new ListView<Match>();
		prochainsMatchsListView.getStyleClass().addAll("customListView", "prochainsLV");
		prochainsMatchsListView.setPrefWidth(320);
		prochainsMatchsListView.setMaxHeight(105);
		prochainsMatchsGrid.add(prochainsMatchsListView, 0, 1);

		grillePodium.add(prochainsMatchsGrid, 1, 0);
		GridPane.setRowSpan(prochainsMatchsGrid, GridPane.REMAINING);

		panneauMilieuAccueil.add(grillePodium, 0, 1);

		// Récapitulatif
		Text recapitulatifTitle = new Text("Récapitulatif :");
		recapitulatifTitle.getStyleClass().add("etapeLight");
		GridPane.setMargin(recapitulatifTitle, new Insets(20, 0, 20, 10));
		panneauMilieuAccueil.add(recapitulatifTitle, 0, 2);

		recapitulatifTableView = new TableView<Match>();	
		recapitulatifTableView.getStyleClass().add("customTableView");
		matchColumn = new TableColumn<Match, Integer>("Match");
		matchColumn.setPrefWidth(430);
		matchColumn.setSortable(false);
		matchColumn.setEditable(false);
		matchColumn.setResizable(false);
		dateColumn = new TableColumn<Match, Journee>("Date");
		dateColumn.setPrefWidth(200);
		dateColumn.setSortable(false);
		dateColumn.setEditable(false);
		dateColumn.setResizable(false);
		actionColumn = new TableColumn<Match, Boolean>("Action");
		actionColumn.setPrefWidth(200);
		actionColumn.setSortable(false);
		actionColumn.setEditable(false);
		actionColumn.setResizable(false);
		recapitulatifTableView.getColumns().addAll(matchColumn, dateColumn, actionColumn);
		panneauMilieuAccueil.add(recapitulatifTableView, 0, 3);
		supprimerChampionnat = new Button();
		
		//showChampionnatFinieContent();
	}
	
	public void showChampionnatFinieContent() {
		titreProchainMatch.setText("Le championnat est fini :");
		GridPane.setMargin(titreProchainMatch, new Insets(20, 0, 0, 0));
		prochainsMatchsGrid.getChildren().remove(prochainsMatchsListView);
		supprimerChampionnat.setText("Terminer le championnat");
		supprimerChampionnat.getStyleClass().add("bigActionButton");
		GridPane.setMargin(supprimerChampionnat, new Insets(15, 0, 0, 0));
		Tooltip suppWarning = new Tooltip("Cette action supprime définitivement tout le championat !");
		suppWarning.setAutoHide(true);
		suppWarning.setConsumeAutoHidingEvents(false);
		supprimerChampionnat.setTooltip(suppWarning);
		prochainsMatchsGrid.add(supprimerChampionnat, 0, 1);
	}

	public void showClassementContent() {
		panePrincipal.setCenter(panneauMilieuClassement);
	}

	private void initClassementContent() {
		panneauMilieuClassement = new GridPane();
		panneauMilieuClassement.getStyleClass().add("panneauMilieuClassement");

		// Excplications
		Text text1 = new Text("Cet écran affiche ");
		Text text2 = new Text("le classement complet");
		text2.getStyleClass().add("explicationsTexteGras");
		Text text3 = new Text(" du championnat à la date actuelle.");
		TextFlow explicationFlow = new TextFlow(text1, text2, text3);
		explicationFlow.getStyleClass().add("explicationsTexte");
		explicationFlow.setTextAlignment(TextAlignment.JUSTIFY);
		GridPane.setMargin(explicationFlow, new Insets(0, 0, 5, 0));
		panneauMilieuClassement.add(explicationFlow, 0, 0);

		// Classement
		GridPane classementPane = new GridPane();
		classementPane.setPrefWidth(850);
		classementPane.getStyleClass().add("classementPane");
		Text titreSigne = new Text(" ");
		titreSigne.getStyleClass().add("signeRankGaucheBig");
		Text titreText = new Text("Classement");
		titreText.getStyleClass().add("etapeLight");
		TextFlow titreClassement = new TextFlow(titreSigne, titreText);
		GridPane.setMargin(titreClassement, new Insets(0, 0, 10, 0));
		classementPane.add(titreClassement, 0, 0);

		classementTableView = new TableView<Equipe>();
		classementTableView.setPrefHeight(600);
		classementTableView.setPrefWidth(800);
		classementTableView.getStyleClass().addAll("customTableView2", "classementTableView");
		equipeClassementColumn = new TableColumn<Equipe, String>("Équipe");
		equipeClassementColumn.setPrefWidth(420);
		equipeClassementColumn.setEditable(false);
		equipeClassementColumn.setResizable(false);
		equipeClassementColumn.setSortable(false);
		villeClassementColumn = new TableColumn<Equipe, String>("Ville");
		villeClassementColumn.getStyleClass().add("recapDate");
		villeClassementColumn.setPrefWidth(200);
		villeClassementColumn.setEditable(false);
		villeClassementColumn.setResizable(false);
		villeClassementColumn.setSortable(false);
		pointsClassementColumn = new TableColumn<Equipe, Integer>("Points");
		pointsClassementColumn.setPrefWidth(100);
		pointsClassementColumn.setEditable(false);
		pointsClassementColumn.setResizable(false);
		pointsClassementColumn.setSortable(false);
		classementTableView.getColumns().addAll(equipeClassementColumn, villeClassementColumn, pointsClassementColumn);
		classementPane.add(classementTableView, 0, 1);

		panneauMilieuClassement.add(classementPane, 0, 1);

	}

	public void showMatchsContent() {
		panePrincipal.setCenter(panneauMilieuMatchs);
	}

	private void initMatchsContent() {
		panneauMilieuMatchs = new GridPane();
		panneauMilieuMatchs.getStyleClass().add("panneauMilieuMatchs");

		// Explications
		Text text1 = new Text("Voici la liste de ");
		Text text2 = new Text("tout les matchs");
		text2.getStyleClass().add("explicationsTexteGras");
		Text text3 = new Text(" du championnat.");
		TextFlow explicationFlow = new TextFlow(text1, text2, text3);
		explicationFlow.getStyleClass().add("explicationsTexte");
		explicationFlow.setTextAlignment(TextAlignment.JUSTIFY);
		GridPane.setMargin(explicationFlow, new Insets(0, 0, 10, 0));
		panneauMilieuMatchs.add(explicationFlow, 0, 0);

		// Matchs
		GridPane matchsPane = new GridPane();
		matchsPane.setPrefWidth(850);
		matchsPane.getStyleClass().add("matchsPane");
		Text titreText = new Text("Liste des matchs");
		titreText.getStyleClass().add("etapeLight");
		GridPane.setMargin(titreText, new Insets(0, 0, 10, 0));
		matchsPane.add(titreText, 0, 0);

		matchsTableView = new TableView<Match>();
		matchsTableView.setPrefHeight(600);
		matchsTableView.setPrefWidth(800);
		matchsTableView.getStyleClass().addAll("customTableView2", "matchsTableView");
		matchForMatchsColumn = new TableColumn<Match, Integer>("Match");
		matchForMatchsColumn.setPrefWidth(410);
		matchForMatchsColumn.setSortable(false);
		matchForMatchsColumn.setEditable(false);
		matchForMatchsColumn.setResizable(false);
		dateForMatchsColumn = new TableColumn<Match, Journee>("Date");
		dateForMatchsColumn.setPrefWidth(165);
		dateForMatchsColumn.setSortable(false);
		dateForMatchsColumn.setEditable(false);
		dateForMatchsColumn.setResizable(false);
		actionForMatchsColumn = new TableColumn<Match, Boolean>("Action");
		actionForMatchsColumn.setPrefWidth(190);
		actionForMatchsColumn.setSortable(false);
		actionForMatchsColumn.setEditable(false);
		actionForMatchsColumn.setResizable(false);
		matchsTableView.getColumns().addAll(matchForMatchsColumn, dateForMatchsColumn, actionForMatchsColumn);
		matchsPane.add(matchsTableView, 0, 1);

		panneauMilieuMatchs.add(matchsPane, 0, 1);
	}

	public void showJourneesContent() {
		panePrincipal.setCenter(panneauMilieuJournees);
	}

	private void initJourneesContent() {
		panneauMilieuJournees = new GridPane();
		panneauMilieuJournees.getStyleClass().add("panneauMilieuJournees");

		GridPane barreDate = new GridPane();
		barreDate.setId("barreMilieu");
		GridPane.setMargin(barreDate, new Insets(0, 10, 0, 0));

		HBox rechercheDateBox = new HBox();
		rechercheDateBox.getStyleClass().add("recherche-box");
		rechercheDateTextField = new TextField("rechercher ...");
		rechercheDateTextField.getStyleClass().add("champ-recherche");
		HBox.setHgrow(rechercheDateTextField, Priority.ALWAYS);
		rechercheDateTextField.setPrefWidth(90);
		Text iconDateTextField = new Text("");
		iconDateTextField.getStyleClass().add("signe-recherche");
		HBox.setMargin(iconDateTextField, new Insets(5, 5, 0, 0));
		rechercheDateBox.getChildren().addAll(iconDateTextField, rechercheDateTextField);
		GridPane.setMargin(rechercheDateBox, new Insets(0, 0, 10, 0));
		barreDate.add(rechercheDateBox, 0, 0);

		journeesListView = new ListView<Journee>();
		journeesListView.setPrefWidth(135);
		journeesListView.setPrefHeight(550);
		GridPane.setMargin(journeesListView, new Insets(0, 0, 10, 0));
		Label placeholder2 = new Label("Aucune journée");
		placeholder2.getStyleClass().add("placeholder2");
		journeesListView.setPlaceholder(placeholder2);
		barreDate.add(journeesListView, 0, 1);

		panneauMilieuJournees.add(barreDate, 0, 0);

		GridPane barreMatch = new GridPane();
		barreMatch.setId("barreMilieu");
		GridPane.setMargin(barreMatch, new Insets(0, 10, 0, 0));

		HBox rechercheMatchBox = new HBox();
		rechercheMatchBox.getStyleClass().add("recherche-box");
		rechercheMatchTextField = new TextField("rechercher ...");
		rechercheMatchTextField.getStyleClass().add("champ-recherche");
		rechercheMatchTextField.setPrefWidth(160);
		HBox.setHgrow(rechercheMatchTextField, Priority.ALWAYS);
		Text iconMatchTextField = new Text("");
		iconMatchTextField.getStyleClass().add("signe-recherche");
		HBox.setMargin(iconMatchTextField, new Insets(5, 5, 0, 0));
		rechercheMatchBox.getChildren().addAll(iconMatchTextField, rechercheMatchTextField);
		GridPane.setMargin(rechercheMatchBox, new Insets(0, 0, 10, 0));
		barreMatch.add(rechercheMatchBox, 0, 0);

		matchListView = new ListView<Match>();
		matchListView.setPrefWidth(0);
		matchListView.setPrefHeight(550);
		Label placeholder3 = new Label("Aucune date sélectionné");
		placeholder3.getStyleClass().add("placeholder2");
		matchListView.setPlaceholder(placeholder3);
		barreMatch.add(matchListView, 0, 1);

		panneauMilieuJournees.add(barreMatch, 1, 0);

		infoMatch = new GridPane();
		infoMatch.setPrefWidth(495);
		GridPane.setMargin(infoMatch, new Insets(0, 0, 10, 0));
		infoMatch.getStyleClass().add("infoMatch");
		pasInfoMatch = new GridPane();
		pasInfoMatch.setPrefWidth(495);
		pasInfoMatch.getStyleClass().add("infoMatch");
		pasChargerMatch = new GridPane();
		pasChargerMatch.setPrefWidth(495);
		pasChargerMatch.getStyleClass().add("infoMatch");
		
		Text pasMatch = new Text("Pas de match sélectionné");
		pasMatch.setWrappingWidth(400);
		pasMatch.getStyleClass().add("pasMatch");
		pasInfoMatch.add(pasMatch, 0, 0);

		recapMatch = new GridPane();
		recapMatch.getStyleClass().add("recapMatch");
		recapMatch.setPrefWidth(500);
		GridPane.setMargin(recapMatch, new Insets(0, 0, 10, 0));
		ColumnConstraints column1 = new ColumnConstraints();
		column1.setPercentWidth(50);
		ColumnConstraints column2 = new ColumnConstraints();
		column2.setPercentWidth(50);
		recapMatch.getColumnConstraints().addAll(column1, column2);
		titreMatch = new Text();
		titreMatch.getStyleClass().add("titreMatch");
		GridPane.setColumnSpan(titreMatch, GridPane.REMAINING);
		GridPane.setHalignment(titreMatch,HPos.CENTER);
		etatEquipe1 = new Text();
		etatEquipe1.getStyleClass().add("etatEquipe");
		etatEquipe2 = new Text();
		etatEquipe2.getStyleClass().add("etatEquipe");
		GridPane.setHalignment(etatEquipe1, HPos.CENTER);
		GridPane.setHalignment(etatEquipe2, HPos.CENTER);
		recapMatch.add(etatEquipe1, 0, 1);
		recapMatch.add(etatEquipe2, 1, 1);
		scoreEquipe1 = new Text();
		scoreEquipe1.getStyleClass().add("scoreMatch");
		scoreEquipe2 = new Text();
		scoreEquipe2.getStyleClass().add("scoreMatch");
		GridPane.setHalignment(scoreEquipe1, HPos.CENTER);
		GridPane.setHalignment(scoreEquipe2, HPos.CENTER);
		recapMatch.add(scoreEquipe1, 0, 2);
		recapMatch.add(scoreEquipe2, 1, 2);
		Text nbrFautesText = new Text("nombre de fautes totales : ");
		nbrFautesText.getStyleClass().add("recapLight");
		nombreDeFautes = new Text();
		nombreDeFautes.getStyleClass().add("recapNbr");
		TextFlow nombreFauteFlow = new TextFlow(nbrFautesText, nombreDeFautes);
		recapMatch.add(nombreFauteFlow, 0, 3);
		GridPane.setColumnSpan(nombreFauteFlow, GridPane.REMAINING);
		Text nbrPassesText = new Text("nombre de passes décisives totales : ");
		nbrPassesText.getStyleClass().add("recapLight");
		nombreDePasses = new Text();
		nombreDePasses.getStyleClass().add("recapNbr");
		TextFlow nombrePassesFlow = new TextFlow(nbrPassesText, nombreDePasses);
		recapMatch.add(nombrePassesFlow, 0, 4);
		GridPane.setColumnSpan(nombrePassesFlow, GridPane.REMAINING);
		infoMatch.add(recapMatch, 0, 0);
		
		Text pasCharger = new Text("Le match n'est pas chargé");
		pasCharger.setWrappingWidth(400);
		pasCharger.getStyleClass().add("pasMatch");
		pasChargerMatch.add(pasCharger, 0, 1);
		
		actionsLV = new ListView<Action>();
		actionsLV.getStyleClass().addAll("customListView", "actionLV");
		infoMatch.add(actionsLV, 0, 1);
		
		showPasInfoMatch();
	}

	public void showInfoMatch() {
		panneauMilieuJournees.getChildren().remove(pasInfoMatch);
		panneauMilieuJournees.getChildren().remove(infoMatch);
		panneauMilieuJournees.getChildren().remove(pasChargerMatch);
		recapMatch.getChildren().remove(titreMatch);
		recapMatch.add(titreMatch, 0, 0);
		panneauMilieuJournees.add(infoMatch, 2, 0);
	}

	public void showPasInfoMatch() {
		panneauMilieuJournees.getChildren().remove(infoMatch);
		panneauMilieuJournees.getChildren().remove(pasInfoMatch);
		panneauMilieuJournees.getChildren().remove(pasChargerMatch);
		panneauMilieuJournees.add(pasInfoMatch, 2, 0);
	}
	
	public void showPasChargerMatch() {
		panneauMilieuJournees.getChildren().remove(infoMatch);
		panneauMilieuJournees.getChildren().remove(pasInfoMatch);
		panneauMilieuJournees.getChildren().remove(pasChargerMatch);
		pasChargerMatch.getChildren().remove(titreMatch);
		pasChargerMatch.add(titreMatch, 0, 0);
		panneauMilieuJournees.add(pasChargerMatch, 2, 0);
	}
	
	public void showStatistiquesContent() {
		panePrincipal.setCenter(panneauMilieuStatistiques);
	}
	
	private void initStatistiquesContent() {
		panneauMilieuStatistiques = new GridPane();
		panneauMilieuStatistiques.getStyleClass().add("panneauMilieu");
		
		barreJoueurs = new GridPane();
		barreJoueurs.setId("barreMilieu");
		GridPane.setMargin(barreJoueurs, new Insets(0, 20, 0, 0));
		barreJoueurs.setMinWidth(225);
		panneauMilieuStatistiques.add(barreJoueurs, 0, 0);
		
		rechercheStatistiquesBox = new HBox();
		rechercheStatistiquesBox.getStyleClass().add("recherche-box");
		rechercheStatistiquesTextField = new TextField("rechercher ...");
		rechercheStatistiquesTextField.getStyleClass().add("champ-recherche");
		HBox.setHgrow(rechercheStatistiquesTextField, Priority.ALWAYS);
		iconStatistiqueTextField = new Text("");
		iconStatistiqueTextField.getStyleClass().add("signe-recherche");
		HBox.setMargin(iconStatistiqueTextField, new Insets(5, 5, 0, 0));
		rechercheStatistiquesBox.getChildren().addAll(iconStatistiqueTextField, rechercheStatistiquesTextField);
		GridPane.setMargin(rechercheStatistiquesBox, new Insets(0, 0, 10, 0));
		barreJoueurs.add(rechercheStatistiquesBox, 0, 0);
		
		joueursStatistiquesListView = new ListView<Joueur>();
		joueursStatistiquesListView.setPrefWidth(225);
		joueursStatistiquesListView.setPrefHeight(550);
		GridPane.setMargin(joueursStatistiquesListView, new Insets(0, 0, 10, 0));
		Label placeholder2 = new Label("Aucun joueur");
		placeholder2.getStyleClass().add("placeholder2");
		joueursStatistiquesListView.setPlaceholder(placeholder2);
		barreJoueurs.add(joueursStatistiquesListView, 0, 1);
		
		statScroll = new ScrollPane();
		statScroll.setPrefWidth(590);
		statScroll.getStyleClass().add("statScrollPane");
		statistiqueGrid = new GridPane();
		statistiqueGrid.getStyleClass().add("statistiqueGrid");
		statistiqueGrid.setPrefWidth(585);
		pasStatistiqueGrid = new GridPane();
		pasStatistiqueGrid.getStyleClass().add("statistiqueGrid");
		pasStatistiqueGrid.setPrefWidth(585);
		
		Text pasStat = new Text("Pas de joueur sélectionné");
		pasStat.setWrappingWidth(550);
		pasStat.getStyleClass().add("pasMatch");
		pasStatistiqueGrid.add(pasStat, 0, 0);
		
		GridPane titleStat = new GridPane();
		GridPane.setMargin(titleStat, new Insets(0, 0, 10, 0));
		GridPane.setHgrow(titleStat, Priority.ALWAYS);
		titleStat.getStyleClass().add("titleStatGrid");
		Text statText = new Text("Statistiques de");
		statText.getStyleClass().add("titreStatLight");
		nomStat = new Text();
		nomStat.getStyleClass().add("statBig");
		titleStat.add(statText, 0, 0);
		titleStat.add(nomStat, 0, 1);
		statistiqueGrid.add(titleStat, 0, 0);
		
		Text vueEnsemble = new Text("vue d'ensemble");
		vueEnsemble.getStyleClass().add("titreStatLight");
		Text labelNombreButTotal = new Text("Nombre de but total : ");
		nombreButTotal = new Text();
		nombreButTotal.getStyleClass().add("nombreStatistique");
		Text labelNombrePassesDecisivesTatales = new Text("Nombre de passes décisives totales : ");
		nombrePassesTotales = new Text();
		nombrePassesTotales.getStyleClass().add("nombreStatistique");
		Text labelNombreFautesTotales = new Text("Nombre de fautes totales : ");
		nombreFautesTotales = new Text();
		nombreFautesTotales.getStyleClass().add("nombreStatistique");
		Text labelMoyenneButParMatch = new Text("Nombre moyen de but par match : ");
		nombreMoyenButsParMatch = new Text();
		nombreMoyenButsParMatch.getStyleClass().add("nombreStatistique");
		TextFlow butstotal = new TextFlow(labelNombreButTotal, nombreButTotal);
		butstotal.getStyleClass().add("texteStatistique");
		TextFlow passesTotales = new TextFlow(labelNombrePassesDecisivesTatales, nombrePassesTotales);
		passesTotales.getStyleClass().add("texteStatistique");
		TextFlow fautesTotales = new TextFlow(labelNombreFautesTotales, nombreFautesTotales);
		fautesTotales.getStyleClass().add("texteStatistique");
		TextFlow moyenneButs = new TextFlow(labelMoyenneButParMatch, nombreMoyenButsParMatch);
		moyenneButs.getStyleClass().add("texteStatistique");
		Text vueDetaillee = new Text("vue détaillée");
		vueDetaillee.getStyleClass().add("titreStatLight");
		GridPane.setMargin(vueDetaillee, new Insets(10, 0, 10, 0));
		final CategoryAxis axeX = new CategoryAxis();
		final NumberAxis axeY = new NumberAxis(0,15,2);
		axeX.setLabel("Date de match");
		detailLineChart = new LineChart<String, Number>(axeX, axeY);
		detailLineChart.setAnimated(false);
		statistiqueGrid.add(vueEnsemble, 0, 1);
		statistiqueGrid.add(butstotal, 0, 2);
		statistiqueGrid.add(passesTotales, 0, 3);
		statistiqueGrid.add(fautesTotales, 0, 4);
		statistiqueGrid.add(moyenneButs, 0, 5);
		statistiqueGrid.add(vueDetaillee, 0, 6);
		statistiqueGrid.add(detailLineChart, 0, 7);
		showPasStatistiquesGrid();
		panneauMilieuStatistiques.add(statScroll, 1, 0);
	}
	
	public void showStatistiquesGrid() {
		statScroll.setContent(statistiqueGrid);
	}
	
	public void showPasStatistiquesGrid() {
		statScroll.setContent(pasStatistiqueGrid);
	}

	private Button initButton(String signe, String texte, String cssStyle) {
		Text buttonSigne = new Text(signe+"  ");
		buttonSigne.getStyleClass().add(cssStyle);
		Text buttonTexte = new Text(texte);
		buttonTexte.getStyleClass().add("textGauche");
		TextFlow buttonTitle = new TextFlow(buttonSigne, buttonTexte);

		Button button = new Button();
		button.setGraphic(buttonTitle);
		button.getStyleClass().add("panneauGaucheButton");
		button.setPrefWidth(150);
		GridPane.setMargin(button, new Insets(4, 0, 4, 0));
		return button;
	}

	public BorderPane getPanePrincipal() {
		return panePrincipal;
	}

	public Button getAccueilButton() {
		return accueilButton;
	}

	public Button getClassementButton() {
		return classementButton;
	}

	public Button getMatchsButton() {
		return matchsButton;
	}

	public Button getJourneesButton() {
		return journeesButton;
	}

	public Button getStatistiquesButton() {
		return statistiquesButton;
	}

	public ListView<Equipe> getPodiumListView() {
		return podiumListView;
	}

	public ListView<Match> getProchainsMatchsListView() {
		return prochainsMatchsListView;
	}

	public TableView<Match> getRecapitulatifTableView() {
		return recapitulatifTableView;
	}

	public TableColumn<Match, Integer> getMatchColumn() {
		return matchColumn;
	}

	public TableColumn<Match, Journee> getDateColumn() {
		return dateColumn;
	}

	public TableColumn<Match, Boolean> getActionColumn() {
		return actionColumn;
	}

	public TableView<Equipe> getClassementTableView() {
		return classementTableView;
	}

	public TableColumn<Equipe, String> getEquipeClassementColumn() {
		return equipeClassementColumn;
	}

	public TableColumn<Equipe, String> getVilleClassementColumn() {
		return villeClassementColumn;
	}

	public TableColumn<Equipe, Integer> getPointsClassementColumn() {
		return pointsClassementColumn;
	}

	public TableView<Match> getMatchsTableView() {
		return matchsTableView;
	}

	public TableColumn<Match, Integer> getMatchForMatchsColumn() {
		return matchForMatchsColumn;
	}

	public TableColumn<Match, Journee> getDateForMatchsColumn() {
		return dateForMatchsColumn;
	}

	public TableColumn<Match, Boolean> getActionForMatchsColumn() {
		return actionForMatchsColumn;
	}

	public ListView<Journee> getJourneesListView() {
		return journeesListView;
	}

	public ListView<Match> getMatchListView() {
		return matchListView;
	}

	public ListView<Action> getActionsLV() {
		return actionsLV;
	}

	public LineChart<String, Number> getDetailLineChart() {
		return detailLineChart;
	}

	public ListView<Joueur> getJoueursStatistiquesListView() {
		return joueursStatistiquesListView;
	}

	public TextField getRechercheDateTextField() {
		return rechercheDateTextField;
	}

	public TextField getRechercheMatchTextField() {
		return rechercheMatchTextField;
	}

	public TextField getRechercheStatistiquesTextField() {
		return rechercheStatistiquesTextField;
	}

	public Text getTitreMatch() {
		return titreMatch;
	}

	public Text getEtatEquipe1() {
		return etatEquipe1;
	}

	public Text getEtatEquipe2() {
		return etatEquipe2;
	}

	public Text getScoreEquipe1() {
		return scoreEquipe1;
	}

	public Text getScoreEquipe2() {
		return scoreEquipe2;
	}

	public Text getNombreDeFautes() {
		return nombreDeFautes;
	}

	public Text getNombreDePasses() {
		return nombreDePasses;
	}

	public Text getNomStat() {
		return nomStat;
	}

	public Text getNombreButTotal() {
		return nombreButTotal;
	}

	public Text getNombrePassesTotales() {
		return nombrePassesTotales;
	}

	public Text getNombreMoyenButsParMatch() {
		return nombreMoyenButsParMatch;
	}

	public Text getNombreFautesTotales() {
		return nombreFautesTotales;
	}

	public Button getSupprimerChampionnat() {
		return supprimerChampionnat;
	}

}
