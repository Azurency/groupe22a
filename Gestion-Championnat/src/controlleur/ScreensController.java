package controlleur;

import java.sql.SQLException;
import java.util.HashMap;

import application.Main;
import modele.ConnectionBD;
import javafx.animation.FadeTransition;
import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.property.DoubleProperty;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.util.Duration;

/**
 * <b>Le controlleur d'écran</b>
 * <p>Ecran qui contient toutes les vues et qui permet de changer l'écran sans changer de fenêtre. Il y a une animation à chaque changement
 * de vue.</p> 
 * 
 * @author Antoine Lassier
 *
 */
public class ScreensController extends StackPane {
	/**
	 * La scène ie. le contenu qui sera passé à la fenêtre.
	 */
	Scene s;
	
	/**
	 * Les positions de la fenêtre.
	 */
	double x, y; // Position de la fenetre
	
	private Button closeButton;
	
	/**
	 * screens stocke tout les écrans chargé dans un HashMap, pour garder toujours les écrans au même état sans les réinitialiser.
	 */
	private HashMap<String, Node> screens = new HashMap<>();
	
	/**
	 * l'animation entre les écrans.
	 */
	private FadeTransition fadeout;
	
	/**
	 * La timeline qui gère l'animation et le changement d'écran.
	 */
	private Timeline fadeCloseButtonTL;

	
	/**
	 * Instancie un ScreenController.
	 * 
	 * @param width
	 * 		La largeur de la fenêtre.
	 * @param height
	 * 		La hauteur de la fenêtre.
	 */
	public ScreensController(int width, int height) {
		super();
		this.setOnMousePressed(new WindowEventHandler());
		this.setOnMouseDragged(new WindowEventHandler());

		// Close button
		closeButton = new Button("");
		closeButton.setId("closeButton");
		ScreensController.setAlignment(closeButton, Pos.TOP_RIGHT);
		ScreensController.setMargin(closeButton, new Insets(35, 40, 0, 0));
		closeButton.setOnMouseClicked(new CloseButtonEventHandler());
		closeButton.setOnMouseMoved(new CloseButtonEventHandler());

		this.setOnMouseMoved(new WindowEventHandler());

		s = new Scene(this, width, height, Color.TRANSPARENT);
	}

	/**
	 * Ajoute un écran dans le HashMap des écrans géré par le ScreensController
	 * 		
	 */
	public void addScreen(String name, Node screen) {
		screens.put(name, screen);
	}

	/**
	 * Charge un écran et l'ajoute dans le HashMap des écrans.
	 * <p>
	 * Cette méthode set le ScreensController en tant que parent de l'écran passé en paramètre et l'ajoute dans la liste des écrans.
	 * </p>
	 * @param name
	 * 		Le nom de l'écran
	 * @param controlleur
	 * 		Le contrôleur de l'écran
	 * @param screen
	 * 		L'écran (ex. un StackPane, un BorderPane)
	 */
	public void loadScreen(String name, ControlledScreen controlleur, Node screen) {
		controlleur.setScreenParent(this);
		addScreen(name, screen);
	}

	/**
	 * Change l'écran actuel pour l'écran passé en paramètre.
	 * @param name
	 * 		Le nom de l'écran a afficher
	 * @return
	 * 		true si l'écran à pu être affiché, false sinon.
	 */
	public boolean setScreen(final String name) {       
		if (screens.get(name) != null) {   //screen loaded
			if (!getChildren().isEmpty()) {    // On regarde si il y a un écran
				final DoubleProperty oldScreenOpacity = getChildren().get(0).opacityProperty();
				final DoubleProperty newScreenOpacity = screens.get(name).opacityProperty();
				Timeline fade = new Timeline(
						new KeyFrame(Duration.ZERO, new KeyValue(oldScreenOpacity, 1.0)),
						new KeyFrame(new Duration(400), new EventHandler<ActionEvent>() {
							@Override
							public void handle(ActionEvent t) {
								getChildren().remove(0); //retire l'ancien écran
								getChildren().add(0, screens.get(name)); //ajoute le nouveau
								ActionEvent.fireEvent(screens.get(name), new ActionEvent()); // on créer un event au screen
								screens.get(name).setOpacity(0);
								Timeline fadeIn = new Timeline(
										new KeyFrame(Duration.ZERO, new KeyValue(newScreenOpacity, 0.0)),
										new KeyFrame(new Duration(400), new KeyValue(newScreenOpacity, 1.0)));
								fadeIn.play();
							}
						}, new KeyValue(oldScreenOpacity, 0.0)));
				fade.play();
			} else { //sinon on affiche le premier
				setOpacity(0.0);
				getChildren().add(screens.get(name));
				getChildren().add(closeButton);
				Timeline fadeIn = new Timeline(
						new KeyFrame(Duration.ZERO, new KeyValue(this.opacityProperty(), 0.0)),
						new KeyFrame(new Duration(1500), new KeyValue(this.opacityProperty(), 1.0)));
				fadeIn.play();
				ActionEvent.fireEvent(screens.get(name), new ActionEvent()); // on créer un event au screen
			}
			return true;
		} else {
			System.out.println("screen hasn't been loaded!!! \n");
			return false;
		}
	}

	public Scene getS() {
		return s;
	}

	// EVENT
	class WindowEventHandler implements EventHandler<MouseEvent> {
		@Override
		public void handle(MouseEvent event) {
			if (event.getEventType().equals(MouseEvent.MOUSE_PRESSED)) {
				x = s.getWindow().getX() - event.getScreenX();
				y = s.getWindow().getY() - event.getScreenY();
			} 
			else if (event.getEventType().equals(MouseEvent.MOUSE_DRAGGED)) {
				s.getWindow().setX(event.getScreenX() + x);
				s.getWindow().setY(event.getScreenY() + y);
			}
			else if (event.getEventType().equals(MouseEvent.MOUSE_MOVED)) {

				if (fadeCloseButtonTL != null) {
					fadeout.stop();
					fadeCloseButtonTL.stop();
					closeButton.setOpacity(1);
				}

				fadeout = new FadeTransition(new Duration(300), closeButton);
				fadeout.setToValue(0.0);

				fadeCloseButtonTL = new Timeline(
										new KeyFrame(new Duration(1000), new EventHandler<ActionEvent>() {
											@Override
											public void handle(ActionEvent event) {
												fadeout.play();
											}
										})
									);
				fadeCloseButtonTL.play();

			}
		}
	}

	class CloseButtonEventHandler implements EventHandler<MouseEvent> {

		@Override
		public void handle(MouseEvent event) {
			if (event.getEventType().equals(MouseEvent.MOUSE_CLICKED)) {
				try {
					ConnectionBD.getStatement().seDeco();
				} catch (SQLException e) {
					e.printStackTrace();
				}
				Main.getStage().setScene(null);
				Platform.setImplicitExit(true);
				Platform.exit();
			}
			else if (event.getEventType().equals(MouseEvent.MOUSE_MOVED)) {
				if (fadeCloseButtonTL != null) {
					fadeout.stop();
					fadeCloseButtonTL.stop();
					closeButton.setOpacity(1);
				}
			}
		}

	}
}
