package controlleur.fenetrePrincipale;

import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Collections;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.util.Callback;
import modele.Action;
import modele.Championnat;
import modele.Equipe;
import modele.Joueur;
import modele.Journee;
import modele.Match;
import vue.fenetrePrincipale.FenetrePrincipaleScreenView;
import controlleur.ControlledScreen;
import controlleur.ScreensController;
import controlleur.customListCell.ListActionCell;
import controlleur.customListCell.ListJoueurStatistiquesCell;
import controlleur.customListCell.ListJourneesCell;
import controlleur.customListCell.ListMatchCell;
import controlleur.customListCell.ListPodiumCell;
import controlleur.customListCell.ListProchainsMatchsCell;
import controlleur.customTableCell.TableActionCell;
import controlleur.customTableCell.TableDateCell;
import controlleur.customTableCell.TableEquipeClassementCell;
import controlleur.customTableCell.TableMatchCell;
import controlleur.customTableCell.TablePointsClassementCell;

public class FenetrePrincipaleScreenController implements ControlledScreen {
	private FenetrePrincipaleScreenView vue;
	private Championnat c;
	private ScreensController screensManager;
	private ObservableList<Journee> journees;
	private ObservableList<Journee> nouvelleListeJournees;
	private ObservableList<Match> matchs;
	private ObservableList<Match> nouvelleListeMatchs;
	private ObservableList<Joueur> joueurs;
	private ObservableList<Joueur> nouvelleListeJoueurs;
	private ObservableList<Equipe> podiumEquipes;
	private ObservableList<Match> prochainsMatchs;
	private ObservableList<Equipe> classementEquipes;
	private ObservableList<Match> recapitulatifMatch;

	@SuppressWarnings("unchecked")
	public FenetrePrincipaleScreenController() {
		vue = new FenetrePrincipaleScreenView();
		this.c = Championnat.chargerChampionnat();
		
		podiumEquipes = FXCollections.observableArrayList(c.getPodiumEquipe());
		prochainsMatchs = FXCollections.observableArrayList(c.getProchainsMatchs());
		classementEquipes = FXCollections.observableArrayList(c.getClassement());
		joueurs = FXCollections.observableArrayList(c.getAllJoueurs());
		recapitulatifMatch = FXCollections.observableArrayList(c.getRecapitulatifMatchs());
		matchs = FXCollections.observableArrayList(c.getAllMatchs());

		journees = FXCollections.observableArrayList(Arrays.asList(c.getJournees()));

		vue.getAccueilButton().setOnMouseReleased(new AccueilButtonMouseEventHandler());
		vue.getClassementButton().setOnMouseReleased(new ClassementButtonMouseEventHandler());
		vue.getJourneesButton().setOnMouseReleased(new JourneesButtonMouseEventHandler());
		vue.getMatchsButton().setOnMouseReleased(new MatchsButtonMouseEventHandler());
		vue.getStatistiquesButton().setOnMouseReleased(new StatistiquesButtonMouseEventHandler());
		
		/*for (int i = 0; i < 25; i++) {
			Match toSave = matchs.get(i);
			toSave.genererFeuilleMatch(new java.io.File(toSave.getTextEquipes()+".yaml"));
		}*/

		vue.getPodiumListView().setItems(podiumEquipes);
		vue.getPodiumListView().setCellFactory(new Callback<ListView<Equipe>, ListCell<Equipe>>() {

			@Override
			public ListCell<Equipe> call(ListView<Equipe> param) {
				return new ListPodiumCell();
			}
		});

		vue.getProchainsMatchsListView().setItems(prochainsMatchs);
		vue.getProchainsMatchsListView().setCellFactory(new Callback<ListView<Match>, ListCell<Match>>() {

			@Override
			public ListCell<Match> call(ListView<Match> param) {
				return new ListProchainsMatchsCell();
			}
		});

		vue.getRecapitulatifTableView().setItems(recapitulatifMatch);

		vue.getMatchColumn().setCellValueFactory(new PropertyValueFactory<Match, Integer>("id"));
		vue.getMatchColumn().setCellFactory(new Callback<TableColumn<Match,Integer>, TableCell<Match,Integer>>() {

			@Override
			public TableCell<Match, Integer> call(TableColumn<Match, Integer> param) {
				return new TableMatchCell();
			}
		});

		vue.getDateColumn().setCellValueFactory(new PropertyValueFactory<Match, Journee>("jour"));
		vue.getDateColumn().setCellFactory(new Callback<TableColumn<Match,Journee>, TableCell<Match,Journee>>() {

			@Override
			public TableCell<Match, Journee> call(TableColumn<Match, Journee> param) {
				return new TableDateCell();
			}
		});

		vue.getActionColumn().setCellValueFactory(new PropertyValueFactory<Match, Boolean>("charge"));
		vue.getActionColumn().setCellFactory(new Callback<TableColumn<Match,Boolean>, TableCell<Match,Boolean>>() {

			@Override
			public TableCell<Match, Boolean> call(TableColumn<Match, Boolean> param) {
				return new TableActionCell(FenetrePrincipaleScreenController.this);
			}
		});

		vue.getRecapitulatifTableView().getColumns().addListener(new ListChangeListener() {
			@Override
			public void onChanged(Change change) {
				change.next();
				if(change.wasReplaced()) {
					vue.getRecapitulatifTableView().getColumns().clear();
					vue.getRecapitulatifTableView().getColumns().addAll(vue.getMatchColumn(),vue.getDateColumn(),vue.getActionColumn());
				}
			}
		});

		vue.getClassementTableView().setItems(classementEquipes);

		vue.getEquipeClassementColumn().setCellValueFactory(new PropertyValueFactory<Equipe, String>("nom"));
		vue.getEquipeClassementColumn().setCellFactory(new Callback<TableColumn<Equipe,String>, TableCell<Equipe,String>>() {

			@Override
			public TableCell<Equipe, String> call(TableColumn<Equipe, String> param) {
				return new TableEquipeClassementCell();
			}
		});

		vue.getVilleClassementColumn().setCellValueFactory(new PropertyValueFactory<Equipe, String>("ville"));

		vue.getPointsClassementColumn().setCellValueFactory(new PropertyValueFactory<Equipe, Integer>("points"));
		vue.getPointsClassementColumn().setCellFactory(new Callback<TableColumn<Equipe,Integer>, TableCell<Equipe,Integer>>() {

			@Override
			public TableCell<Equipe, Integer> call(TableColumn<Equipe, Integer> param) {
				return new TablePointsClassementCell();
			}
		});

		vue.getClassementTableView().getColumns().addListener(new ListChangeListener() {
			@Override
			public void onChanged(Change change) {
				change.next();
				if(change.wasReplaced()) {
					vue.getClassementTableView().getColumns().clear();
					vue.getClassementTableView().getColumns().addAll(vue.getEquipeClassementColumn(),vue.getVilleClassementColumn(),vue.getPointsClassementColumn());
				}
			}
		});

		vue.getMatchsTableView().setItems(matchs);

		vue.getMatchForMatchsColumn().setCellValueFactory(new PropertyValueFactory<Match, Integer>("id"));
		vue.getMatchForMatchsColumn().setCellFactory(new Callback<TableColumn<Match,Integer>, TableCell<Match,Integer>>() {

			@Override
			public TableCell<Match, Integer> call(TableColumn<Match, Integer> param) {
				return new TableMatchCell();
			}
		});

		vue.getDateForMatchsColumn().setCellValueFactory(new PropertyValueFactory<Match, Journee>("jour"));
		vue.getDateForMatchsColumn().setCellFactory(new Callback<TableColumn<Match,Journee>, TableCell<Match,Journee>>() {

			@Override
			public TableCell<Match, Journee> call(TableColumn<Match, Journee> param) {
				return new TableDateCell();
			}
		});

		vue.getActionForMatchsColumn().setCellValueFactory(new PropertyValueFactory<Match, Boolean>("charge"));
		vue.getActionForMatchsColumn().setCellFactory(new Callback<TableColumn<Match,Boolean>, TableCell<Match,Boolean>>() {

			@Override
			public TableCell<Match, Boolean> call(TableColumn<Match, Boolean> param) {
				return new TableActionCell(FenetrePrincipaleScreenController.this);
			}
		});

		vue.getMatchsTableView().getColumns().addListener(new ListChangeListener() {
			@Override
			public void onChanged(Change change) {
				change.next();
				if(change.wasReplaced()) {
					vue.getMatchsTableView().getColumns().clear();
					vue.getMatchsTableView().getColumns().addAll(vue.getMatchForMatchsColumn(),vue.getDateForMatchsColumn(),vue.getActionForMatchsColumn());
				}
			}
		});

		vue.getJourneesListView().getSelectionModel().selectedItemProperty().addListener(new JourneesListViewChangeListener());
		vue.getJourneesListView().setItems(journees);
		vue.getJourneesListView().setCellFactory(new Callback<ListView<Journee>, ListCell<Journee>>() {

			@Override
			public ListCell<Journee> call(ListView<Journee> param) {
				return new ListJourneesCell();
			}
		});

		vue.getMatchListView().getSelectionModel().selectedItemProperty().addListener(new MatchListViewChangeListener());
		vue.getMatchListView().setCellFactory(new Callback<ListView<Match>, ListCell<Match>>() {

			@Override
			public ListCell<Match> call(ListView<Match> param) {
				return new ListMatchCell();
			}
		});

		vue.getActionsLV().setCellFactory(new Callback<ListView<Action>, ListCell<Action>>() {

			@Override
			public ListCell<Action> call(ListView<Action> param) {
				return new ListActionCell();
			}
		});

		vue.getJoueursStatistiquesListView().getSelectionModel().selectedItemProperty().addListener(new JoueursStatistiquesListViewChangeListener());
		vue.getJoueursStatistiquesListView().setItems(joueurs);
		vue.getJoueursStatistiquesListView().setCellFactory(new Callback<ListView<Joueur>, ListCell<Joueur>>() {

			@Override
			public ListCell<Joueur> call(ListView<Joueur> param) {
				return new ListJoueurStatistiquesCell();
			}
		});

		// RECHERCHES 
		vue.getRechercheDateTextField().setOnMousePressed(new RechercheDateTextFieldMouseEventHandler());
		vue.getRechercheDateTextField().focusedProperty().addListener(new RechercheDateFocusChangeListener());
		vue.getRechercheDateTextField().textProperty().addListener(new RechercheDateChangeListener());
		vue.getRechercheMatchTextField().setOnMousePressed(new RechercheMatchTextFieldMouseEventHandler());
		vue.getRechercheMatchTextField().focusedProperty().addListener(new RechercheMatchFocusChangeListener());
		vue.getRechercheMatchTextField().textProperty().addListener(new RechercheMatchChangeListener());
		vue.getRechercheStatistiquesTextField().setOnMousePressed(new RechercheStatistiquesTextFieldMouseEventHandler());
		vue.getRechercheStatistiquesTextField().focusedProperty().addListener(new RechercheStatistiquesFocusChangeListener());
		vue.getRechercheStatistiquesTextField().textProperty().addListener(new RechercheStatistiquesChangeListener());
		
		vue.getSupprimerChampionnat().setOnMouseClicked(new SupprimerChampionnatMouseEventHandler());
		
		// SI LE CHAMPIONNAT EST FINI AU LANCEMENT
		if (c.isFinish()) {
			vue.showChampionnatFinieContent();
		}
	}

	public FenetrePrincipaleScreenView getVue() {
		return vue;
	}

	@Override
	public void setScreenParent(ScreensController screenPage) {
		screensManager = screenPage;
	}
	
	
	

	// --------------------------------------------------
	// Méthodes diverses
	// --------------------------------------------------

	public void setSelectedButton(Button b) {
		vue.getAccueilButton().getStyleClass().remove("panneauGaucheButtonpressed");
		vue.getClassementButton().getStyleClass().remove("panneauGaucheButtonpressed");
		vue.getJourneesButton().getStyleClass().remove("panneauGaucheButtonpressed");
		vue.getMatchsButton().getStyleClass().remove("panneauGaucheButtonpressed");
		vue.getStatistiquesButton().getStyleClass().remove("panneauGaucheButtonpressed");
		b.getStyleClass().add("panneauGaucheButtonpressed");

	}
	
	public void refreshPodium() {
		vue.getPodiumListView().setItems(null);
		podiumEquipes = FXCollections.observableArrayList(c.getPodiumEquipe());
		vue.getPodiumListView().setItems(podiumEquipes);
	}
	
	public void refreshClassement() {
		Collections.sort(classementEquipes, new Equipe.ComparateurParPoints());
	}
	
	public void refreshMatchs() {
		vue.getMatchsTableView().getColumns().clear();
		vue.getMatchsTableView().getColumns().addAll(vue.getMatchForMatchsColumn(),vue.getDateForMatchsColumn(),vue.getActionForMatchsColumn());
	}
	
	public void refreshRecapitulatif() {
		vue.getRecapitulatifTableView().setItems(null);
		recapitulatifMatch = FXCollections.observableArrayList(c.getRecapitulatifMatchs());
		vue.getRecapitulatifTableView().setItems(recapitulatifMatch);
		vue.getRecapitulatifTableView().getColumns().clear();
		vue.getRecapitulatifTableView().getColumns().addAll(vue.getMatchColumn(),vue.getDateColumn(),vue.getActionColumn());
	}
	
	public void refreshProchainMatch() {
		vue.getProchainsMatchsListView().setItems(null);
		prochainsMatchs = FXCollections.observableArrayList(c.getProchainsMatchs());
		vue.getProchainsMatchsListView().setItems(prochainsMatchs);
	}
	
	public void afficherLeCompteRenduDuMatch(Match item) {
		setSelectedButton(vue.getJourneesButton());
		vue.showJourneesContent();
		vue.getJourneesListView().getSelectionModel().clearSelection();
		vue.getJourneesListView().getSelectionModel().select(item.getJour());
		vue.getMatchListView().getSelectionModel().clearSelection();
		vue.getMatchListView().getSelectionModel().select(item);
	}


	
	
	// --------------------------------------------------
	// evenements
	// --------------------------------------------------


	class AccueilButtonMouseEventHandler implements EventHandler<MouseEvent> {

		@Override
		public void handle(MouseEvent event) {
			setSelectedButton(vue.getAccueilButton());
			vue.showAccueilContent();
			if (c.isFinish()) {
				vue.showChampionnatFinieContent();
			}
		}

	}
	
	class SupprimerChampionnatMouseEventHandler implements EventHandler<MouseEvent> {

		@Override
		public void handle(MouseEvent event) {
			c.finaliseLeChampionnat();
			vue.getSupprimerChampionnat().setDisable(true);
		}
		
	}

	class ClassementButtonMouseEventHandler implements EventHandler<MouseEvent> {

		@Override
		public void handle(MouseEvent event) {
			setSelectedButton(vue.getClassementButton());
			vue.showClassementContent();
		}

	}

	class JourneesButtonMouseEventHandler implements EventHandler<MouseEvent> {

		@Override
		public void handle(MouseEvent event) {
			setSelectedButton(vue.getJourneesButton());
			vue.showJourneesContent();
		}

	}

	class MatchsButtonMouseEventHandler implements EventHandler<MouseEvent> {

		@Override
		public void handle(MouseEvent event) {
			setSelectedButton(vue.getMatchsButton());
			vue.showMatchsContent();
		}

	}

	class StatistiquesButtonMouseEventHandler implements EventHandler<MouseEvent> {

		@Override
		public void handle(MouseEvent event) {
			setSelectedButton(vue.getStatistiquesButton());
			vue.showStatistiquesContent();
		}

	}

	class RechercheDateTextFieldMouseEventHandler implements EventHandler<MouseEvent> {

		@Override
		public void handle(MouseEvent event) {
			if (vue.getRechercheDateTextField().getText().equals("rechercher ...")) {
				vue.getRechercheDateTextField().clear();
			}
		}

	}

	class RechercheMatchTextFieldMouseEventHandler implements EventHandler<MouseEvent> {

		@Override
		public void handle(MouseEvent event) {
			if (vue.getRechercheMatchTextField().getText().equals("rechercher ...")) {
				vue.getRechercheMatchTextField().clear();
			}
		}

	}

	class RechercheStatistiquesTextFieldMouseEventHandler implements EventHandler<MouseEvent> {

		@Override
		public void handle(MouseEvent event) {
			if (vue.getRechercheStatistiquesTextField().getText().equals("rechercher ...")) {
				vue.getRechercheStatistiquesTextField().clear();
			}
		}

	}

	class RechercheDateFocusChangeListener implements ChangeListener<Boolean> {

		@Override
		public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
			if (!newValue) {
				if (vue.getRechercheDateTextField().getText().equals("")) {
					vue.getRechercheDateTextField().setText("rechercher ...");
				}
			}
		}

	}

	class RechercheMatchFocusChangeListener implements ChangeListener<Boolean> {

		@Override
		public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
			if (!newValue) {
				if (vue.getRechercheMatchTextField().getText().equals("")) {
					vue.getRechercheMatchTextField().setText("rechercher ...");
				}
			}
		}

	}

	class RechercheStatistiquesFocusChangeListener implements ChangeListener<Boolean> {

		@Override
		public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
			if (!newValue) {
				if (vue.getRechercheStatistiquesTextField().getText().equals("")) {
					vue.getRechercheStatistiquesTextField().setText("rechercher ...");
				}
			}
		}

	}

	class RechercheDateChangeListener implements ChangeListener<String> {

		@Override
		public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
			Journee journeeSelectionee = vue.getJourneesListView().getSelectionModel().getSelectedItem();

			if (oldValue != null && (newValue.length() < oldValue.length()) && !oldValue.equals("rechercher ...")) {
				vue.getJourneesListView().setItems(journees);
			}

			String[] parties = newValue.toLowerCase().split(" ");

			nouvelleListeJournees = FXCollections.observableArrayList();
			for (Journee j : vue.getJourneesListView().getItems()) {
				boolean match = true;
				String rechercheSur = j.getDate().format(DateTimeFormatter.ofPattern("dd/MM/YYYY"));
				for (String partie : parties) {
					if (! rechercheSur.toLowerCase().contains(partie)) {
						match = false;
						break;
					}
				}
				if (match) {
					nouvelleListeJournees.add(j);
				}
			}
			if (!newValue.equals("rechercher ...")) {
				vue.getJourneesListView().setItems(nouvelleListeJournees);
				if (nouvelleListeJournees.contains(journeeSelectionee)) {
					int index = vue.getJourneesListView().getItems().indexOf(journeeSelectionee);
					vue.getJourneesListView().getSelectionModel().select(index);
				}
				else {
					vue.showPasInfoMatch();
					vue.getJourneesListView().getSelectionModel().clearSelection();
					vue.getMatchListView().getSelectionModel().clearSelection();
				}
			}
		}

	}
	
	class RechercheMatchChangeListener implements ChangeListener<String> {

		@Override
		public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
			Match matchSelectionnee = vue.getMatchListView().getSelectionModel().getSelectedItem();
			
			if (oldValue != null && (newValue.length() < oldValue.length()) && !oldValue.equals("rechercher ...")) {
				vue.getMatchListView().setItems(matchs);
			}

			String[] parties = newValue.toLowerCase().split(" ");

			nouvelleListeMatchs = FXCollections.observableArrayList();
			for (Match m : vue.getMatchListView().getItems()) {
				boolean match = true;
				String rechercheSur = m.getEquipe1().getNom() + m.getEquipe1().getVille() + m.getEquipe2().getNom() + m.getEquipe2().getVille();
				for (String partie : parties) {
					if (! rechercheSur.toLowerCase().contains(partie)) {
						match = false;
						break;
					}
				}
				if (match) {
					nouvelleListeMatchs.add(m);
				}
			}
			if (!newValue.equals("rechercher ...")) {
				vue.getMatchListView().setItems(nouvelleListeMatchs);
				if (nouvelleListeMatchs.contains(matchSelectionnee)) {
					int index = vue.getMatchListView().getItems().indexOf(matchSelectionnee);
					vue.getMatchListView().getSelectionModel().select(index);
				}
				else {
					vue.showPasInfoMatch();
					vue.getMatchListView().getSelectionModel().clearSelection();
				}
			}
		}
		
	}
	
	class RechercheStatistiquesChangeListener implements ChangeListener<String> {

		@Override
		public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
			Joueur joueurSelectionne = vue.getJoueursStatistiquesListView().getSelectionModel().getSelectedItem();

			if (oldValue != null && (newValue.length() < oldValue.length()) && !oldValue.equals("rechercher ...")) {
				vue.getJoueursStatistiquesListView().setItems(joueurs);
			}

			String[] parties = newValue.toLowerCase().split(" ");

			nouvelleListeJoueurs = FXCollections.observableArrayList();
			for (Joueur j : vue.getJoueursStatistiquesListView().getItems()) {
				boolean match = true;
				String rechercheSur = j.getPrenom() + j.getNom() + j.getRole() + j.getEquipe().getNom() + j.getEquipe().getVille();
				for (String partie : parties) {
					if (! rechercheSur.toLowerCase().contains(partie)) {
						match = false;
						break;
					}
				}
				if (match) {
					nouvelleListeJoueurs.add(j);
				}
			}
			if (!newValue.equals("rechercher ...")) {
				vue.getJoueursStatistiquesListView().setItems(nouvelleListeJoueurs);
				if (nouvelleListeJoueurs.contains(joueurSelectionne)) {
					int index = vue.getJoueursStatistiquesListView().getItems().indexOf(joueurSelectionne);
					vue.getJoueursStatistiquesListView().getSelectionModel().select(index);
				}
				else {
					vue.showPasStatistiquesGrid();
					vue.getJoueursStatistiquesListView().getSelectionModel().clearSelection();
				}
			}
		}
		
	}
	
	class JourneesListViewChangeListener implements ChangeListener<Journee> {

		@Override
		public void changed(ObservableValue<? extends Journee> observable, Journee oldValue, Journee newValue) {
			if (newValue != null) {
				vue.getMatchListView().getSelectionModel().clearSelection();
				vue.getMatchListView().setItems(FXCollections.observableArrayList(newValue.getMatchs()));
			}
			if (newValue == null) {
				vue.getMatchListView().getSelectionModel().clearSelection();
				vue.getMatchListView().setItems(null);
			}
		}
		
	}
	
	class MatchListViewChangeListener implements ChangeListener<Match> {

		@Override
		public void changed(ObservableValue<? extends Match> observable, Match oldValue, Match newValue) {
			if (newValue != null) {
				vue.getTitreMatch().setText(newValue.getEquipe1().getNom() + " - " + newValue.getEquipe2().getNom());
				if (newValue.estCharge()) {
					if (newValue.getScoreEq1() == newValue.getScoreEq2()) {
						vue.getEtatEquipe1().setText("ex-aequo");
						vue.getEtatEquipe2().setText("ex-aequo");
					} else if (newValue.getScoreEq1() > newValue.getScoreEq2()) {
						vue.getEtatEquipe1().setText("gagnant");
						vue.getEtatEquipe2().setText("perdant");
					} else {
						vue.getEtatEquipe1().setText("perdant");
						vue.getEtatEquipe2().setText("gagnant");
					}
					vue.getScoreEquipe1().setText(newValue.getScoreEq1()+"");
					vue.getScoreEquipe2().setText(newValue.getScoreEq2()+"");
					int sum[] = newValue.getSommesFautePasse();
					vue.getNombreDeFautes().setText(sum[0]+" fautes");
					vue.getNombreDePasses().setText(sum[1]+" passes");
					vue.getActionsLV().setItems(FXCollections.observableArrayList(newValue.getActions()));
					vue.showInfoMatch();
				} else {
					vue.showPasChargerMatch();
				}
			}
			if (newValue == null) {
				vue.showPasInfoMatch();
			}
		}
		
	}
	
	class JoueursStatistiquesListViewChangeListener implements ChangeListener<Joueur> {

		@Override
		public void changed(ObservableValue<? extends Joueur> observable, Joueur oldValue, Joueur newValue) {
			if (oldValue == null) {
				vue.showStatistiquesGrid();
			}
			if (newValue != null) {
				vue.getNomStat().setText(newValue.getPrenom() + " " + newValue.getNom());
				vue.getNombreButTotal().setText(newValue.getNbrButsTotal()+" buts");
				vue.getNombrePassesTotales().setText(newValue.getNbrPassesDecisivesTotales()+ " passes");
				vue.getNombreFautesTotales().setText(newValue.getNbrFautesTotales() + " fautes");
				vue.getNombreMoyenButsParMatch().setText(newValue.getMoyenneButsParMatch()+ " buts");
				vue.getDetailLineChart().getData().clear();
				vue.getDetailLineChart().getData().addAll(newValue.getButParMatch(), newValue.getPasseParMatch(), newValue.getFauteParMatch());
			}
			if (newValue == null) {
				vue.showPasStatistiquesGrid();
			}
		}
		
	}

}
