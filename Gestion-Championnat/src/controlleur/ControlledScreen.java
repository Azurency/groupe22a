package controlleur;

/**
 * Permet de définir qu'un écran (une vue) est controllée par un ScreenController
 * 
 * @author Antoine
 * @see ScreensController
 */
public interface ControlledScreen {
	
	/**
	 * Définie le ScreensController qui contrôle l'écran.
	 * 
	 * @param screenPage
	 * 		  Le ScreenScroller
	 * @see ScreensController
	 */
	public void setScreenParent(ScreensController screenPage);
}
