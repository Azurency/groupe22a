package controlleur.customListCell;

import java.time.LocalDate;
import java.time.Period;

import modele.Joueur;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DataFormat;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.paint.Color;

public class ListJoueurCell extends ListCell<Joueur> {
	GridPane cellLayout;
	Label roleJoueur;
	Label iconeJoueur;
	Label nomJoueur;
	Label ageJoueur;
	LocalDate now;
	ListView<Joueur> joueursLV;
	public static final DataFormat dataFormat =  new DataFormat("joueur");
	
	public ListJoueurCell(ListView<Joueur> lv) {
		super();
		now = LocalDate.now();
		joueursLV = lv;
		
		initializeComponents();
		initializeEvents();
	}
	
	public void initializeComponents() {
		cellLayout = new GridPane();
		cellLayout.getStyleClass().add("cellLayout");

		roleJoueur = new Label();
		roleJoueur.getStyleClass().add("roleJoueur");
		GridPane.setHgrow(roleJoueur, Priority.ALWAYS);
		iconeJoueur = new Label();
		iconeJoueur.getStyleClass().add("iconeJoueur");
		GridPane.setHalignment(iconeJoueur, HPos.CENTER);

		nomJoueur = new Label();
		nomJoueur.setMaxWidth(135);
		nomJoueur.getStyleClass().add("nomJoueur");
		GridPane.setMargin(nomJoueur, new Insets(3, 0, 0, 0));
		GridPane.setHgrow(nomJoueur, Priority.ALWAYS);
		ageJoueur = new Label();
		ageJoueur.getStyleClass().add("ageJoueur");
		GridPane.setMargin(ageJoueur, new Insets(3, 0, 0, 0));
		
		cellLayout.add(roleJoueur, 0, 0);
		cellLayout.add(iconeJoueur, 1, 0);
		cellLayout.add(nomJoueur, 0, 1);
		cellLayout.add(ageJoueur, 1, 1);
	}
	
	public void initializeEvents() {
		setOnDragDetected(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				Dragboard dragboard = joueursLV.startDragAndDrop(TransferMode.MOVE);
				ClipboardContent content = new ClipboardContent();
				content.put(dataFormat, getItem());
				content.putString(getItem().getId()+"");
				dragboard.setContent(content);
				SnapshotParameters test = new SnapshotParameters();
				test.setFill(Color.TRANSPARENT);
				dragboard.setDragView(snapshot(test, null));
				event.consume();
			}
		});
	}
	
	@Override
	protected void updateItem(Joueur item, boolean empty) {
		super.updateItem(item, empty);
		setText(null);
		if (empty) {
			setGraphic(null);
		} else {
			switch (item.getRole()) {
			case "joueur":
				roleJoueur.setText("Joueur");
				iconeJoueur.setText("");
				break;
			case "entraineur":
				roleJoueur.setText("Entraineur");
				iconeJoueur.setText("");
				break;
			case "arbitre":
				roleJoueur.setText("Arbitre");
				iconeJoueur.setText("");
				break;
			}
			nomJoueur.setText(item.getPrenom()+" "+item.getNom());
			Period age = Period.between(item.getDateNais(), now);
			ageJoueur.setText(age.getYears()+" ans");
			setGraphic(cellLayout);
		}
	}
	
}
