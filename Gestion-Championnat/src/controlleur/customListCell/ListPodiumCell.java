package controlleur.customListCell;

import modele.Equipe;
import javafx.scene.control.ListCell;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;

public class ListPodiumCell extends ListCell<Equipe> {
	private GridPane cellLayout;
	private Text numeroClassement;
	private Text nomEquipe;
	private TextFlow texteEquipe;
	private Text points;

	public ListPodiumCell() {
		super();
		initComponents();
	}

	private void initComponents() {
		cellLayout = new GridPane();
		numeroClassement = new Text();
		numeroClassement.getStyleClass().add("podiumNumber");
		nomEquipe = new Text();
		texteEquipe = new TextFlow(numeroClassement, nomEquipe);
		GridPane.setHgrow(texteEquipe, Priority.ALWAYS);
		points = new Text();
		points.getStyleClass().add("podiumPoints");

		cellLayout.add(texteEquipe, 0, 0);
		cellLayout.add(points, 1, 0);
	}

	@Override
	protected void updateItem(Equipe item, boolean empty) {
		super.updateItem(item, empty);
		setText(null);
		if (empty) {
			setGraphic(null);
		} else {
			numeroClassement.setText((getListView().getItems().indexOf(item)+1) + "");
			nomEquipe.setText(" - " + item.getNom());
			points.setText(item.getPoints() + " points");
			setGraphic(cellLayout);
		}
	} 

}
