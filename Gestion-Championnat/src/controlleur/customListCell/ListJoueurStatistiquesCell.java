package controlleur.customListCell;

import java.time.LocalDate;
import java.time.Period;

import modele.Joueur;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;

public class ListJoueurStatistiquesCell extends ListCell<Joueur> {
	GridPane cellLayout;
	Label roleJoueur;
	Label iconeJoueur;
	Label nomJoueur;
	Label ageJoueur;
	LocalDate now;
	
	public ListJoueurStatistiquesCell() {
		super();
		now = LocalDate.now();
		
		cellLayout = new GridPane();
		cellLayout.getStyleClass().add("cellLayout");

		roleJoueur = new Label();
		roleJoueur.getStyleClass().add("roleJoueur");
		GridPane.setHgrow(roleJoueur, Priority.ALWAYS);
		iconeJoueur = new Label();
		iconeJoueur.getStyleClass().add("iconeJoueur");
		GridPane.setHalignment(iconeJoueur, HPos.CENTER);

		nomJoueur = new Label();
		nomJoueur.setMaxWidth(135);
		nomJoueur.getStyleClass().add("nomJoueur");
		GridPane.setMargin(nomJoueur, new Insets(3, 0, 0, 0));
		GridPane.setHgrow(nomJoueur, Priority.ALWAYS);
		ageJoueur = new Label();
		ageJoueur.getStyleClass().add("ageJoueur");
		GridPane.setMargin(ageJoueur, new Insets(3, 0, 0, 0));
		
		cellLayout.add(roleJoueur, 0, 0);
		cellLayout.add(iconeJoueur, 1, 0);
		cellLayout.add(nomJoueur, 0, 1);
		cellLayout.add(ageJoueur, 1, 1);
	}
	
	@Override
	protected void updateItem(Joueur item, boolean empty) {
		super.updateItem(item, empty);
		setText(null);
		if (empty) {
			setGraphic(null);
		} else {
			switch (item.getRole()) {
			case "joueur":
				roleJoueur.setText("Joueur");
				iconeJoueur.setText("");
				break;
			case "entraineur":
				roleJoueur.setText("Entraineur");
				iconeJoueur.setText("");
				break;
			case "arbitre":
				roleJoueur.setText("Arbitre");
				iconeJoueur.setText("");
				break;
			}
			nomJoueur.setText(item.getPrenom()+" "+item.getNom());
			Period age = Period.between(item.getDateNais(), now);
			ageJoueur.setText(age.getYears()+" ans");
			setGraphic(cellLayout);
		}
	}
}
