package controlleur.customListCell;

import modele.Action;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.control.ListCell;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;

public class ListActionCell extends ListCell<Action> {
	GridPane cellLayout;
	Text timer;
	Text action;
	Text joueur;
	TextFlow texte;
	
	public ListActionCell() {
		super();
		cellLayout = new GridPane();
		timer = new Text();
		timer.getStyleClass().add("texteTimer");
		action = new Text();
		action.getStyleClass().add("recapLight");
		joueur = new Text();
		joueur.getStyleClass().add("recapNbr");
		texte = new TextFlow();
	}
	
	@Override
	protected void updateItem(Action item, boolean empty) {
		super.updateItem(item, empty);
		setText(null);
		if (empty) {
			setGraphic(null);
		} else {
			ObservableList<Action> liste = getListView().getItems();
			timer.setText(item.getTimer());
			action.setText(" : " + item.getAction() + " de ");
			joueur.setText(item.getJoueur());
			texte.getChildren().clear();
			texte.getChildren().addAll(timer, action, joueur);
			if (liste.indexOf(item) != 0 && liste.get((liste.indexOf(item)-1)).getEquipe().equals(item.getEquipe())) {
				cellLayout.getChildren().clear();
				cellLayout.add(texte, 0, 0);
			} else {
				Text equipe = new Text(item.getEquipe());
				equipe.getStyleClass().add("miniNomEquipe");
				if (liste.indexOf(item) != 0) {
					GridPane.setMargin(equipe, new Insets(5, 0, 0, 0));
				}
				cellLayout.getChildren().clear();
				cellLayout.add(equipe, 0, 0);
				cellLayout.add(texte, 0, 1);
			}
			setGraphic(cellLayout);
		}
	}

}
