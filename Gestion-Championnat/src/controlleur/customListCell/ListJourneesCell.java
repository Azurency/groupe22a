package controlleur.customListCell;

import java.time.format.DateTimeFormatter;

import modele.Journee;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;

public class ListJourneesCell extends ListCell<Journee> {
	private Label jour;
	
	public ListJourneesCell() {
		super();
		jour = new Label();
		jour.getStyleClass().add("journeeDate");
	}
	
	@Override
	protected void updateItem(Journee item, boolean empty) {
		super.updateItem(item, empty);
		setText(null);
		if (empty) {
			setGraphic(null);
		} else {
			jour.setText(item.getDate().format(DateTimeFormatter.ofPattern("dd/MM/YYYY")));
			setGraphic(jour);
		}
	}
}
