package controlleur.customListCell;

import modele.Match;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.layout.GridPane;

public class ListMatchCell extends ListCell<Match> {
	GridPane cellLayout;
	Label equipe1;
	Label equipe2;
	Label versus;
	
	public ListMatchCell() {
		super();
		initLayout();
	}
	
	private void initLayout() {
		cellLayout = new GridPane();
		
		equipe1 = new Label();
		equipe1.getStyleClass().add("nomEquipe");
		
		equipe2 = new Label();
		equipe2.getStyleClass().add("nomEquipe");
		
		versus = new Label("Match");
		versus.getStyleClass().add("versus");
		
		cellLayout.add(equipe1, 0, 1);
		cellLayout.add(versus, 0, 0);
		cellLayout.add(equipe2, 0, 2);
	}
	
	@Override
	protected void updateItem(Match item, boolean empty) {
		super.updateItem(item, empty);
		setText(null);
		if (empty) {
			setGraphic(null);
		} else {
			equipe1.setText(item.getEquipe1().getNom());
			equipe2.setText(item.getEquipe2().getNom());
			setGraphic(cellLayout);
		}
	}

}
