package controlleur.customListCell;

import java.io.File;
import java.io.IOException;

import controlleur.fenetreCreation.EquipeScreenController;
import modele.Equipe;
import modele.Joueur;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;

public class ListEquipeCell extends ListCell<Equipe> {
	HBox cellLayout;
	Pane pane; // pour aligner à droite le nombre de joueur
	Label nomEquipe;
	Label nombreJoueurs;
	ScrollPane parent;
	EquipeScreenController c;
	ListView<Equipe> equipesLV;

	public ListEquipeCell(ScrollPane s, EquipeScreenController c, ListView<Equipe> lv) {
		super();
		parent = s;
		this.c = c;
		equipesLV = lv;

		initializeComponents();
		initializeEvents();
	}

	public void initializeComponents() {
		cellLayout = new HBox();
		pane = new Pane();
		HBox.setHgrow(pane, Priority.ALWAYS);
		nomEquipe = new Label();
		nombreJoueurs = new Label();
		nombreJoueurs.getStyleClass().add("labelNombre");
		HBox.setMargin(nombreJoueurs, new Insets(2, 10, 0, 0));

		cellLayout.getChildren().addAll(nomEquipe, pane, nombreJoueurs);
	}

	public void initializeEvents() {
		setOnDragDetected(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				if (getItem().getId() != 0) {
					Dragboard db = equipesLV.startDragAndDrop(TransferMode.ANY);
					ClipboardContent content = new ClipboardContent();
					File tmpFile = null;
					try {
						tmpFile = File.createTempFile(getItem().getNom() + " save", ".yaml");
					} catch (IOException e) {
						e.printStackTrace();
					}
					getItem().saveEquipeYAML(tmpFile);
					content.putFiles(FXCollections.observableArrayList(tmpFile));
					db.setContent(content);
					event.consume();
				}
			}
		});

		setOnDragDone(new EventHandler<DragEvent>() {

			@Override
			public void handle(DragEvent event) {
				event.consume();
			}
		});

		setOnScroll(new EventHandler<ScrollEvent>() {

			@Override
			public void handle(ScrollEvent event) {
				ScrollEvent.fireEvent(parent, event); // ne pas consumer l'event
			}
		});

		setOnDragEntered(new EventHandler<DragEvent>() {

			@Override
			public void handle(DragEvent event) {
				if (event.getTransferMode() == TransferMode.MOVE && event.getDragboard().hasString()) {
					if (((ListEquipeCell)event.getSource()).getItem() != null && !(((ListEquipeCell)event.getSource()).getItem()).equals(equipesLV.getSelectionModel().getSelectedItem())) {
						nomEquipe.getStyleClass().add("cell-drag-over");
						nombreJoueurs.getStyleClass().add("cell-drag-over");
					}
				}
				event.consume();
			}
		});

		setOnDragOver(new EventHandler<DragEvent>() {
			@Override
			public void handle(DragEvent event) {
				if (event.getTransferMode() == TransferMode.MOVE && event.getDragboard().hasString()) {
					if (((ListEquipeCell)event.getSource()).getItem() != null && !(((ListEquipeCell)event.getSource()).getItem()).equals(equipesLV.getSelectionModel().getSelectedItem())) {
						event.acceptTransferModes(TransferMode.MOVE);
					}
				}
				event.consume();
			}
		});

		setOnDragDropped(new EventHandler<DragEvent>() {

			@Override
			public void handle(DragEvent event) {
				// si on drag sur autre chose que l'équipe actuelle
				if (!(((ListEquipeCell)event.getSource()).getItem()).equals(equipesLV.getSelectionModel().getSelectedItem()) && 
						event.getDragboard().hasContent(ListJoueurCell.dataFormat)) {
					Joueur joueurDragged = (Joueur) event.getDragboard().getContent(ListJoueurCell.dataFormat);
					joueurDragged.setEquipe(c.getVue().getEquipesListView().getSelectionModel().getSelectedItem());
					joueurDragged.changerEquipe(getItem());
					c.addToJoueurModifies(joueurDragged);
					c.removeJoueurFromLV(joueurDragged);
				}
				event.consume();
			}
		});

		setOnDragExited(new EventHandler<DragEvent>() {
			@Override
			public void handle(DragEvent event) {
				if (event.getTransferMode() == TransferMode.MOVE && event.getDragboard().hasString()) {
					nomEquipe.getStyleClass().remove("cell-drag-over");
					nombreJoueurs.getStyleClass().remove("cell-drag-over");
				}
				event.consume();
			}
		});
	}

	@Override
	protected void updateItem(Equipe item, boolean empty) {
		super.updateItem(item, empty);
		setText(null);
		if (empty) {
			setGraphic(null);
		} else {
			nomEquipe.setText(item != null ? item.getNom() : "null");
			nomEquipe.setOnMouseClicked(new EventHandler<MouseEvent>() {
				private boolean escaped;

				@Override
				public void handle(MouseEvent event) {
					if (event.getClickCount() == 2 && !equipesLV.getSelectionModel().getSelectedItem().getNom().equals("Sans équipe")) {
						escaped = false;
						final TextField editField = new TextField(nomEquipe.getText());
						editField.setPrefWidth(150);
						editField.getStyleClass().add("nomEquipeEdit");
						nomEquipe.setGraphic(editField);
						nomEquipe.setContentDisplay(ContentDisplay.GRAPHIC_ONLY);
						editField.requestFocus();

						editField.setOnKeyReleased(new EventHandler<KeyEvent>() {
							@Override
							public void handle(KeyEvent event) {
								if (event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.TAB) {
									nomEquipe.setContentDisplay(ContentDisplay.TEXT_ONLY);
									if (editField.getText() == null || editField.getText().equals("") || editField.getText().equals("Sans équipe")) {
										return;
									}
									nomEquipe.setText(editField.getText());
									item.setNom(editField.getText());
									c.addToEquipeModifiees(item);
									c.refreshNoJoueurPane(item);
								} else if (event.getCode() == KeyCode.ESCAPE) {
									escaped = true;
									nomEquipe.setContentDisplay(ContentDisplay.TEXT_ONLY);
								}
							}
						});

						editField.focusedProperty().addListener(new ChangeListener<Boolean>() {

							@Override
							public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
								if (!newValue && !escaped) {
									nomEquipe.setContentDisplay(ContentDisplay.TEXT_ONLY);
									if (editField.getText() == null || editField.getText().equals("") || editField.getText().equals("Sans équipe")) {
										return;
									}
									nomEquipe.setText(editField.getText());
									item.setNom(editField.getText());
									c.addToEquipeModifiees(item);
									c.refreshNoJoueurPane(item);
								}
							}
						});
					}
				}
			});
			nombreJoueurs.setText(item != null ? item.getNombreJoueurs()+"" : "null");
			setGraphic(cellLayout);
		}
	}
}
