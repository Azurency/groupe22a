package controlleur.customListCell;

import java.io.File;

import application.Main;
import modele.Match;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.ListCell;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;

public class ListProchainsMatchsCell extends ListCell<Match> {
	private GridPane cellLayout;
	private Text match;
	private Button downloadButton;

	public ListProchainsMatchsCell() {
		super();
		initComponents();
	}

	private void initComponents() {
		cellLayout = new GridPane();
		match = new Text();
		GridPane.setHgrow(match, Priority.ALWAYS);
		downloadButton = new Button("");
		downloadButton.getStyleClass().add("downloadButton");

		cellLayout.add(match, 0, 0);
		cellLayout.add(downloadButton, 1, 0);
	}

	@Override
	protected void updateItem(Match item, boolean empty) {
		super.updateItem(item, empty);
		setText(null);
		if (empty) {
			setGraphic(null);
		} else {
			match.setText(item.getEquipe1().getNom() + " vs " + item.getEquipe2().getNom());
			downloadButton.setOnAction(new EventHandler<ActionEvent>() {

				@Override
				public void handle(ActionEvent event) {
					FileChooser fileChooser = new FileChooser();
					fileChooser.setTitle("Enregistrer la feuille de match");
					fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("YAML", "*.yaml", "*.yml"));
					fileChooser.setInitialDirectory(new File(System.getProperty("user.home"))); 
					fileChooser.setInitialFileName(item.getTextEquipes());
					File file = fileChooser.showSaveDialog(Main.getStage());
					if (file != null) {
						item.genererFeuilleMatch(file);
					}
				}
			});
			setGraphic(cellLayout);
		}
	}
}
