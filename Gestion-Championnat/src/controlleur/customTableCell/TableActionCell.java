package controlleur.customTableCell;

import java.io.File;
import java.time.LocalDate;

import controlleur.fenetrePrincipale.FenetrePrincipaleScreenController;
import application.Main;
import modele.Match;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.stage.FileChooser;

public class TableActionCell extends TableCell<Match, Boolean> {
	private Button action;
	FenetrePrincipaleScreenController c;

	public TableActionCell(FenetrePrincipaleScreenController c) {
		super();
		this.c = c;
		action = new Button();
		action.getStyleClass().add("actionButton");
	}

	@Override
	protected void updateItem(Boolean item, boolean empty) {
		super.updateItem(item, empty);
		setText(null);
		if (empty) {
			setGraphic(null);
		} else {
			Match matchItem = (Match) getTableRow().getItem();
			if (matchItem.getJour().getDate().compareTo(LocalDate.now()) < 0) {
				if (!item) {
					action.setText("charger feuille");
				} else {
					action.setText("voir le match");
				}
			} else {
				action.setText("générer feuille");
			}
			action.setOnAction(new EventHandler<ActionEvent>() {

				@Override
				public void handle(ActionEvent event) {
					if (!action.getText().equals("voir le match")) {
						FileChooser fileChooser = new FileChooser();
						fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("YAML", "*.yaml", "*.yml"));
						fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
						if (action.getText().equals("charger feuille")) {
							fileChooser.setTitle("Charger la feuille de match");
							File file = fileChooser.showOpenDialog(Main.getStage());							
							if (file != null) {
								matchItem.charger(file);
							}
							c.refreshPodium();
							c.refreshClassement();
							c.refreshMatchs();
							c.refreshRecapitulatif();
							c.refreshProchainMatch();
						} else if (action.getText().equals("générer feuille")) {
							fileChooser.setTitle("Enregistrer la feuille de match");
							fileChooser.setInitialFileName(matchItem.getTextEquipes());
							File file = fileChooser.showSaveDialog(Main.getStage());
							if (file != null) {
								matchItem.genererFeuilleMatch(file);
							}
						}
					} else { // Si l'action est : voir le match
						c.afficherLeCompteRenduDuMatch(matchItem);
					}
				}
			});
			setGraphic(action);
		}
	}
}
