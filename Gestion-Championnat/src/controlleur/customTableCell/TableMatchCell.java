package controlleur.customTableCell;

import java.time.LocalDate;

import modele.Match;
import javafx.scene.control.TableCell;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;

public class TableMatchCell extends TableCell<Match, Integer> {
	private TextFlow matchText;
	private Text signeMatch;
	private Text valueMatch;
	
	public TableMatchCell() {
		super();
		initComponents();
	}
	
	private void initComponents() {
		signeMatch = new Text();
		signeMatch.getStyleClass().add("signeRecapitulatif");
		valueMatch = new Text();
		valueMatch.getStyleClass().add("recapLight");
		matchText = new TextFlow(signeMatch, valueMatch);
	}
	
	@Override
	protected void updateItem(Integer item, boolean empty) {
		super.updateItem(item, empty);
		setText(null);
		if (empty) {
			setGraphic(null);
		} else {
			Match matchItem = (Match) getTableRow().getItem();
			if (matchItem.getJour().getDate().compareTo(LocalDate.now()) < 0) {
				if (!matchItem.estCharge()) {
					signeMatch.setText("");
				} else {
					signeMatch.setText(" ");
				}
			} else {
				signeMatch.setText("");
			}
			valueMatch.setText("  "+matchItem.getEquipe1().getNom() + " vs " + matchItem.getEquipe2().getNom());
			setGraphic(matchText);
		}
	}

}
