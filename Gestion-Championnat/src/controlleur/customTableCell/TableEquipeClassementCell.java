package controlleur.customTableCell;

import modele.Equipe;
import javafx.scene.control.TableCell;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;

public class TableEquipeClassementCell extends TableCell<Equipe, String> {
	private Text numClassement;
	private Text nomEquipe;
	private TextFlow text;

	public TableEquipeClassementCell() {
		numClassement = new Text();
		numClassement.getStyleClass().add("podiumNumberBig");
		nomEquipe = new Text();
		nomEquipe.getStyleClass().add("recapLight");
		text = new TextFlow(numClassement, nomEquipe);
	}

	@Override
	protected void updateItem(String item, boolean empty) {
		super.updateItem(item, empty);
		setText(null);
		if (empty) {
			setGraphic(null);
		} else {
			Equipe matchItem = (Equipe) getTableRow().getItem();
			numClassement.setText((getTableView().getItems().indexOf(matchItem)+1)+"");
			nomEquipe.setText(" - " + item);
			setGraphic(text);
		}
	}
}
