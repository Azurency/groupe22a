package controlleur.customTableCell;

import modele.Equipe;
import javafx.scene.control.TableCell;
import javafx.scene.text.Text;

public class TablePointsClassementCell extends TableCell<Equipe, Integer> {
	private Text points;
	
	public TablePointsClassementCell() {
		points = new Text();
		points.getStyleClass().add("recapDate");
	}
	
	@Override
	protected void updateItem(Integer item, boolean empty) {
		super.updateItem(item, empty);
		setText(null);
		if (empty) {
			setGraphic(null);
		} else {
			points.setText(item + " points");
			setGraphic(points);
		}
	}
}
