package controlleur.customTableCell;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import modele.Journee;
import modele.Match;
import javafx.scene.control.TableCell;
import javafx.scene.text.Text;

public class TableDateCell extends TableCell<Match, Journee> {
	private Text dateTexte;
	private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
	
	public TableDateCell() {
		super();
		dateTexte = new Text();
		dateTexte.getStyleClass().add("recapDate");
	}
	
	@Override
	protected void updateItem(Journee item, boolean empty) {
		super.updateItem(item, empty);
		setText(null);
		if (empty) {
			setGraphic(null);
		} else {
			LocalDate date = item.getDate();
			dateTexte.setText("le " + date.format(formatter));
			setGraphic(dateTexte);
		}
	}
}
