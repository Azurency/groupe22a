package controlleur.fenetreCreation;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Calendar;

import application.FenetreCreation;
import controlleur.ControlledScreen;
import controlleur.ScreensController;
import vue.fenetreCreation.DateScreenView;
import javafx.animation.FadeTransition;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.ScrollBar;
import javafx.scene.control.cell.TextFieldListCell;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.util.Duration;
import javafx.util.StringConverter;
import jfxtras.internal.scene.control.skin.DateTimeToCalendarHelper;

public class DateScreenController implements ControlledScreen {

	double x, y; // Position de la fenetre
	private DateScreenView vue; // La vue
	private ScreensController screensManager;

	private Timeline fadeScrollTL;
	private FadeTransition fadeout;
	private ScrollBar scrollbar;
	
	private static int nombreEquipeMax;
	private static ObservableList<LocalDate> datesSelectionnees;

	public DateScreenController() {
		vue = new DateScreenView();
		nombreEquipeMax = 4;
		datesSelectionnees = FXCollections.observableArrayList();

		// attribution des events
		vue.getContinuer().setOnMouseReleased(new ContinuerEventHandler());
		
		vue.getDateListView().addEventHandler(ScrollEvent.ANY, new ListViewScrollEventHandler());
		vue.getDateListView().addEventHandler(MouseEvent.ANY, new ListViewMouseEventHandler());
		
		vue.getCalendrier().calendars().addListener(new ListViewChangeListener());
		
		// --------------------------------------------------
		// Remplissage de la ListView
		// --------------------------------------------------
		
		vue.getDateListView().setItems(datesSelectionnees);
		
		vue.getDateListView().setCellFactory(TextFieldListCell.forListView(new StringConverter<LocalDate>() {

			@Override
			public String toString(LocalDate object) {
				return object == null ? "" : object.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL));
			}

			@Override
			public LocalDate fromString(String string) {
				return null;
			}
		}));
		
	}

	@Override
	public void setScreenParent(ScreensController screenPage) {
		screensManager = screenPage;	
	}

	public DateScreenView getVue() {
		return vue;
	}
	
	public static int getNombreEquipeMax() {
		return nombreEquipeMax;
	}
	
	public static ObservableList<LocalDate> getDatesSelectionnees() {
		return datesSelectionnees;
	}
	
	// --------------------------------------------------
	// Méthodes scroll
	// --------------------------------------------------

	public void stopScrollbarHiding() {
		if (fadeScrollTL != null) {
			fadeout.stop();
			fadeScrollTL.stop();
		}
		scrollbar.setOpacity(1);
	}
	
	public void playScrollbarHiding() {
		stopScrollbarHiding();

		fadeout = new FadeTransition(new Duration(200), scrollbar);
		fadeout.setToValue(0.0);

		fadeScrollTL = new Timeline(
				new KeyFrame(
						new Duration(1200), 
						new EventHandler<ActionEvent>() {
							@Override
							public void handle(ActionEvent event) {
								fadeout.play();

							}
						}
						)
				);
		fadeScrollTL.play();
	}
	
	public void checkScrollbarCreated() {
		if (scrollbar == null && vue.getDateListView().lookup(".scroll-bar") != null) {
			scrollbar = (ScrollBar) vue.getDateListView().lookup(".scroll-bar");
			scrollbar.addEventHandler(MouseEvent.ANY, new ListViewMouseEventHandler());
		}
	}
	
	
	
	
	
	// --------------------------------------------------
	// evenements
	// --------------------------------------------------
	
	
	class TextEventHandler implements EventHandler<MouseEvent> {
		@Override
		public void handle(MouseEvent event) {
			event.consume();
		}

	}

	class ContinuerEventHandler implements EventHandler<MouseEvent> {

		@Override
		public void handle(MouseEvent event) {
			int nbrDates = vue.getDateListView().getItems().size();
			if (nbrDates < 6) {
				vue.getNbrJoursTooltip().show(vue.getContinuer(), event.getScreenX(), event.getScreenY() - 45);
				vue.getNbrJoursTooltip().setAutoHide(true);
				vue.getNbrJoursTooltip().setConsumeAutoHidingEvents(false);
			}
			else {
				screensManager.setScreen(FenetreCreation.SCREEN2ID);
			}
		}

	}

	class ListViewScrollEventHandler implements EventHandler<ScrollEvent> {

		@Override
		public void handle(ScrollEvent event) {
			checkScrollbarCreated();

			if (scrollbar != null) {
				playScrollbarHiding();
			}
			event.consume();
		}

	}

	class ListViewMouseEventHandler implements EventHandler<MouseEvent> {

		@Override
		public void handle(MouseEvent event) {
			checkScrollbarCreated();
			if (scrollbar != null) {
				if (event.getSource().equals(scrollbar)) {
					if (event.getEventType().equals(MouseEvent.MOUSE_EXITED) || event.getEventType().equals(MouseEvent.MOUSE_EXITED_TARGET)) {
						playScrollbarHiding();
					}
					else {
						stopScrollbarHiding();
					}
				}
				else {
					playScrollbarHiding();
				}
			}
			event.consume();
		}

	}
	
	class ListViewChangeListener implements ListChangeListener<Calendar> {

		@Override
		public void onChanged(javafx.collections.ListChangeListener.Change<? extends Calendar> c) {
			if (c.next()) {
				if (c.getRemovedSize() < 1) {
					LocalDate dateChange = DateTimeToCalendarHelper.createLocalDateFromCalendar(c.getAddedSubList().get(0));
					if (!datesSelectionnees.contains(dateChange)) {
						datesSelectionnees.add(dateChange);
						FXCollections.sort(datesSelectionnees);
					}
				}
				else {
					LocalDate dateRemoved = DateTimeToCalendarHelper.createLocalDateFromCalendar(c.getRemoved().get(0));
					datesSelectionnees.remove(dateRemoved);
					FXCollections.sort(datesSelectionnees);
				}
			}
			if (nombreEquipeMax < 16) {
				int nbrDates = vue.getDateListView().getItems().size();
				if (nbrDates > 6) {
					nombreEquipeMax = (int) Math.ceil((nbrDates + 1)/2);
					vue.getlNombreEquipes().setText("Vous pouvez inscrire au maximum " 
													+ nombreEquipeMax + " équipes");
				}
			}
			checkScrollbarCreated();
			if (scrollbar != null) {
				playScrollbarHiding();
			}
		}
		
	}

}
