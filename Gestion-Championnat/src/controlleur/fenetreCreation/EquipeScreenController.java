package controlleur.fenetreCreation;

import java.awt.MouseInfo;
import java.awt.Point;
import java.io.File;
import java.io.FileNotFoundException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.time.format.FormatStyle;
import java.util.ArrayList;

import org.ho.yaml.exception.YamlException;

import application.FenetreCreation;
import javafx.animation.FadeTransition;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.ScrollBar;
import javafx.scene.control.TextField;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import javafx.util.Callback;
import javafx.util.Duration;
import vue.fenetreCreation.EquipeScreenView;
import modele.Equipe;
import modele.Joueur;
import controlleur.ControlledScreen;
import controlleur.ScreensController;
import controlleur.customListCell.ListEquipeCell;
import controlleur.customListCell.ListJoueurCell;

public class EquipeScreenController implements ControlledScreen {
	private static ObservableList<Equipe> equipes;
	private ObservableList<Joueur> joueurs;
	private ObservableList<Joueur> nouvelleListeJoueurs; // pour la recherche
	private ScreensController screensManager;
	private EquipeScreenView vue;
	
	private final int LIST_CELL_HEIGHT = 35; // La taille d'un onglet à gauche
	Timeline fadeScrollGaucheTL;
	FadeTransition fadeoutGauche;
	private ScrollBar scrollbarGauche;
	Timeline fadeScrollMilieuTL;
	FadeTransition fadeoutMilieu;
	private ScrollBar scrollbarMilieu;
	
	private static final ObservableList<String> roleJoueurs = FXCollections.observableArrayList("joueur", "arbitre", "entraineur");
	private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d MMMM yyyy"); // Format de date entré par l'utilisateur
	private static final DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("d/M/yyyy"); // Format de date entré par l'utilisateur
	private static ArrayList<Equipe> equipesSupprimees;
	private static ArrayList<Equipe> equipesModifiees;
	private static ArrayList<Equipe> equipesAjoutees;
	private static ArrayList<Joueur> joueursModifies;
	private static ArrayList<Joueur> joueursAjoutes;

	public EquipeScreenController() {
		vue = new EquipeScreenView();
		screensManager = null;
		equipes = FXCollections.observableArrayList();
		joueurs = FXCollections.observableArrayList();
		equipesSupprimees = new ArrayList<Equipe>();
		equipesModifiees = new ArrayList<Equipe>();
		equipesAjoutees = new ArrayList<Equipe>();
		joueursModifies = new ArrayList<Joueur>();
		joueursAjoutes = new ArrayList<Joueur>();

		equipes.addAll(Equipe.getEquipeFromBD()); // Charge les équipes présente dans la BD
		vue.getRoleJoueurComboBox().getStylesheets().add(getClass().getResource("/vue/fenetreCreation/FenetreCreationComboBox.css").toExternalForm());



		vue.getEquipesListView().setItems(equipes);
		vue.getJoueursListView().setItems(joueurs);
		vue.getRoleJoueurComboBox().setItems(roleJoueurs);
		vue.getRoleJoueurComboBox().setOnHiding(new EventHandler<Event>() {
			@Override
			public void handle(Event event) {
				Point p = MouseInfo.getPointerInfo().getLocation();
				MouseEvent e = new MouseEvent(MouseEvent.MOUSE_PRESSED, 1, 1, p.getX(), p.getY(), MouseButton.PRIMARY, 1, false, false, false, false, false, false, false, false, false, false, null);
				Event.fireEvent(screensManager, e);
			}
		});

		vue.getTitledPaneTitleNombre().setText((equipes.size() - 1)+"");
		vue.getExplicationTexteGras().setText("entre 4 et " + DateScreenController.getNombreEquipeMax() + " équipes.");
		vue.getPanePrincipal().addEventFilter(ActionEvent.ANY, new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				if (event.getTarget().equals(vue.getPanePrincipal())) {
					if (DateScreenController.getNombreEquipeMax() == 4) {
						vue.getExplicationTexteGras().setText("exactement 4 équipes.");
					}
					else {
						vue.getExplicationTexteGras().setText("entre 4 et " + DateScreenController.getNombreEquipeMax() + " équipes.");
					}
					vue.getTropEquipeTooltip().setText("Vous devez inscrire moins de " + DateScreenController.getNombreEquipeMax() + " équipes");
				}
			}
		});

		vue.getPanePrincipal().setOnDragOver(new EventHandler<DragEvent>() {

			@Override
			public void handle(DragEvent event) {
				Dragboard db = event.getDragboard();
				if (db.hasFiles()) {
					event.acceptTransferModes(TransferMode.COPY);
				} else {
					event.consume();
				}
			}
		});

		vue.getPanePrincipal().setOnDragDropped(new EventHandler<DragEvent>() {

			@Override
			public void handle(DragEvent event) {
				for (File file:event.getDragboard().getFiles()) {
					Equipe aCharger = null;
					try {
						aCharger = Equipe.chargerEquipeYAML(file);
						boolean dejaCharger = false;
						for (Equipe e : equipes) {
							if (aCharger.equalsWithouId(e)) {
								dejaCharger = true;
							}
						}
						if (dejaCharger) {
							vue.getEquipeDejaChargerTooltip().show(screensManager.getS().getWindow());
							vue.getEquipeDejaChargerTooltip().setAnchorX(event.getScreenX());
							vue.getEquipeDejaChargerTooltip().setAnchorY(event.getScreenY());
							vue.getEquipeDejaChargerTooltip().setAutoHide(true);
							vue.getEquipeDejaChargerTooltip().setConsumeAutoHidingEvents(false);
						} else {
							aCharger = Equipe.chargerEquipeYAML(file);
							addToEquipeAjoutees(aCharger);
							if (!equipes.get(0).getJoueurs().isEmpty()) {
								ArrayList<Joueur> toRemove = new ArrayList<Joueur>();
								for (Joueur j : equipes.get(0).getJoueurs()) {
									for (Joueur j2 : aCharger.getJoueurs()) {
										if (j.equalsWithoutId(j2)) {
											toRemove.add(j);
											addToJoueurModifies(j);
										}
									}
								}
								equipes.get(0).getJoueurs().removeAll(toRemove);
							}
							for (Joueur j : aCharger.getJoueurs()) {
								addToJoueurAjoutes(j);
							}
							equipes.add(aCharger);
							addToEquipeAjoutees(aCharger);
							vue.getTitledPaneTitleCell().getStyleClass().remove("titleCellSelected");
							vue.getTitledPaneTitleNombre().getStyleClass().remove("titledPaneTitleNombreSelected");
							vue.getTitledPaneTitleSigne().getStyleClass().remove("titledPaneTitleSigneSelected");
							vue.getTitledPaneTitleTexte().getStyleClass().remove("titledPaneTitleTexteSelected");
							refreshEquipeListView(equipes.indexOf(aCharger), true);
						}
					} catch (YamlException | ClassCastException | FileNotFoundException e) {
						vue.getPasBonFormatEquipeTooltip().show(screensManager.getS().getWindow());
						vue.getPasBonFormatEquipeTooltip().setAnchorX(event.getScreenX());
						vue.getPasBonFormatEquipeTooltip().setAnchorY(event.getScreenY());
						vue.getPasBonFormatEquipeTooltip().setAutoHide(true);
						vue.getPasBonFormatEquipeTooltip().setConsumeAutoHidingEvents(false);
					}
				}
			}
		});

		vue.getEquipesListView().prefHeightProperty().bind(Bindings.size(equipes).multiply(LIST_CELL_HEIGHT).add(3));

		vue.getTitledPaneTitleCell().setOnMouseClicked(new TitreTitledPaneMouseEventHandler());
		vue.getEquipesListView().setOnMousePressed(new EquipeListViewMouseEventHandler());
		vue.getRechercheTextField().setOnMouseClicked(new RechercheTextFieldMouseEventHandler());
		vue.getRechercheTextField().textProperty().addListener(new RechercheTextFieldChangeListener());
		vue.getRechercheTextField().focusedProperty().addListener(new RechercheFieldFocusChangeListener());
		vue.getJoueursListView().getSelectionModel().selectedItemProperty().addListener(new JoueursListViewChangeLister());
		vue.getEquipesListView().getSelectionModel().selectedItemProperty().addListener(new EquipesListViewChangeListener());
		vue.getEquipesListView().addEventHandler(MouseEvent.ANY, new AccordeonMouseEventHandler());
		vue.getAccordeon().setOnMouseEntered(new AccordeonMouseEventHandler());
		vue.getAccordeon().setOnMouseMoved(new AccordeonMouseEventHandler());
		vue.getAccordeon().addEventHandler(ScrollEvent.ANY, new AccordeonScrollEventHandler());
		vue.getJoueursListView().addEventHandler(MouseEvent.ANY, new JoueursListViewMouseEventHandler());
		vue.getJoueursListView().addEventHandler(ScrollEvent.ANY, new JoueurListViewScrollEventHandler());
		vue.getAjouterEquipe().setOnAction(new AjouterEquipeEventHandler());
		vue.getAjouterJoueur().setOnAction(new AjouterJoueurEventHandler());
		
		vue.getJoueurGrid().setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				if (event.getClickCount() == 1) {
					vue.getJoueurGrid().requestFocus();
				}
			}
		});

		vue.getEquipesListView().setCellFactory(new Callback<ListView<Equipe>, ListCell<Equipe>>() {
			@Override
			public ListCell<Equipe> call(ListView<Equipe> param) {
				return new ListEquipeCell(vue.getAccordeon(), EquipeScreenController.this, vue.getEquipesListView());
			}
		});
		
		vue.getJoueursListView().setCellFactory(new Callback<ListView<Joueur>, ListCell<Joueur>>() {

			@Override
			public ListCell<Joueur> call(ListView<Joueur> param) {
				return new ListJoueurCell(vue.getJoueursListView());
			}
		});

		// Rendre le Texte des joueurs éditable
		vue.getPrenomJoueur().setOnMouseClicked(new PrenomDoubleClickToEditEventHandler());
		vue.getNomJoueur().setOnMouseClicked(new NomDoubleClickToEditEventHandler());
		vue.getRoleJoueurComboBox().setOnAction(new RoleJoueurActionEventHandler());
		vue.getDateNaissanceJoueurValeur().setOnMouseClicked(new DateNaissanceDoubleClickToEditEventHandler());
		vue.getProfessionelJoueurValeur().setOnMouseClicked(new ProfessionelDoubleClickToEditEventHandler());
		vue.getNumLicenseJoueurValeur().setOnMouseClicked(new NumLicenceDoubleClickToEditEventHandler());
		vue.getVilleEquipeValeur().setOnMouseClicked(new VilleEquipeDoubleClickToEditEventHandler());
		vue.getNomEquipeValeur().setOnMouseClicked(new NomEquipeDoubleClickToEditEventHandler());

		// Boutons suppression equipe/joueur
		vue.getSupprimerJoueur().setOnAction(new SupprimerJoueurEventHandler());
		vue.getSupprimerEquipe().setOnAction(new SupprimerEquipeEventHandler());


		vue.getPrecedent().setOnMouseReleased(new PrecedentEventHandler());
		vue.getContinuer().setOnMouseReleased(new ContinuerEventHandler());
	}

	@Override
	public void setScreenParent(ScreensController screenPage) {
		screensManager = screenPage;
	}

	public EquipeScreenView getVue() {
		return vue;
	}

	public static ArrayList<Equipe> getEquipesSupprimees() {
		return equipesSupprimees;
	}

	public static ArrayList<Equipe> getEquipesModifiees() {
		return equipesModifiees;
	}

	public static ArrayList<Equipe> getEquipesAjoutees() {
		return equipesAjoutees;
	}

	public static ArrayList<Joueur> getJoueursModifies() {
		return joueursModifies;
	}

	public static ArrayList<Joueur> getJoueursAjoutes() {
		return joueursAjoutes;
	}

	public static ObservableList<Equipe> getEquipes() {
		return equipes;
	}

	// --------------------------------------------------
	// Méthodes scroll
	// --------------------------------------------------

	public void stopScrollbarGaucheHiding() {
		if (fadeScrollGaucheTL != null) {
			fadeoutGauche.stop();
			fadeScrollGaucheTL.stop();
		}
		scrollbarGauche.setOpacity(1);
	}

	public void playScrollbarGaucheHiding() {
		stopScrollbarGaucheHiding();

		fadeoutGauche = new FadeTransition(new Duration(200), scrollbarGauche);
		fadeoutGauche.setToValue(0.0);

		fadeScrollGaucheTL = new Timeline(
				new KeyFrame(
						new Duration(1200), 
						new EventHandler<ActionEvent>() {
							@Override
							public void handle(ActionEvent event) {
								fadeoutGauche.play();

							}
						}
						)
				);
		fadeScrollGaucheTL.play();
	}

	public void checkScrollbarGaucheCreated() {
		if (scrollbarGauche == null && vue.getPanneauGauche().lookup(".scroll-bar") != null) {
			for (Node node : vue.getPanneauGauche().lookupAll(".scroll-bar")) {
				if (node.getParent().equals(vue.getAccordeon()) && node.isVisible()) {
					scrollbarGauche = (ScrollBar) node;
					scrollbarGauche.addEventHandler(MouseEvent.ANY, new AccordeonMouseEventHandler());
				}
			}
		}
	}

	public void stopScrollbarMilieuHiding() {
		if (fadeScrollMilieuTL != null) {
			fadeoutMilieu.stop();
			fadeScrollMilieuTL.stop();
		}
		scrollbarMilieu.setOpacity(1);
	}

	public void playScrollbarMilieuHiding() {
		stopScrollbarMilieuHiding();

		fadeoutMilieu = new FadeTransition(new Duration(200), scrollbarMilieu);
		fadeoutMilieu.setToValue(0.0);

		fadeScrollMilieuTL = new Timeline(
				new KeyFrame(
						new Duration(1200), 
						new EventHandler<ActionEvent>() {
							@Override
							public void handle(ActionEvent event) {
								fadeoutMilieu.play();

							}
						}
						)
				);
		fadeScrollMilieuTL.play();
	}

	public void checkScrollbarMilieuCreated() {
		if (scrollbarMilieu == null && vue.getJoueursListView().lookup(".scroll-bar") != null) {
			scrollbarMilieu = (ScrollBar) vue.getJoueursListView().lookup(".scroll-bar");
			scrollbarMilieu.addEventHandler(MouseEvent.ANY, new JoueursListViewMouseEventHandler());
		}
	}

	/*
	 * Méthodes de MAJ des joueurs
	 */

	private void showPasJoueurGrille() {
		if (vue.getEquipesListView().getSelectionModel().getSelectedItem().getNom().equals("Sans équipe")) {
			vue.showPasJoueurGrid(false);
		}
		else {
			vue.showPasJoueurGrid(true);
		}
	}

	private TextField addTextFieldFor(Text text, TextFlow parentFlow, String styleProperty, int prefWidth) {
		final TextField editField = new TextField(text.getText());
		int indiceTexte = parentFlow.getChildren().indexOf(text);
		parentFlow.getChildren().remove(text);
		parentFlow.getChildren().add(indiceTexte, editField);
		editField.getStyleClass().add(styleProperty);
		editField.getStyleClass().add("editField");
		editField.setPrefWidth(prefWidth);
		editField.requestFocus();
		editField.selectAll();
		return editField;
	}

	private void refreshJoueurListView(int indiceJoueurDansListe) {
		ObservableList<Joueur> toRefresh = vue.getJoueursListView().getItems();
		vue.getJoueursListView().setItems(null);
		vue.getJoueursListView().setItems(toRefresh);
		vue.getJoueursListView().getSelectionModel().select(indiceJoueurDansListe);
	}

	private void validateTextFieldChange(TextField editField, Text text, TextFlow parentFlow, int indiceItemJoueurs, int indiceItemFlow) {
		refreshJoueurListView(indiceItemJoueurs);
		parentFlow.getChildren().remove(editField);
		parentFlow.getChildren().remove(text);
		parentFlow.getChildren().add(indiceItemFlow, text);
	}

	private void cancelTextFieldChange(TextField editField, Text text, TextFlow parentFlow, int indiceItemFlow) {
		parentFlow.getChildren().remove(editField);
		parentFlow.getChildren().remove(text);
		parentFlow.getChildren().add(indiceItemFlow, text);
	}

	private void parseDate(String dateString, Joueur joueurSelectionne, TextField editField, Text text, TextFlow parentFlow, int indiceJoueurSelectionne, int indiceInfoDansTextFlow) {
		try {
			LocalDate newDateNaissance = LocalDate.parse(dateString, formatter);
			joueurSelectionne.setDateNais(newDateNaissance);
			text.setText(newDateNaissance.format(formatter));
			validateTextFieldChange(editField, text, vue.getInfosJoueur(), indiceJoueurSelectionne, indiceInfoDansTextFlow);
		}
		catch (DateTimeParseException exc) {
			try {
				LocalDate newDateNaissance = LocalDate.parse(dateString, formatter2);
				joueurSelectionne.setDateNais(newDateNaissance);
				text.setText(newDateNaissance.format(formatter));
				validateTextFieldChange(editField, text, vue.getInfosJoueur(), indiceJoueurSelectionne, indiceInfoDansTextFlow);
			}
			catch (DateTimeParseException exc2) {
				System.out.println("Impossible de parser la date " + dateString);
				cancelTextFieldChange(editField, text, vue.getInfosJoueur(), indiceInfoDansTextFlow);
			}
		}
	}

	public void refreshNoJoueurPane(Equipe e) {
		vue.getNomEquipeValeur().setText(e.getNom());
		vue.getVilleEquipeValeur().setText(e.getVille());
	}

	public void refreshEquipeListView(int selected) {
		ObservableList<Equipe> list = vue.getEquipesListView().getItems();
		vue.getTitledPaneTitleNombre().setText((equipes.size() - 1)+"");
		vue.getEquipesListView().setItems(null);
		vue.getEquipesListView().layout();
		vue.getEquipesListView().setItems(list);
		vue.getEquipesListView().getSelectionModel().select(selected);
	}
	
	public void refreshEquipeListView(int selected, boolean runLater) {
		ObservableList<Equipe> list = vue.getEquipesListView().getItems();
		vue.getTitledPaneTitleNombre().setText((equipes.size() - 1)+"");
		vue.getEquipesListView().setItems(null);
		vue.getEquipesListView().layout();
		vue.getEquipesListView().setItems(list);
		if (runLater) {
			Platform.runLater(new Runnable() {
				
				@Override
				public void run() {
					vue.getEquipesListView().getSelectionModel().select(selected);
				}
			});
		} else {
			vue.getEquipesListView().getSelectionModel().select(selected);
		}
	}

	public void removeJoueurFromLV(Joueur j) {
		if (vue.getJoueursListView().getItems().equals(nouvelleListeJoueurs)) {
			nouvelleListeJoueurs.remove(j);
		}
		joueurs.remove(j);
		vue.getJoueursListView().getSelectionModel().clearSelection();
		showPasJoueurGrille();
	}

	public void addToEquipeModifiees(Equipe e) {
		if (!equipesModifiees.contains(e) && !equipesAjoutees.contains(e)) {
			equipesModifiees.add(e);
		}
	}

	public void addToEquipeAjoutees(Equipe e) {
		if (!equipesAjoutees.contains(e) && !equipesSupprimees.contains(e)) {
			equipesAjoutees.add(e);
		} 
	}

	public void addToJoueurModifies(Joueur j) {
		if (!joueursModifies.contains(j) && !joueursAjoutes.contains(j)) {
			joueursModifies.add(j);
		} else if (joueursModifies.contains(j)) {
			joueursModifies.remove(j);
			joueursModifies.add(j);
		} else if (joueursAjoutes.contains(j)) {
			joueursAjoutes.remove(j);
			joueursAjoutes.add(j);
		}
	}

	public void addToJoueurAjoutes(Joueur j) {
		if (!joueursAjoutes.contains(j) && !joueursModifies.contains(j)) {
			joueursAjoutes.add(j);
		} else if (joueursAjoutes.contains(j)) {
			joueursAjoutes.remove(j);
			joueursAjoutes.add(j);
		} else if (joueursModifies.contains(j)) {
			joueursModifies.remove(j);
			joueursModifies.add(j);
		}
	}

	public void removeFromJoueursLists(Joueur j) {
		if (joueursAjoutes.contains(j)) {
			joueursAjoutes.remove(j);
		} else { 
			j.supprJoueurBD();
			if (joueursModifies.contains(j)) {
				joueursModifies.remove(j);
			}
		}
	}

	public void removeFromEquipesLists(Equipe e) {
		if (equipesAjoutees.contains(e)) {
			equipesAjoutees.remove(e);
		} else {
			equipesSupprimees.add(e);
			if (equipesModifiees.contains(e)) {
				equipesModifiees.remove(e);
			}
		}
	}


	
	
	
	
	// --------------------------------------------------
	// evenements
	// --------------------------------------------------



	class PrecedentEventHandler implements EventHandler<MouseEvent> {

		@Override
		public void handle(MouseEvent event) {
			screensManager.setScreen(FenetreCreation.SCREEN1ID);
		}

	}

	class ContinuerEventHandler implements EventHandler<MouseEvent> {

		@Override
		public void handle(MouseEvent event) {
			if ((equipes.size() - 1) < 4) {
				vue.getPasAssezEquipeTooltip().show(vue.getContinuer(), event.getScreenX(), event.getScreenY() - 45);
				vue.getPasAssezEquipeTooltip().setAutoHide(true);
				vue.getPasAssezEquipeTooltip().setConsumeAutoHidingEvents(false);
			}
			else if ((equipes.size() - 1) > DateScreenController.getNombreEquipeMax()) {
				vue.getTropEquipeTooltip().show(vue.getContinuer(), event.getScreenX(), event.getScreenY() - 45);
				vue.getTropEquipeTooltip().setAutoHide(true);
				vue.getTropEquipeTooltip().setConsumeAutoHidingEvents(false);
			}
			else {
				boolean pasAssezJoueur = false;
				boolean pasAssezEntraineur = false;
				for (Equipe e : equipes) {
					if (e.getId() != 0) {
						if (e.getJoueurs().size() < 7) {
							pasAssezJoueur = true;
							break;
						} else {
							int cptJoueurs = 0;
							int cptEntraineur = 0;
							for (Joueur j : e.getJoueurs()) {
								if (j.getRole().equals("joueur")) {
									cptJoueurs++;
								} else if (j.getRole().equals("entraineur")) {
									cptEntraineur++;
								}
							}
							if (cptJoueurs < 7) {
								pasAssezJoueur = true;
								break;
							} else if (cptEntraineur == 0) {
								pasAssezEntraineur = true;
								break;
							}
						}
					}
				}
				if (pasAssezJoueur) {
					vue.getPasAssezJoueurTooltip().show(vue.getContinuer(), event.getScreenX(), event.getScreenY() - 45);
					vue.getPasAssezJoueurTooltip().setAutoHide(true);
					vue.getPasAssezJoueurTooltip().setConsumeAutoHidingEvents(false);
				} else if (pasAssezEntraineur) {
					vue.getPasEntraineurTooltip().show(vue.getContinuer(), event.getScreenX(), event.getScreenY() - 45);
					vue.getPasEntraineurTooltip().setAutoHide(true);
					vue.getPasEntraineurTooltip().setConsumeAutoHidingEvents(false);
				} else {
					screensManager.setScreen(FenetreCreation.SCREEN3ID);
				}
			}
		}

	}

	class TitreTitledPaneMouseEventHandler implements EventHandler<MouseEvent> {

		@Override
		public void handle(MouseEvent event) {
			if (!vue.getEquipesListView().getItems().isEmpty()) {
				vue.getEquipesListView().getSelectionModel().clearSelection();
				vue.getTitledPaneTitleCell().getStyleClass().add("titleCellSelected");
				vue.getTitledPaneTitleNombre().getStyleClass().add("titledPaneTitleNombreSelected");
				vue.getTitledPaneTitleSigne().getStyleClass().add("titledPaneTitleSigneSelected");
				vue.getTitledPaneTitleTexte().getStyleClass().add("titledPaneTitleTexteSelected");
			}
		}

	}

	class EquipeListViewMouseEventHandler implements EventHandler<MouseEvent> {

		@Override
		public void handle(MouseEvent event) {
			if (!vue.getEquipesListView().getItems().isEmpty()) {
				vue.getTitledPaneTitleCell().getStyleClass().remove("titleCellSelected");
				vue.getTitledPaneTitleNombre().getStyleClass().remove("titledPaneTitleNombreSelected");
				vue.getTitledPaneTitleSigne().getStyleClass().remove("titledPaneTitleSigneSelected");
				vue.getTitledPaneTitleTexte().getStyleClass().remove("titledPaneTitleTexteSelected");
				if (scrollbarMilieu != null)
					stopScrollbarMilieuHiding();
			}
		}

	}

	class AjouterEquipeEventHandler implements EventHandler<ActionEvent> {

		@Override
		public void handle(ActionEvent event) {
			Equipe newEquipe = new Equipe("Nouvelle equipe", "Double cliquez");
			equipes.add(newEquipe);
			addToEquipeAjoutees(newEquipe);
			vue.getTitledPaneTitleCell().getStyleClass().remove("titleCellSelected");
			vue.getTitledPaneTitleNombre().getStyleClass().remove("titledPaneTitleNombreSelected");
			vue.getTitledPaneTitleSigne().getStyleClass().remove("titledPaneTitleSigneSelected");
			vue.getTitledPaneTitleTexte().getStyleClass().remove("titledPaneTitleTexteSelected");
			refreshEquipeListView(equipes.indexOf(newEquipe));
			if (scrollbarGauche != null)
				stopScrollbarGaucheHiding();
		}

	}

	class AjouterJoueurEventHandler implements EventHandler<ActionEvent> {

		@Override
		public void handle(ActionEvent event) {
			Joueur newJoueur = new Joueur(0, "Nom", "Prenom", false, LocalDate.now(), "joueur", vue.getEquipesListView().getSelectionModel().getSelectedItem());
			joueurs.add(newJoueur);
			addToJoueurAjoutes(newJoueur);
			vue.getEquipesListView().getSelectionModel().getSelectedItem().setJoueurs(new ArrayList<Joueur>(joueurs));
			refreshEquipeListView(vue.getEquipesListView().getSelectionModel().getSelectedIndex());
			refreshJoueurListView(joueurs.indexOf(newJoueur));
			vue.getJoueursListView().scrollTo(newJoueur);
			if (scrollbarMilieu != null)
				stopScrollbarMilieuHiding();
		}

	}

	class RechercheTextFieldMouseEventHandler implements EventHandler<MouseEvent> {

		@Override
		public void handle(MouseEvent event) {
			if (vue.getRechercheTextField().getText().equals("rechercher ...")) {
				vue.getRechercheTextField().clear();
			}
		}

	}

	class RechercheTextFieldChangeListener implements ChangeListener<String> {

		@Override
		public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
			Joueur joueurSelectionne = vue.getJoueursListView().getSelectionModel().getSelectedItem();

			if (oldValue != null && (newValue.length() < oldValue.length()) && !oldValue.equals("rechercher ...")) {
				vue.getJoueursListView().setItems(joueurs);
			}

			// on sépare la chaine pour une meilleur recherche ie pas linéaire, on peu avoir des bouts de mots
			String[] parties = newValue.toLowerCase().split(" ");

			nouvelleListeJoueurs = FXCollections.observableArrayList();
			for (Joueur j : vue.getJoueursListView().getItems()) {
				boolean match = true;
				String rechercheSur = j.getPrenom() + j.getNom() + j.getRole();
				for (String partie : parties) {
					if (! rechercheSur.toLowerCase().contains(partie)) {
						match = false;
						break;
					}
				}
				if (match) {
					nouvelleListeJoueurs.add(j);
				}
			}
			if (!newValue.equals("rechercher ...")) {
				vue.getJoueursListView().setItems(nouvelleListeJoueurs);
				if (nouvelleListeJoueurs.contains(joueurSelectionne)) {
					int index = vue.getJoueursListView().getItems().indexOf(joueurSelectionne);
					vue.getJoueursListView().getSelectionModel().select(index);
				}
				else {
					showPasJoueurGrille();
					vue.getJoueursListView().getSelectionModel().clearSelection();
				}
			}
		}

	}

	class RechercheFieldFocusChangeListener implements ChangeListener<Boolean> {

		@Override
		public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
			if (!newValue) {
				if (vue.getRechercheTextField().getText().equals("")) {
					vue.getRechercheTextField().setText("rechercher ...");
				}
			}
		}

	}

	class JoueursListViewChangeLister implements ChangeListener<Joueur> {

		@Override
		public void changed(ObservableValue<? extends Joueur> observable, Joueur oldValue, Joueur newValue) {
			if (oldValue == null) {
				vue.showJoueurGrid();
			}
			if (newValue != null) {
				vue.getPrenomJoueur().setText(newValue.getPrenom()+"\n");
				vue.getNomJoueur().setText(newValue.getNom());
				vue.getRoleJoueurComboBox().getSelectionModel().select(newValue.getRole());
				vue.getProfessionelJoueurValeur().setText(newValue.isProfessionnel() ? "oui" : "non");
				vue.getDateNaissanceJoueurValeur().setText(newValue.getDateNais().format(DateTimeFormatter.ofLocalizedDate(FormatStyle.LONG)));
				vue.getNumLicenseJoueurValeur().setText(newValue.getNumLic()+"");
				vue.getAncienClubJoueurValeur().setText(newValue.getAncienneEquipesString());
			}
		}

	}

	class EquipesListViewChangeListener implements ChangeListener<Equipe> {

		@Override
		public void changed(ObservableValue<? extends Equipe> observable, Equipe oldValue, Equipe newValue) {
			if (oldValue == null) {
				showPasJoueurGrille();
				refreshNoJoueurPane(newValue);
			}
			if (newValue != null) {
				showPasJoueurGrille();
				vue.getRechercheTextField().setText("rechercher ...");
				joueurs = FXCollections.observableArrayList(newValue.getJoueurs());
				vue.getJoueursListView().setItems(joueurs);
				vue.getJoueursListView().getSelectionModel().clearSelection();
			} else {
				vue.showExplications();
			}
		}

	}

	class AccordeonMouseEventHandler implements EventHandler<MouseEvent> {

		@Override
		public void handle(MouseEvent event) {
			checkScrollbarGaucheCreated();
			if (scrollbarGauche != null) {
				if (event.getSource().equals(scrollbarGauche) && !event.getEventType().equals(MouseEvent.MOUSE_EXITED) &&
						!event.getEventType().equals(MouseEvent.MOUSE_EXITED_TARGET)) {
					stopScrollbarGaucheHiding();
				}
				else {
					playScrollbarGaucheHiding();
				}
			}
			if (event.getEventType().equals(MouseEvent.MOUSE_CLICKED) && event.getClickCount() == 1) {
				if (vue.getJoueursListView().getSelectionModel().getSelectedIndex() != -1) {
					vue.getJoueursListView().getSelectionModel().clearSelection();
					showPasJoueurGrille();
				}
			}
			event.consume();
		}

	}

	class AccordeonScrollEventHandler implements EventHandler<ScrollEvent> {

		@Override
		public void handle(ScrollEvent event) {
			checkScrollbarGaucheCreated();
			if (scrollbarGauche != null) {
				playScrollbarGaucheHiding();
			}
			event.consume();
		}

	}

	class JoueursListViewMouseEventHandler implements EventHandler<MouseEvent> {

		@Override
		public void handle(MouseEvent event) {
			checkScrollbarMilieuCreated();
			if (scrollbarMilieu != null) {
				if (event.getSource().equals(scrollbarMilieu) && !event.getEventType().equals(MouseEvent.MOUSE_EXITED) && 
						!event.getEventType().equals(MouseEvent.MOUSE_EXITED_TARGET)) {
					stopScrollbarMilieuHiding();
				}
				else {
					playScrollbarMilieuHiding();
				}
			}
			event.consume();
		}

	}

	class JoueurListViewScrollEventHandler implements EventHandler<ScrollEvent> {

		@Override
		public void handle(ScrollEvent event) {
			checkScrollbarMilieuCreated();
			if (scrollbarMilieu != null) {
				playScrollbarMilieuHiding();
			}
			event.consume();
		}

	}


	class PrenomDoubleClickToEditEventHandler implements EventHandler<MouseEvent> {
		private Boolean escaped;

		@Override
		public void handle(MouseEvent event) {
			escaped = false;
			if (event.getClickCount() == 2) {
				Text text = (Text) event.getSource();
				int indiceJoueurSelectionne = vue.getJoueursListView().getSelectionModel().getSelectedIndex();
				int indiceDansLeFlow = vue.getNomCompletJoueur().getChildren().indexOf(text);
				final TextField editField = addTextFieldFor(text, vue.getNomCompletJoueur(), "prenomJoueurEquipe", 390);

				editField.setOnKeyPressed(new EventHandler<KeyEvent>() {

					@Override
					public void handle(KeyEvent event) {
						if (event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.TAB) {
							if (editField.getText() == null || editField.getText().equals("")) {
								cancelTextFieldChange(editField, text, vue.getNomCompletJoueur(), indiceDansLeFlow);
								return;
							}
							Joueur joueurSelectionne = vue.getJoueursListView().getSelectionModel().getSelectedItem();
							joueurSelectionne.setPrenom(editField.getText());
							text.setText(editField.getText()+"\n");
							addToJoueurModifies(joueurSelectionne);
							validateTextFieldChange(editField, text, vue.getNomCompletJoueur(), indiceJoueurSelectionne, indiceDansLeFlow);
						}
						if (event.getCode() == KeyCode.ESCAPE) {
							escaped = true;
							cancelTextFieldChange(editField, text, vue.getNomCompletJoueur(), indiceDansLeFlow);
							return;
						}

					}
				});

				editField.focusedProperty().addListener(new ChangeListener<Boolean>() {
					@Override
					public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
						if (!newValue) { // si il y a eu un changement de focus
							if (editField.getText() == null || editField.getText().equals("")) {
								cancelTextFieldChange(editField, text, vue.getNomCompletJoueur(), indiceDansLeFlow);
								return;
							}
							if (!escaped) {
								Joueur joueurSelectionne = vue.getJoueursListView().getSelectionModel().getSelectedItem();
								joueurSelectionne.setPrenom(editField.getText());
								text.setText(editField.getText()+"\n");
								addToJoueurModifies(joueurSelectionne);
								validateTextFieldChange(editField, text, vue.getNomCompletJoueur(), indiceJoueurSelectionne, indiceDansLeFlow);
							}
						}

					}
				});
			}
		}

	}

	class NomDoubleClickToEditEventHandler implements EventHandler<MouseEvent> {
		private Boolean escaped;

		@Override
		public void handle(MouseEvent event) {
			escaped = false;
			if (event.getClickCount() == 2) {
				Text text = (Text) event.getSource();
				int indiceJoueurSelectionne = vue.getJoueursListView().getSelectionModel().getSelectedIndex();
				int indiceInfoDansTextFlow = vue.getNomCompletJoueur().getChildren().indexOf(text);
				Joueur joueurSelectionne = vue.getJoueursListView().getSelectionModel().getSelectedItem();
				final TextField editField = addTextFieldFor(text, vue.getNomCompletJoueur(), "nomJoueurEquipe", 390);

				editField.setOnKeyPressed(new EventHandler<KeyEvent>() {
					@Override
					public void handle(KeyEvent event) {
						if (event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.TAB) {
							if (editField.getText() == null || editField.getText().equals("")) {
								cancelTextFieldChange(editField, text, vue.getNomCompletJoueur(), indiceInfoDansTextFlow);
								return;
							}
							joueurSelectionne.setNom(editField.getText());
							text.setText(editField.getText());
							addToJoueurModifies(joueurSelectionne);
							validateTextFieldChange(editField, text, vue.getNomCompletJoueur(), indiceJoueurSelectionne, indiceInfoDansTextFlow);
						}
						if (event.getCode() == KeyCode.ESCAPE) {
							escaped = true;
							cancelTextFieldChange(editField, text, vue.getNomCompletJoueur(), indiceInfoDansTextFlow);
							return;
						}

					}
				});

				editField.focusedProperty().addListener(new ChangeListener<Boolean>() {
					@Override
					public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
						if (!newValue) { // si il y a eu un changement de focus
							if (editField.getText() == null || editField.getText().equals("")) {
								cancelTextFieldChange(editField, text, vue.getNomCompletJoueur(), indiceInfoDansTextFlow);
								return;
							}
							if (!escaped) {
								joueurSelectionne.setNom(editField.getText());
								text.setText(editField.getText());
								addToJoueurModifies(joueurSelectionne);
								validateTextFieldChange(editField, text, vue.getNomCompletJoueur(), indiceJoueurSelectionne, indiceInfoDansTextFlow);								
							}
						}

					}
				});
			}
		}
	}

	class RoleJoueurActionEventHandler implements EventHandler<ActionEvent> {

		@Override
		public void handle(ActionEvent event) {
			Joueur joueurSelectionne = vue.getJoueursListView().getSelectionModel().getSelectedItem();
			int indexJoueurSelectionne = vue.getJoueursListView().getSelectionModel().getSelectedIndex();
			String oldValue = joueurSelectionne.getRole();
			String newValue = vue.getRoleJoueurComboBox().getValue();
			if (oldValue != newValue) {
				joueurSelectionne.setRole(newValue);
				addToJoueurModifies(joueurSelectionne);
				refreshJoueurListView(indexJoueurSelectionne);
			}
		}

	}

	class DateNaissanceDoubleClickToEditEventHandler implements EventHandler<MouseEvent> {
		private Boolean escaped;

		@Override
		public void handle(MouseEvent event) {
			if (event.getClickCount() == 2) {
				escaped = false;
				Text text = (Text) event.getSource();
				int indiceJoueurSelectionne = vue.getJoueursListView().getSelectionModel().getSelectedIndex();
				int indiceInfoDansTextFlow = vue.getInfosJoueur().getChildren().indexOf(text);
				Joueur joueurSelectionne = vue.getJoueursListView().getSelectionModel().getSelectedItem();
				final TextField editField = addTextFieldFor(text, vue.getInfosJoueur(), "valeurEquipe", 200);

				editField.setOnKeyPressed(new EventHandler<KeyEvent>() {

					@Override
					public void handle(KeyEvent event) {
						if (event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.TAB) {
							if (editField.getText() == null || editField.getText().equals("")) {
								cancelTextFieldChange(editField, text, vue.getInfosJoueur(), indiceInfoDansTextFlow);
								return;
							}
							addToJoueurModifies(joueurSelectionne);
							parseDate(editField.getText().toLowerCase().trim(), joueurSelectionne, editField, text, vue.getInfosJoueur(), indiceJoueurSelectionne, indiceInfoDansTextFlow);
						}
						if (event.getCode() == KeyCode.ESCAPE) {
							escaped = true;
							cancelTextFieldChange(editField, text, vue.getInfosJoueur(), indiceInfoDansTextFlow);
							return;
						}
					}

				});

				editField.focusedProperty().addListener(new ChangeListener<Boolean>() {
					@Override
					public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
						if (!newValue) { // si il y a eu un changement de focus
							if (editField.getText() == null || editField.getText().equals("")) {
								cancelTextFieldChange(editField, text, vue.getInfosJoueur(), indiceInfoDansTextFlow);
								return;
							}
							if (!escaped) {
								addToJoueurModifies(joueurSelectionne);
								parseDate(editField.getText().toLowerCase().trim(), joueurSelectionne, editField, text, vue.getInfosJoueur(), indiceJoueurSelectionne, indiceInfoDansTextFlow);								
							}
						}

					}
				});
			}
		}
	}

	class ProfessionelDoubleClickToEditEventHandler implements EventHandler<MouseEvent> {
		private Boolean escaped;

		@Override
		public void handle(MouseEvent event) {
			if (event.getClickCount() == 2) {
				escaped = false;
				Text text = (Text) event.getSource();
				int indiceJoueurSelectionne = vue.getJoueursListView().getSelectionModel().getSelectedIndex();
				int indiceInfoDansTextFlow = vue.getInfosJoueur().getChildren().indexOf(text);
				Joueur joueurSelectionne = vue.getJoueursListView().getSelectionModel().getSelectedItem();
				final TextField editField = addTextFieldFor(text, vue.getInfosJoueur(), "valeurEquipe", 200);

				editField.setOnKeyPressed(new EventHandler<KeyEvent>() {

					@Override
					public void handle(KeyEvent event) {
						if (event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.TAB) {
							if (editField.getText() == null || editField.getText().equals("")) {
								cancelTextFieldChange(editField, text, vue.getInfosJoueur(), indiceInfoDansTextFlow);
								return;
							}
							if (editField.getText().equals("oui")) {
								joueurSelectionne.setProfessionnel(true);
								addToJoueurModifies(joueurSelectionne);
							} else if (editField.getText().equals("non")) {
								joueurSelectionne.setProfessionnel(false);
								addToJoueurModifies(joueurSelectionne);
							}
							else {
								cancelTextFieldChange(editField, text, vue.getInfosJoueur(), indiceInfoDansTextFlow);
							}
							text.setText(editField.getText());
							validateTextFieldChange(editField, text, vue.getInfosJoueur(), indiceJoueurSelectionne, indiceInfoDansTextFlow);	
						}
						else if (event.getCode() == KeyCode.ESCAPE) {
							escaped = true;
							cancelTextFieldChange(editField, text, vue.getInfosJoueur(), indiceInfoDansTextFlow);
							return;
						}
					}

				});

				editField.focusedProperty().addListener(new ChangeListener<Boolean>() {
					@Override
					public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
						if (!newValue) { // si il y a eu un changement de focus
							if (editField.getText() == null || editField.getText().equals("")) {
								cancelTextFieldChange(editField, text, vue.getInfosJoueur(), indiceInfoDansTextFlow);
								return;
							}
							if (!escaped) {
								if (editField.getText().equals("oui")) {
									joueurSelectionne.setProfessionnel(true);
									addToJoueurModifies(joueurSelectionne);
								} else if (editField.getText().equals("non")) {
									joueurSelectionne.setProfessionnel(false);
									addToJoueurModifies(joueurSelectionne);
								}
								else{ cancelTextFieldChange(editField, text, vue.getInfosJoueur(), indiceInfoDansTextFlow); }
								text.setText(editField.getText());
								validateTextFieldChange(editField, text, vue.getInfosJoueur(), indiceJoueurSelectionne, indiceInfoDansTextFlow);								
							}
						}

					}
				});

			}	
		}
	}

	class NumLicenceDoubleClickToEditEventHandler implements EventHandler<MouseEvent> {
		private Boolean escaped;

		@Override
		public void handle(MouseEvent event) {
			if (event.getClickCount() == 2) {
				escaped = false;
				Text text = (Text) event.getSource();
				int indiceJoueurSelectionne = vue.getJoueursListView().getSelectionModel().getSelectedIndex();
				int indiceInfoDansTextFlow = vue.getInfosJoueur().getChildren().indexOf(text);
				Joueur joueurSelectionne = vue.getJoueursListView().getSelectionModel().getSelectedItem();
				final TextField editField = addTextFieldFor(text, vue.getInfosJoueur(), "valeurEquipe", 200);

				editField.setOnKeyPressed(new EventHandler<KeyEvent>() {

					@Override
					public void handle(KeyEvent event) {
						if (event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.TAB) {
							if (editField.getText() == null || editField.getText().equals("") || !editField.getText().matches("\\d+")) {
								cancelTextFieldChange(editField, text, vue.getInfosJoueur(), indiceInfoDansTextFlow);
								return;
							}
							joueurSelectionne.setNumLic(Integer.parseInt(editField.getText()));
							text.setText(editField.getText());
							addToJoueurModifies(joueurSelectionne);
							validateTextFieldChange(editField, text, vue.getInfosJoueur(), indiceJoueurSelectionne, indiceInfoDansTextFlow);
						}
					}

				});

				editField.focusedProperty().addListener(new ChangeListener<Boolean>() {

					@Override
					public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
						if (!newValue) { // si il y a eu un changement de focus
							if (editField.getText() == null || editField.getText().equals("") || !editField.getText().matches("\\d+")) {
								cancelTextFieldChange(editField, text, vue.getInfosJoueur(), indiceInfoDansTextFlow);
								return;
							}
							else if (!escaped) {
								joueurSelectionne.setNumLic(Integer.parseInt(editField.getText()));
								text.setText(editField.getText());
								addToJoueurModifies(joueurSelectionne);
								validateTextFieldChange(editField, text, vue.getInfosJoueur(), indiceJoueurSelectionne, indiceInfoDansTextFlow);								
							}
						}

					}

				});
			}	
		}
	}

	class VilleEquipeDoubleClickToEditEventHandler implements EventHandler<MouseEvent> {
		private Boolean escaped;

		@Override
		public void handle(MouseEvent event) {
			if (event.getClickCount() == 2 && !vue.getEquipesListView().getSelectionModel().getSelectedItem().getNom().equals("Sans équipe")) {
				escaped = false;
				Text text = (Text) event.getSource();
				Equipe equipeSelectionne = vue.getEquipesListView().getSelectionModel().getSelectedItem();
				int indiceInfoDansTextFlow = vue.getInfoEquipe().getChildren().indexOf(text);
				final TextField editField = new TextField(text.getText());
				vue.getInfoEquipe().getChildren().remove(text);
				vue.getInfoEquipe().getChildren().add(indiceInfoDansTextFlow, editField);
				editField.getStyleClass().add("valeurEquipe");
				editField.getStyleClass().add("editField");
				editField.setPrefWidth(200);
				editField.requestFocus();
				editField.selectAll();

				editField.setOnKeyPressed(new EventHandler<KeyEvent>() {

					@Override
					public void handle(KeyEvent event) {
						if (event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.TAB) {
							if (editField.getText() == null || editField.getText().equals("")) {
								vue.getInfoEquipe().getChildren().remove(editField);
								vue.getInfoEquipe().getChildren().remove(text);
								vue.getInfoEquipe().getChildren().add(indiceInfoDansTextFlow, text);
								return;
							}
							equipeSelectionne.setVille(editField.getText());
							text.setText(editField.getText());
							refreshEquipeListView(vue.getEquipesListView().getSelectionModel().getSelectedIndex());
							vue.getInfoEquipe().getChildren().remove(editField);
							vue.getInfoEquipe().getChildren().remove(text);
							vue.getInfoEquipe().getChildren().add(indiceInfoDansTextFlow, text);
							addToEquipeModifiees(equipeSelectionne);
						} else if (event.getCode() == KeyCode.ESCAPE) {
							escaped = true;
							vue.getInfoEquipe().getChildren().remove(editField);
							vue.getInfoEquipe().getChildren().remove(text);
							vue.getInfoEquipe().getChildren().add(indiceInfoDansTextFlow, text);
						}
					}

				});

				editField.focusedProperty().addListener(new ChangeListener<Boolean>() {

					@Override
					public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
						if (editField.getText() == null || editField.getText().equals("")) {
							vue.getInfoEquipe().getChildren().remove(editField);
							vue.getInfoEquipe().getChildren().remove(text);
							vue.getInfoEquipe().getChildren().add(indiceInfoDansTextFlow, text);
							return;
						}
						else if (!escaped) {
							equipeSelectionne.setVille(editField.getText());
							text.setText(editField.getText());
							refreshEquipeListView(vue.getEquipesListView().getSelectionModel().getSelectedIndex());
							vue.getInfoEquipe().getChildren().remove(editField);
							vue.getInfoEquipe().getChildren().remove(text);
							vue.getInfoEquipe().getChildren().add(indiceInfoDansTextFlow, text);
							addToEquipeModifiees(equipeSelectionne);
						}
					}

				});

			}
		}

	}

	class NomEquipeDoubleClickToEditEventHandler implements EventHandler<MouseEvent> {
		private Boolean escaped;

		@Override
		public void handle(MouseEvent event) {
			if (event.getClickCount() == 2 && !vue.getEquipesListView().getSelectionModel().getSelectedItem().getNom().equals("Sans équipe")) {
				escaped = false;
				Text text = (Text) event.getSource();
				Equipe equipeSelectionne = vue.getEquipesListView().getSelectionModel().getSelectedItem();
				int indiceInfoDansTextFlow = vue.getInfoEquipe().getChildren().indexOf(text);
				final TextField editField = new TextField(text.getText());
				vue.getInfoEquipe().getChildren().remove(text);
				vue.getInfoEquipe().getChildren().add(indiceInfoDansTextFlow, editField);
				editField.getStyleClass().add("valeurEquipe");
				editField.getStyleClass().add("editField");
				editField.setPrefWidth(200);
				editField.requestFocus();
				editField.selectAll();

				editField.setOnKeyPressed(new EventHandler<KeyEvent>() {

					@Override
					public void handle(KeyEvent event) {
						if (event.getCode() == KeyCode.ENTER || event.getCode() == KeyCode.TAB) {
							if (editField.getText() == null || editField.getText().equals("") || editField.getText().equals("Sans équipe")) {
								vue.getInfoEquipe().getChildren().remove(editField);
								vue.getInfoEquipe().getChildren().remove(text);
								vue.getInfoEquipe().getChildren().add(indiceInfoDansTextFlow, text);
								return;
							}
							equipeSelectionne.setNom(editField.getText());
							text.setText(editField.getText());
							refreshEquipeListView(vue.getEquipesListView().getSelectionModel().getSelectedIndex());
							vue.getInfoEquipe().getChildren().remove(editField);
							vue.getInfoEquipe().getChildren().remove(text);
							vue.getInfoEquipe().getChildren().add(indiceInfoDansTextFlow, text);
							addToEquipeModifiees(equipeSelectionne);
						} else if (event.getCode() == KeyCode.ESCAPE) {
							escaped = true;
							vue.getInfoEquipe().getChildren().remove(editField);
							vue.getInfoEquipe().getChildren().remove(text);
							vue.getInfoEquipe().getChildren().add(indiceInfoDansTextFlow, text);
						}
					}

				});

				editField.focusedProperty().addListener(new ChangeListener<Boolean>() {

					@Override
					public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
						if (editField.getText() == null || editField.getText().equals("") || editField.getText().equals("Sans équipe")) {
							vue.getInfoEquipe().getChildren().remove(editField);
							vue.getInfoEquipe().getChildren().remove(text);
							vue.getInfoEquipe().getChildren().add(indiceInfoDansTextFlow, text);
							return;
						}
						else if (!escaped) {
							equipeSelectionne.setNom(editField.getText());
							text.setText(editField.getText());
							refreshEquipeListView(vue.getEquipesListView().getSelectionModel().getSelectedIndex());
							vue.getInfoEquipe().getChildren().remove(editField);
							vue.getInfoEquipe().getChildren().remove(text);
							vue.getInfoEquipe().getChildren().add(indiceInfoDansTextFlow, text);	
							addToEquipeModifiees(equipeSelectionne);
						}
					}

				});

			}
		}

	}

	class SupprimerJoueurEventHandler implements EventHandler<ActionEvent> {

		@Override
		public void handle(ActionEvent event) {
			Equipe equipeSelectionnee = vue.getEquipesListView().getSelectionModel().getSelectedItem();
			removeFromJoueursLists(vue.getJoueursListView().getSelectionModel().getSelectedItem());
			equipeSelectionnee.removeJoueur(vue.getJoueursListView().getSelectionModel().getSelectedItem());
			refreshEquipeListView(vue.getEquipesListView().getSelectionModel().getSelectedIndex());
		}

	}

	class SupprimerEquipeEventHandler implements EventHandler<ActionEvent> {

		@Override
		public void handle(ActionEvent event) {
			Equipe equipeSelectionnee = vue.getEquipesListView().getSelectionModel().getSelectedItem();
			ArrayList<Joueur> joueursSupprimés = (ArrayList<Joueur>) equipeSelectionnee.getJoueurs().clone();
			equipes.remove(equipeSelectionnee);
			removeFromEquipesLists(equipeSelectionnee);
			for (Joueur joueur : joueursSupprimés) {
				equipes.get(0).addJoueur(joueur);
				joueur.addAncienneEquipe(equipeSelectionnee.getNom());
				addToJoueurModifies(joueur);
			}
			refreshEquipeListView(vue.getEquipesListView().getSelectionModel().getSelectedIndex());
		}

	}

}
