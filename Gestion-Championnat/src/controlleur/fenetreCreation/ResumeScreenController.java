package controlleur.fenetreCreation;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;

import modele.Championnat;
import modele.Equipe;
import application.FenetreCreation;
import application.Main;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.cell.TextFieldListCell;
import javafx.scene.input.MouseEvent;
import javafx.util.StringConverter;
import vue.fenetreCreation.ResumeScreenView;
import controlleur.ControlledScreen;
import controlleur.ScreensController;

public class ResumeScreenController implements ControlledScreen {
	private ResumeScreenView vue;
	private ScreensController screensManager;
	private ObservableList<Equipe> affichageSansSansEquipe;

	public ResumeScreenController() {
		vue = new ResumeScreenView();
		
		vue.getPanePrincipal().addEventFilter(ActionEvent.ANY, new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				if (event.getTarget().equals(vue.getPanePrincipal())) {
					affichageSansSansEquipe = FXCollections.observableArrayList(EquipeScreenController.getEquipes());
					affichageSansSansEquipe.remove(0);
					vue.getEquipeListView().setItems(affichageSansSansEquipe);
					vue.getTexte4().setText("Avec cette configuration, aucune équipe n'est desavantagée. Vous pouvez valider le championnat.");
					
					if (affichageSansSansEquipe.size() % 2 == 1) {
						int resultatModuloDesavantage = DateScreenController.getDatesSelectionnees().size() % (affichageSansSansEquipe.size() - 1);
						if (resultatModuloDesavantage != 0) {
							int nbrDeJoursAAjouter = (affichageSansSansEquipe.size() - 1) - resultatModuloDesavantage;
							vue.getTexte4().setText("Avec cette configuration, une équipe est desavantagée. Il est conseillé d'ajouter " + nbrDeJoursAAjouter + " jours.");
						}
					}
				}
			}
		});

		// --------------------------------------------------
		// Remplissage des ListViews
		// --------------------------------------------------
		affichageSansSansEquipe = FXCollections.observableArrayList(EquipeScreenController.getEquipes());
		if (!EquipeScreenController.getEquipes().isEmpty()) {
			affichageSansSansEquipe.remove(0);
		}
		vue.getEquipeListView().setItems(affichageSansSansEquipe);
		vue.getDateListView().setItems(DateScreenController.getDatesSelectionnees());

		vue.getDateListView().setCellFactory(TextFieldListCell.forListView(new StringConverter<LocalDate>() {

			@Override
			public String toString(LocalDate object) {
				return object.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL));
			}

			@Override
			public LocalDate fromString(String string) {
				return null;
			}
		}));
		
		vue.getEquipeListView().setCellFactory(TextFieldListCell.forListView(new StringConverter<Equipe>() {

			@Override
			public String toString(Equipe object) {
				return object.getNom();
			}

			@Override
			public Equipe fromString(String string) {
				return null;
			}
		}));

		vue.getEcranDate().setOnMouseReleased(new EcranDateEventHandler());
		vue.getEcranEquipe().setOnMouseReleased(new EcranEquipeEventHandler());
		vue.getValider().setOnMouseReleased(new ValiderEventHandler());
	}

	@Override
	public void setScreenParent(ScreensController screenPage) {
		screensManager = screenPage;
	}

	public ResumeScreenView getVue() {
		return vue;
	}

	public ScreensController getScreensManager() {
		return screensManager;
	}

	class EcranDateEventHandler implements EventHandler<MouseEvent> {

		@Override
		public void handle(MouseEvent event) {
			screensManager.setScreen(FenetreCreation.SCREEN1ID);
		}

	}

	class EcranEquipeEventHandler implements EventHandler<MouseEvent> {

		@Override
		public void handle(MouseEvent event) {
			screensManager.setScreen(FenetreCreation.SCREEN2ID);
		}

	}
	
	class ValiderEventHandler implements EventHandler<MouseEvent> {

		@Override
		public void handle(MouseEvent event) {
			Championnat.validerChampionnat(EquipeScreenController.getEquipesSupprimees(), new ArrayList<Equipe>(affichageSansSansEquipe) , 
					EquipeScreenController.getEquipesModifiees(), EquipeScreenController.getEquipesAjoutees(), 
					EquipeScreenController.getJoueursModifies(), EquipeScreenController.getJoueursAjoutes(), 
					new ArrayList<LocalDate>(DateScreenController.getDatesSelectionnees()));
			Main.showFenetrePrincipale();
		}
		
	}

}
