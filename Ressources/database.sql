-- Scripts de creation de la base de donnees: Projet Handball

DROP TABLE Recevoir;
DROP TABLE Visiter;
DROP TABLE Participer;
DROP TABLE Action;
DROP TABLE Match;
DROP TABLE Journee;
DROP TABLE Joueur;
DROP TABLE Equipe;

CREATE TABLE Equipe(
	idE number(8) NOT NULL PRIMARY KEY, -- on suppose qu'il n'y aura pas plus de 99 999 999 equipes differentes.
	nomE varchar2(30) NOT NULL, 
	VilleE varchar2(30) NOT NULL,
	points number(10)
);

CREATE TABLE Joueur(
	idJ number(9) NOT NULL PRIMARY KEY, -- On suppose qu'il n'y aura pas plus de 999 999 999 de joueurs.
	idE number(8),
	constraint fk_idEJ FOREIGN KEY (idE) REFERENCES Equipe(idE),
	num number(2),
	numLicJ number(8),
	nomJ varchar2(30) NOT NULL,
	prenomJ varchar2(30) NOT NULL,
	profJ char, 
	constraint estProfessionel check (profJ='O' or profJ='N'),
	datenaisJ date NOT NULL,
	roleJ char,
	anciennesEquipe varchar2(1000),
	constraint roleJoueurOk check (roleJ='E' or roleJ='A' or roleJ='J')
);

CREATE TABLE Journee(
	numJour number(3) NOT NULL PRIMARY KEY,
	dateJour date NOT NULL 
);

CREATE TABLE Match(
	idM number(8) NOT NULL PRIMARY KEY,
	idE1 number(9) NOT NULL,
	constraint fk_idE1M FOREIGN KEY (idE1) REFERENCES Equipe(idE),
	idE2 number(9) NOT NULL,
	constraint fk_idE2M FOREIGN KEY (idE2) REFERENCES Equipe(idE),
	constraint ck_idEDiffM check (idE1 <> idE2),
	dateM date NOT NULL,
	scoreLocaux number(3),
	scoreVisiteur number(3),
	numJour number(3) NOT NULL,
	constraint fk_jourM FOREIGN KEY (numJour) REFERENCES Journee(numJour),
	charge char,
	constraint chargeOK check (charge='O' or charge='N')
);

-- drop table visiter;
-- drop table recevoir;
-- drop table action;
-- drop table participer;
-- update match set charge = 'N' , scoreLocaux = 0 , scoreVisiteur = 0;
-- update equipe set points = 0;

CREATE TABLE Participer(
	idJ number(9),
	idM number(8),
	constraint pk_idMJP PRIMARY KEY (idJ,idM),
	constraint fk_idJP FOREIGN KEY (idJ) REFERENCES Joueur(idJ),
	constraint fk_idMP FOREIGN KEY (idM) REFERENCES Match(idM),
	nbButs number(3), -- On suppose qu'il n'est pas possible de marquer plus de 999 buts par matches.
	nbPassesDecisives number(3), --idem
	nbFautes number(3) --idem
);
CREATE TABLE Action(
	idA number(4) PRIMARY KEY,
	idJ number(9),
	idM number(8) NOT NULL,
	constraint fk_idJA FOREIGN KEY (idJ) REFERENCES Joueur(idJ),
	constraint fk_idMA FOREIGN KEY (idM) REFERENCES Match(idM),
	timer varchar2(10) NOT NULL,
	action varchar2(30) NOT NULL,
	joueur varchar2 (30) NOT NULL,
	equipe varchar2(30) NOT NULL
);

CREATE TABLE Recevoir(
	idM number(8) NOT NULL PRIMARY KEY,
	idE number(8),
	constraint fk_idER FOREIGN KEY (idE) REFERENCES Equipe(idE),
	constraint fk_idMR FOREIGN KEY (idM) REFERENCES Match(idM),
	nbButs number(3),
	nbFautes number(3)
);

CREATE TABLE Visiter(
	idM number(8) NOT NULL PRIMARY KEY,
	idE number(8),
	constraint fk_idEV FOREIGN KEY (idE) REFERENCES Equipe(idE),
	constraint fk_idMV FOREIGN KEY (idM) REFERENCES Match(idM),
	nbButs number(3),
	nbFautes number(3)
);
--commit;
