-- Scripts de creation de la base de donnees: Projet Handball

DROP TABLE Recevoir;
DROP TABLE Visiter;
DROP TABLE Journee;
DROP TABLE Participer;
DROP TABLE Matchs;
DROP TABLE Joueur;
DROP TABLE Equipe;

CREATE TABLE Equipe(
	idE int(8) NOT NULL, -- on suppose qu'il n'y aura pas plus de 10 000 000 equipes differentes.
	PRIMARY KEY(idE),
	nomE varchar(30) NOT NULL, 
	VilleE varchar(30) NOT NULL
);

CREATE TABLE Joueur(
	idJ int(9) NOT NULL, -- On suppose qu'il n'y aura pas plus de 100 000 000 de joueurs.
	PRIMARY KEY(idJ),
	idE int(8),
	FOREIGN KEY (idE) REFERENCES Equipe(idE),
	numLicJ int(8),
	nomJ varchar(30) NOT NULL,
	prenomJ varchar(30) NOT NULL,
	profJ char, constraint estProfessionel check (profJ='O' or profJ='N'),
	datenaisJ date NOT NULL,
	roleJ char, constraint roleJoueurOk check (roleJ ='E' or roleJ= 'A' or roleJ= 'J')
) ENGINE=INNODB;

CREATE TABLE Matchs(
	idM int(8) NOT NULL,
	PRIMARY KEY(idM),
	idA1 int(9) NOT NULL,
	FOREIGN KEY (idA1) REFERENCES Joueur(idJ),
	idA2 int(9) NOT NULL,
	FOREIGN KEY (idA2) REFERENCES Joueur(idJ),
	check (idA1 <> idA2),
	dateM date NOT NULL,
	scoreLocaux int(3),
	scoreVisiteur int(3)
);

CREATE TABLE Participer(
	idJ int(9),
	idM int(8),
	PRIMARY KEY(idM, idJ),
	FOREIGN KEY (idJ) REFERENCES Joueur(idJ),
	FOREIGN KEY (idM) REFERENCES Matchs(idM),
	tempsJeu int(4) NOT NULL, -- en minutes
	tempsExp int(4) NOT NULL, -- en minutes
	nbButs int(3),
	nbPassesDecisives int(3),
	nbFautes int(3)
) ENGINE=INNODB;

CREATE TABLE Recevoir(
	idM int(8) NOT NULL,
	PRIMARY KEY(idM),
	idE int(8),
	FOREIGN KEY (idE) REFERENCES Equipe(idE),
	FOREIGN KEY (idM) REFERENCES Matchs(idM),
	nbButs int(3),
	nbFautes int(3)
) ENGINE=INNODB;

CREATE TABLE Visiter(
	idM int(8) NOT NULL,
	PRIMARY KEY(idM),
	idE int(8),
	FOREIGN KEY (idE) REFERENCES Equipe(idE),
	FOREIGN KEY (idM) REFERENCES Matchs(idM),
	nbButs int(3),
	nbFautes int(3)
) ENGINE=INNODB;

CREATE TABLE Journee(
	numJour int(3) NOT NULL,
	PRIMARY KEY(numJour),
	dateJour date NOT NULL
);